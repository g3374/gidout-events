#  This is how **Gidout** is installed on a fresh install of Koozali SME server 10 in the Primary Ibay

You should find that **GIDOUT** is a good example of using flask in a big project. It mostly works, there are lots of areas that need some TLC.

Primarily it is a site for promoting events, something that took a big hit from the coof. It is easy to modify it into a message board, or add email usage, 99% is there already.
It is also possible to build a chan or a voat from this.

Best of luck!

It is possible to install in another ibay, or in a virtual environment, or have multiple installations. I do not go into that so consult with Graham Dumpleton's fine website on wsgi.

### Install sme 10:
A Centos or Fedora or Nethserver install are very similar. It is just that SME has a great templating system. 
Mitel developed SME many years ago and it is a very capable and logical system for organizing a Linux server.  

Instructions at Koozali.org

Add a user, allow ssh to the local network, if you wish to use samba for file transfer to the primary ibay add 'www' group to the user.
Depending on your workstation OS you may run into Samba issues due to the change from SMB1. There are instructions on how to fiddle with this on the Koozali site.

`yum update` 

Make sure that everything is up to date

### Install the necessary development packages:
```
yum -y install gcc openssl-devel python-devel httpd-devel mysql-devel python3 python3-devel bzip2-devel libffi-devel`
```
### Update pip so that the packages will install:

`pip3 install --upgrade pip`
### Allow a user ssh access:

`yum --enablerepo=smecontribs install smeserver-remoteuseraccess`

`signal-event post-upgrade; signal-event reboot`

### Easy install of mod wsgi:

`pip3 install mod_wsgi`

### Optional Manual install of wsgi:
switch to a user area. Note the version may have changed by now, check!

`wget https://github.com/GrahamDumpleton/mod_wsgi/archive/4.7.1.tar.gz`

`tar xvfz 4.7.1.tar.gz`

`cd mod_wsgi-4.7.1`

`./configure`

`make`

`make install`
***


### Use this to find the location of libraries:

`mod_wsgi-express module-config`

### Result:

>LoadModule wsgi_module "/usr/local/lib64/python3.6/site-packages/mod_wsgi/server/mod_wsgi-py36.cpython-36m-x86_64-linux-gnu.so"
WSGIPythonHome "/usr"


### Make the error_log template:

`mkdir -p /etc/e-smith/templates-custom/etc/httpd/conf/httpd.conf`

`cd /etc/e-smith/templates-custom/etc/httpd/conf/httpd.conf`

`nano 10ErrorLog`

```
# ErrorLog: The location of the error log file. If this does not start
# with /, ServerRoot is prepended to it.
ErrorLog /var/log/httpd/error_log`
# LogLevel: Control the number of messages logged to the error_log.
# Possible values include: debug, info, notice, warn, error, crit,
# alert, emerg.
LogLevel info
```
### Make the loadmodule template:

`nano 20LoadModuleWSGI_MOD`

It may be python3.8 now **check** your version of python3.

```
LoadModule wsgi_module "/usr/local/lib64/python3.6/site-packages/mod_wsgi/server/mod_wsgi-py36.cpython-36m-x86_64-linux-gnu.so"
```
### Make a VirtualHosts dir

`mkdir VirtualHosts`

`cd VirtualHosts`

### Make the template so that wsgi can find the instructions script

`nano 26WSGIstuff`

```
WSGIScriptAlias / /home/e-smith/files/ibays/Primary/html/myapp.wsgi
```

### Get the templates working:

`expand-template /etc/httpd/conf/httpd.conf`

`service httpd-e-smith restart`

`apachectl configtest`

### Stop here and make sure everything is working properly so far.

## This section is just if you want to test flask:

### The folder structure 
```
Primary
	html
		myapp.wsgi  		
		app
    		__init__.py
    		static
      			css
      			images
      			js
      			uploads
    		templates
```

### Install flask

`pip3 install flask`

### This is what a simple wsgi script looks like:

`cd /home/e-smith/files/ibays/Primary/html`

`nano myapp.wsgi`

```
# -*- coding: utf-8 -*-
import sys
import os
project = "app"
# Use instance folder, instead of env variables.
# specify dev/production config
#os.environ['%s_APP_CONFIG' % project.upper()] = ''
# http://code.google.com/p/modwsgi/wiki/ApplicationIssues#User_HOME_Environment_Variable
#os.environ['HOME'] = pwd.getpwuid(os.getuid()).pw_dir
BASE_DIR = os.path.join(os.path.dirname(__file__))
if BASE_DIR not in sys.path:
    sys.path.append(BASE_DIR)
# give wsgi the "application"
from app import app as application
```

### This is a small test application to see if it works:

```
from flask import Flask
app = Flask(__name__)
@app.route('/')
def hello_world():
    return 'Hello, World! This is just a test!!'
if __name__ == '__main__':
    app.run(
        host="0.0.0.0",
        port=int("80"),
        debug=True
        )
```

### If it does not run when you direct a browser to your computer:

### Install the lynx browser:

`yum install lynx`

From the terminal go to the html folder.

`export FLASK_APP=app`

`export FLASK_ENV=development`

`flask run`

`lynx 127.0.0.1`

Now you should get some usable errors to hunt down where it went wrong, most likely it will be an uninstalled import.
Or very likely it will be permissions.

Do not leave the system running in this state as it is very insecure.

### To repair the html folder ownership:

`chown -R www:www *`

# The install of GIDOUT:

The folder structure
 
```
Primary
	html
		instance
			config.py
		myapp.wsgi
		favicon.ico  		
		gidout
    		__init__.py
			gregorian_calendar.py
			forms.py
			models.py
    		static
      			css
      			images
      			js
      			uploads
    		templates
			admin
			edit
			entry
			gcalendar
			home
			pages
			payment
			post
			search
			utility
			venue
```

### Install the requirements:

`pip3 -r install requirements.txt`

### Setup the database:

create a user in the mariadb system, and give it the necessary permissions to read, and modify the database, put the necessary info in the config file

Use either emma or mysqlworkbench or a terminal to install the starter sql files. 

### The working myapp.wsgi file:

```
# -*- coding: utf-8 -*-
import sys
import os
project = "app"
# Use instance folder, instead of env variables.
# specify dev/production config
#os.environ['%s_APP_CONFIG' % project.upper()] = ''
# http://code.google.com/p/modwsgi/wiki/ApplicationIssues#User_HOME_Environment_Variable
#os.environ['HOME'] = pwd.getpwuid(os.getuid()).pw_dir
BASE_DIR = os.path.join(os.path.dirname(__file__))
if BASE_DIR not in sys.path:
    sys.path.append(BASE_DIR)
# give wsgi the "application"
from gidout import app as application
```

### Very important adjustment for werkzeug to work:

First find flask_uploads.py

/usr/local/lib/python3.6/site-packages/flask_uploads.py

Change

```
from werkzeug import secure_filename,FileStorage
```

to

```
from werkzeug.utils import secure_filename
from werkzeug.datastructures import  FileStorage
```

I hate it when libraries change like this, hours of frustration. I also hated updating from Python2 to Python3. I would be lost without stackexchange.

## Change the instance/config.py file to your settings

Now it should be working, you may have to reboot.

### Usefull commands

`GRANT ALL ON *.* TO 'name'@'*' IDENTIFIED BY 'bigcomplicatedpassword';`

`sudo mysqldump -u [user] -p [database_name] > [filename].sql`

`signal-event post-upgrade; signal-event reboot`

`scp`






































