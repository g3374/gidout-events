#remove_html_markup /postsearch
from . import search
from flask import Flask, render_template, request, flash, abort, jsonify, redirect, url_for, session, current_app
import datetime, time, uuid
from xtermcolor import colorize
from gidout import db
import os, math, bleach
from flask_login import login_required, UserMixin, current_user
from flask_mail     import Mail
from gidout.entry.routes import send_email
from gidout.models       import Gidgroup, Edge, Post, User
from datetime import datetime

def remove_html_markup(s):
    tag = False
    quote = False
    out = ""
    for c in s:
            if c == '<' and not quote:
                tag = True
            elif c == '>' and not quote:
                tag = False
            elif (c == '"' or c == "'") and tag:
                quote = not quote
            elif not tag:
                out = out + c
    return out

@search.route('/postsearch', methods=['GET', 'POST'])
def postsearch():
    title = "Post Search"
    tuser = "anon"
    but   = current_app.config['DEFAULTBUTTON1']
    if current_user.is_authenticated:
        tuser = current_user.id
        if current_user.role > 4:
            but[5] = 'food'
        if current_user.role > 49:
            but[5] = 'adminhome'
            #print 'admin on front page'
    else:
        but[5] = 'register'
    txtlist   = [["" for x in range(7)] for x in range(50)]
    #print request.args.get("search"), "ARGS"
    spot = 5
    if request.args.get("search") == "" or request.args.get("search") == None:
        return redirect("/home")
    spot = 10
    try:
        #print colorize("Here inside postsearch ", ansi=50)
        # bleach the input, search the bodystripped for the word
        nsearch = bleach.clean(request.args.get("search"), strip = True)
        newsearch = remove_html_markup(nsearch)
        # check the date and only return the future max 100
        if newsearch == "" or newsearch == None:
            return redirect("/home")
        # check to see if empty search

        tmpsearch = Post.query.filter(Post.start >= datetime.now()).filter(Post.bodystripped.like('%' + newsearch + '%')).order_by(Post.start).limit(50)
        it = 0
        for tmp in tmpsearch:
            txtlist[it][0] = tmp.postgroup
            if tmp.bodystripped != None and tmp.bodystripped != "None":
                if len(tmp.bodystripped) > 400:
                    info = (tmp.bodystripped[:400] + ' ...')
                else:
                    info = tmp.bodystripped
            else:
                info = "Empty.."
            txtlist[it][1] = info
            txtlist[it][2] = tmp.posttitle
            txtlist[it][3] = tmp.id
            txtlist[it][4] = datetime.strftime(tmp.start,'%a %b %d %-I:%M %p')
            txtlist[it][5] = tmp.karma_good
            if tmp.image == "":
                # show the group image
                txtlist[it][6] = "/static/images/post/" + tmp.postgroup + ".png"
            else:
                # Take the image from the image column
                txtlist[it][6] = tmp.image
            it += 1

        spot = 90
        return render_template('postsearch.html',
            title = title,
            but   = but,
            tstlistb = txtlist,
            gbut  = current_app.config['DEFAULTBUTTON2']
            )

    except Exception as e:
        flash("Something Failed in post search")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in Post Search",
            errstring = str(e),
            user      = tuser,
            place     = spot)
        subject = "Exception Error!"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))




