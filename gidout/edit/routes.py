#remove_html_markup /_editdelpic /_venedit /editpost /useredit /venueedit /venueimageedit/<int:postnum> /edit_pic

from . import edit
from flask import Flask, render_template, request, flash, abort, jsonify, redirect, url_for, session, current_app
from calendar import Calendar, HTMLCalendar
import datetime, time, uuid
from xtermcolor import colorize
from gidout import db
from gidout.models import Gidgroup, Edge, Post, User, Venue
from gidout.forms import PostForm, UserForm, VenueForm
import os, math, bleach, re
from flask_login import login_required, UserMixin, current_user, fresh_login_required, login_fresh, confirm_login
from gidout.admin.routes import load_user
from werkzeug.utils import secure_filename
from flask_uploads import UploadSet, IMAGES, configure_uploads
import googlemaps
from MySQLdb import escape_string as thwart
from flask_ckeditor import CKEditor, CKEditorField
from flask_mail import Mail
from gidout.entry.routes import send_email
try:
    from urllib.parse import urlparse, urljoin
except:
    from urlparse import urlparse, urljoin
from PIL            import Image


def remove_html_markup(s):
    tag = False
    quote = False
    out = ""
    for c in s:
            if c == '<' and not quote:
                tag = True
            elif c == '>' and not quote:
                tag = False
            elif (c == '"' or c == "'") and tag:
                quote = not quote
            elif not tag:
                out = out + c
    return out

@edit.route('/_editdelpic')
@login_required
def editdelpic(): #this deletes one pic at a time from the database
    # cut the filename and then delete /static/uploads/venue/**date**/image/
    tuser = current_user.nickname

    #print colorize('editdelpic', ansi = 11)
    try:
        spot = 20
        a = request.args.get('a', type=str)
        b = request.args.get('b', type=str)
        post = Venue.query.get(b)
        # print post.image0
        spot = 40
        # print "request args  a", colorize( a, ansi = 191)
        # print "request args  b", colorize( b, ansi = 120)
        path_list = a.split(os.sep)
        # print "path_list ", colorize( path_list, ansi = 11)
        # build path and delete from database
        todel = '/static/uploads/venue/' + path_list[6] + '/images/' + path_list[8]
        # print "path_todel ", colorize( todel, ansi = 223)
        if post.image0 == todel:
            post.image0 = ''
        if post.image1 == todel:
            post.image1 = ''
        if post.image2 == todel:
            post.image2 = ''
        if post.image3 == todel:
            post.image3 = ''
        if post.image4 == todel:
            post.image4 = ''
        if post.image5 == todel:
            post.image5 = ''
        if post.image6 == todel:
            post.image6 = ''
        if post.image7 == todel:
            post.image7 = ''
        if post.image8 == todel:
            post.image8 = ''
        if post.image9 == todel:
            post.image9 = ''
        if post.image10 == todel:
            post.image10 = ''
        if post.image11 == todel:
            post.image11 = ''
        spot = 50
        db.session.commit()
        spot = 100
        return jsonify(result= True)
    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in _editdelpic",
            errstring = str(e),
            user      = tuser,
            place     = spot)
        subject = "Exception Error! In EditDelPic"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@edit.route('/_venedit')
@login_required
def _venedit():
    spot = 0
    try:
        newlist = []
        b = request.args.get('a', type=str) # get the postnum
        # print b
        ven = Venue.query.filter(Venue.id == b).first_or_404()
        if ven:
            if ven.image0:
                newlist.append(ven.image0)
            if ven.image1:
                newlist.append(ven.image1)
            if ven.image2:
                newlist.append(ven.image2)
            if ven.image3:
                newlist.append(ven.image3)
            if ven.image4:
                newlist.append(ven.image4)
            if ven.image5:
                newlist.append(ven.image5)
            if ven.image6:
                newlist.append(ven.image6)
            if ven.image7:
                newlist.append(ven.image7)
            if ven.image8:
                newlist.append(ven.image8)
            if ven.image9:
                newlist.append(ven.image9)
            if ven.image10:
                newlist.append(ven.image10)
            if ven.image11:
                newlist.append(ven.image11)
                spot = 20
        else:
            return redirect(url_for('home.index'))
        #print newlist
        strng  = '<div id="topimg"><div class="w3-row">'
        strng1 = '<div class="w3-col s2 overlay-image"><img class="venimage" src="%s" alt="Picture One"  onclick="togglebut(this)" /><div class="ventext">Selected</div></div>'
        strng3 = '</div>'
        strng4 = '<div class="w3-row">'
        thumbpath = "/static/uploads/venue/"
        x = 0
        for li in newlist:
            bigfilename = os.path.basename(li) # this is the filename
            thumbdest = thumbpath + ven.imagedatename + '/thumbs/' + bigfilename
            # print colorize("thumbdest", ansi = 10), colorize (thumbdest, ansi = 11)
            strng = strng + '<div class="w3-col s2 overlay-image"><img class="venimage" src="%s" alt="Picture One"  onclick="togglebut(this)" /><div class="ventext">Selected</div></div>' %(thumbdest)
            if x == 5:
                spot = 50
                strng += strng3
                strng += strng4
            x += 1
        strng += strng3
        strng += strng3
        # print colorize(strng, ansi = 11)
        spot = 100
        #print colorize(strng, ansi = 9)
        return jsonify(result = strng)
    except Exception as e:
        flash("Something Failed in _venedit")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in _venedit",
            errstring = str(e),
            user      = "admin",
            place     = spot)
        subject = "Exception Error!"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@edit.route('/editpost', methods=['POST'])
@login_required
def editpost():
    # Edit the post
    tuser        = current_user.nickname
    spot = 0
    try:
        city         = current_app.config['CITY']
        gmaps        = googlemaps.Client(key = current_app.config['GOOGLE_MAP_KEY'])
        now          = datetime.datetime.now()
        form         = PostForm()
        but          = current_app.config['DEFAULTBUTTON1']
        butb         = ""
        userid       = current_user.id
        role         = current_user.role
        defaultgroup = "civics"
        filename     = ""
        place        = ""
        filepdf      = ""
        starttime    = ""
        endtime      = ""
        postn        = request.form['postnum'] # this is the number of the post to be edited
        imagefile    = ""
        pdffile      = ""
        spot = 10
        if current_user.role > 4:
            but[5] = 'food'
        if current_user.role > 49:
            but[5] = 'adminhome'
        groups = db.session.query(Gidgroup.name)
        tgroups = []
        # print colorize('GROUPS  ', ansi = 3), groups
        for t in groups:
            s = str(t)
            sm = s[3:-3]
            tgroups.append(sm)
        # print colorize('TESTGROUPS  ', ansi = 3), testgroups
            #print 'admin on front page'

        if request.values.get('edit'): # got here from edit button, This is the first step for an edit
            # fill out the form from the post table and build the template
            # grab the data for the post from the database and fill out the form
            # send image and pdf twice to see if it changes
            pge = db.session.query(Post).filter(Post.id == postn).first()
            form.user_id.data         = pge.user_id
            form.cname.data           = pge.contact
            form.cemail.data          = pge.contactemail
            form.cphone.data          = pge.contactphone
            form.city.data            = pge.city
            form.posttitle.data       = pge.posttitle
            form.imagefile.data       = pge.image
            form.googlemap.data       = pge.googlemapa
            form.mapchoice.data       = pge.mapchoice
            form.other.data           = pge.metatag
            form.externalwebsite.data = pge.weblink
            form.websitetitle.data    = pge.weblink_title
            form.price.data           = pge.price
            form.postbody.data        = pge.body
            form.postgroup.data       = pge.postgroup
            form.agegroup.data        = pge.agegroup
            form.vname.data           = pge.venuename
            form.vcity.data           = pge.city
            form.relay.data           = pge.relay
            form.vemail.data          = pge.venueemail
            form.vphone.data          = pge.venuephone
            form.vaddress.data        = pge.venueaddress
            form.pdffile.data         = pge.pdfbrochure
            start                     = pge.start
            end                       = pge.end
            form.startyr.data         = start.year
            form.endyr.data           = end.year
            form.startmth.data        = start.month
            form.endmth.data          = end.month
            form.startday.data        = start.day
            form.endday.data          = end.day
            form.starthr.data         = start.hour
            form.endhr.data           = end.hour
            form.startmin.data        = start.minute
            form.endmin.data          = end.minute
            form.name.data            = ""
            form.postnum.data         = postn

            spot = 20
            return render_template('editpost.html',
                title   = 'Edit Post',
                form    = form,
                but     = but,
                butb    = butb,
                role    = role,
                groups  = tgroups,
                gbut    = [pge.postgroup],
                postnum = postn,
                ) #end of loading up all the old values

        else: # else got here from the edit page check the data for changes and update the database
            if form.validate():
                #print "validated"
                #the form validates load up a post object with the data and commit to database
                #find the user id from the logon info
                #here I get the results from the posting of the webform to the variables to be validated to the database.
                # here I convert the separate time pieces to unix time
                # here I convert time pieces again
                startyr          = form.startyr.data
                startmth         = form.startmth.data
                startday         = form.startday.data
                starthr          = form.starthr.data
                startmin         = form.startmin.data
                endyr            = form.endyr.data
                endmth           = form.endmth.data
                endday           = form.endday.data
                endhr            = form.endhr.data
                endmin           = form.endmin.data
                startqtr         = (startmin - 1) * 15
                endqtr           = (endmin - 1) * 15
                starttime        = datetime.datetime(startyr,startmth,startday,starthr,startqtr,)
                endtime          = datetime.datetime(endyr,endmth,endday,endhr,endqtr,)
                tmptime          = endtime + datetime.timedelta(minutes=46)
                if tmptime       <= starttime:
                    endtime      = starttime + datetime.timedelta(hours=1)
                form.endyr.data  = endtime.year
                form.endmth.data = endtime.month
                form.endday.data = endtime.day
                form.endhr.data  = endtime.hour
                form.endmin.data = (endtime.minute / 15) + 1
                newfolder = datetime.datetime.today().strftime("%Y-%m-%d")
                tmpconfig = current_app.config.get('UPLOADED_IMAGES_DEST')

                # Check and upload image
                # print "FILENAME image     ", request.files.get('image')
                # print "FILENAME imagefile ", request.files.get('imagefile')
                # check to see if image and imagefile are the same
                # if they are different load the new image

                spot = 25
                #print "Ready to check and then load the image"
                #print request.files.get('image')
                try:
                    if request.files.get('image'): # new image file posted, save the image and update the database
                        ext = os.path.splitext(request.files.get('image').filename)
                        # create a new folder each day
                        # print ext, type(ext)
                        if not os.path.isdir(tmpconfig + '/' + newfolder):
                            os.mkdir(tmpconfig + '/' + newfolder)
                            os.mkdir(tmpconfig + '/' + newfolder + '/' + 'thumbs')
                            os.mkdir(tmpconfig + '/' + newfolder + '/' + 'mini')
                            spot = 500
                        spot = 61
                        # generate random name for file
                        tmpname = str(uuid.uuid4().hex)
                        # save file to disk
                        filename = images.save(request.files.get('image'), folder = tmpconfig + '/' + newfolder + '/', name = tmpname + ".")
                        filename = "/static/uploads/images/" + newfolder + '/' + tmpname + ext[1]
                    else:
                        # keep the old filename and do nothing
                        spot = 75
                        #print "unchanged"
                        #print form.imagefile.data
                        filename = form.imagefile.data
                except:
                    # print "FALTED HERE"
                    filename = ""

                # Check and upload pdf
                # print request.files.get('pdfbrochure').filename
                tmpconfig = current_app.config.get('UPLOADED_PDF_DEST')

                spot = 80
                try:
                    if request.files.get('pdfbrochure'):
                        # find name of new file, to upload PDFBROCHURE
                        # first check to see if todays directory exists
                        ext = os.path.splitext(request.files.get('pdfbrochure').filename)
                        # make a new folder every day
                        if not os.path.isdir(tmpconfig + '/' + newfolder):
                            os.mkdir(tmpconfig + '/' + newfolder)
                        tmpname = str(uuid.uuid4().hex)
                        # print 'request files pdf  ', request.files['pdfbrochure']
                        filepdf = pdf.save(request.files['pdfbrochure'], folder = pdfconfig + '/' + newfolder + '/', name = tmpname + ".")
                        filepdf = "/static/uploads/pdf/" + newfolder + '/' + tmpname + ext[1]
                        # print filepdf
                    else:
                        filepdf = ""
                except:
                    filepdf = ""

                # check and do the googlemap thang.
                try:
                    tmp = form.vaddress.data
                    if tmp != '':
                        if form.vcity.data != '':
                            tmp = tmp + ', ' + form.vcity.data + ', ' + state
                        else:
                            tmp = tmp + ', ' + city + ', ' + state
                    else:
                        tmp = city + ', ' + state
                    geocode_result = gmaps.geocode(tmp)
                    #lat = geocode_result[0]["geometry"]["location"]["lat"]
                    place = geocode_result[0]['place_id']
                except:
                    spot = 112
                    place = "ChIJcWGw3Ytzj1QR7Ui7HnTz6Dg"
                    # default to whatever city place_id

                #get all the info on the user to tst so I can use the id
                #print colorize('gotten here from tst  ', ansi=10), tst.id
                # validate the data from the web, flash an error for weird stuff
                # organize in the proper way
                # instantate a post object , fill the data and send to the database

                spot = 140
                # Clean the Body of nasty stuff
                if role > 19:
                    #standard buttons
                    bleachbody = bleach.clean(unicode(form.postbody.data).encode("utf-8"), strip = True, tags=['b',
                        'strong', 'em', 'blockquote', 'a', 'blockquote', 'p', 'i', 'li', 'ul', 'br', 'ol' 'strike', 'u', 'sub', 'sup', 'img',
                        'table', 'tr', 'td', 'thead', 'th', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'button', 'center', 'col', 'colgroup', 'font',
                        'hr', 'link'])

                    instring  = re.compile(re.escape('fuck'), re.IGNORECASE)
                    instringa = instring.sub('....', bleachbody)
                    instring  = re.compile(re.escape('shit'), re.IGNORECASE)
                    instringb = instring.sub('....', instringa)
                    instring  = re.compile(re.escape('cunt'), re.IGNORECASE)
                    instringc = instring.sub('....', instringb)
                    bleachbody = instringc
                else:
                    #basic buttons
                    bleachbody = bleach.clean(unicode(form.postbody.data).encode("utf-8"), strip = True, tags=['b',
                        'strong', 'em', 'blockquote', 'p', 'i', 'li', 'ul', 'br', 'ol', 'strike', 'u', 'sub', 'sup'])
                    instring  = re.compile(re.escape('fuck'), re.IGNORECASE)
                    instringa = instring.sub('....', bleachbody)
                    instring  = re.compile(re.escape('shit'), re.IGNORECASE)
                    instringb = instring.sub('....', instringa)
                    instring  = re.compile(re.escape('cunt'), re.IGNORECASE)
                    instringc = instring.sub('....', instringb)
                    bleachbody = instringc

                bbodystripped1  = remove_html_markup(bleachbody)
                bbodystripped2  = bbodystripped1.replace('&nbsp;'  , ' ')
                bbodystripped3  = bbodystripped2.replace('&rdquo;' , "'")
                bbodystripped4  = bbodystripped3.replace('&ldquo;' , '`')
                bbodystripped5  = bbodystripped4.replace('&rsquo;' , '"')
                bbodystripped6  = bbodystripped5.replace('&ndash;' , '-')
                bbodystripped7  = bbodystripped6.replace('&quot;'  , '"')
                bbodystripped8  = bbodystripped7.replace('&amp;'   , ' ')
                bbodystripped9  = bbodystripped8.replace('&hellip;', '...')
                bbodystripped10 = bbodystripped9.replace('&bull;'  , '-')
                bbodystripped11 = bbodystripped10.replace('&mdash;', '-')
                bbodystripped12 = bbodystripped11.replace('&lt;', '<')
                bbodystripped13 = bbodystripped12.replace('&gt;', '>')
                bbodystripped14 = bbodystripped13.replace('&middot;', '.')
                bbodystripped15 = bbodystripped14.replace('&eacute;', 'e')
                bbodystripped   = bbodystripped15.replace('&#39;', ' ')

                spot = 250
                #Clean the title of nasty stuff
                titlebodya = bleach.clean(unicode(form.posttitle.data).encode("utf-8"), strip = True)
                instring  = re.compile(re.escape('fuck'), re.IGNORECASE)
                instringa = instring.sub('....', titlebodya)
                instring  = re.compile(re.escape('shit'), re.IGNORECASE)
                instringb = instring.sub('....', instringa)
                instring  = re.compile(re.escape('cunt'), re.IGNORECASE)
                instringc = instring.sub('....', instringb)
                titlebodyb = remove_html_markup(instringc)
                titlebodyc = titlebodyb.replace('&nbsp;'  , ' ')
                titlebodyd = titlebodyc.replace('&rdquo;' , "'")
                titlebodye = titlebodyd.replace('&ldquo;' , '`')
                titlebodyf = titlebodye.replace('&rsquo;' , '"')
                titlebodyg = titlebodyf.replace('&ndash;' , '-')
                titlebodyh = titlebodyg.replace('&quot;'  , '"')
                titlebodyi = titlebodyh.replace('&amp;'   , ' ')
                titlebodyj = titlebodyi.replace('&hellip;', '...')
                titlebodyk = titlebodyj.replace('&bull;'  , '-')
                titlebodyl = titlebodyk.replace('&mdash;', '-')
                titlebodym = titlebodyl.replace('&lt;', '<')
                titlebodyn = titlebodym.replace('&gt;', '>')
                titlebodyo = titlebodyn.replace('&middot;', '.')
                titlebodyp = titlebodyo.replace('&eacute;', 'e')
                titlebody  = titlebodyp.replace('&#39;', ' ')
                spot =320
                #check to see if group exists, try with lower case fail back to post, lower case
                if Gidgroup.query.filter(Gidgroup.name == form.postgroup.data.lower()).first():
                    pgroup = form.postgroup.data.lower()
                else:
                    #group not found
                    flash("Group Not Found")
                    return render_template('post.html',
                        title     = 'Post',
                        form      = form,
                        but       = but,
                        butb      = butb,
                        role      = role,
                        gbut      = ['root','church','health','sport','news','entertainment','law','food','work','education','wealth','store','politics']
                        )

                spot = 360

                db.session.query(Post).filter(Post.id == postn).update({

                    'contact'          : thwart(form.cname.data),
                    'contactemail'     : thwart(form.vemail.data),
                    'contactphone'     : thwart(form.vphone.data),
                    #contactavatar    : form.avatar.data,
                    #contacthideemail : form.hideemail.data,
                    'city'             : thwart(form.vcity.data),
                    'posttitle'        : titlebody,
                    'image'            : filename,
                    #email            : form.email.data,
                    'googlemapa'       : place,
                    #googlemapb       : place2,
                    'mapchoice'        : 1,
                    'metatag'          : 'data',
                    'fax'              : 'data',
                    #weblink          : 'data',
                    'weblink_title'    : thwart(unicode(form.externalwebsite.data).encode("utf-8")),
                    'title'            : thwart(unicode(form.posttitle.data).encode("utf-8")),
                    'price'            : thwart(unicode(form.price.data).encode("utf-8")),
                    'body'             : bleachbody,
                    'bodystripped'     : bbodystripped,
                    'postgroup'        : thwart(form.postgroup.data),
                    'venuename'        : thwart(unicode(form.vname.data).encode("utf-8")),
                    'relay'            : 1,
                    'venueemail'       : thwart(unicode(form.vemail.data).encode("utf-8")),
                    'venuephone'       : thwart(unicode(form.vphone.data).encode("utf-8")),
                    'venueaddress'     : thwart(unicode(form.vaddress.data).encode("utf-8")),
                    'pdfbrochure'      : thwart(filepdf),
                    'timecreated'      : now,
                    'timestamp'        : now,
                    'start'            : starttime,
                    'end'              : endtime,
                    'repeats'          : '0',
                    'language'         : '1',
                    'approved'         : '1',
                    'agegroup'         : thwart(form.agegroup.data)

                    }) # this is the post data collection in the model includes all the cells in the data model

                db.session.commit()
                #print "commited"
                flash('SUCCESS! Post has been submitted!')
                spot = 450
                return render_template('editpost.html',
                    title       = 'Edit Post',
                    form        = form,
                    but         = but,
                    butb        = butb,
                    role        = role,
                    groups      = tgroups,
                    gbut        = [form.postgroup.data],
                    postnum     = postn,
                    imagefile   = filename,
                    pdffile     = filepdf,
                    ) #end of loading up all the old values
            return render_template('editpost.html',
                title       = 'Edit Post',
                form        = form,
                but         = but,
                butb        = butb,
                role        = role,
                groups      = tgroups,
                gbut        = [form.postgroup.data],
                postnum     = postn,
                imagefile   = filename,
                pdffile     = filepdf,
                ) #end of loading up all the old values
            #clear the fields

    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in EDITPOST",
            errstring = str(e),
            user      = tuser,
            place     = spot)
        subject = "Exception Error! In EDITPOST"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@edit.route('/useredit', methods=['GET', 'POST'])
@fresh_login_required
def useredit():
    userid      = current_user.get_id()
    title       = "User Edit"
    form        = UserForm()
    spot = 10
    but   = current_app.config['DEFAULTBUTTON1']
    if current_user.is_authenticated:
        if current_user.role > 4:
            but[5] = 'food'
        if current_user.role > 49:
            but[5] = 'adminhome'
            #print 'admin on front page'
    else:
        but[5] = 'register'
    #post method is when this is submitted, Get is original click
    try:
        if request.method == 'POST':
            if form.is_submitted():
                #db.session.add(post)
                #db.session.commit()
                #db.session.query(Post).filter(Post.id == gevent_id):
                # update the form info and then update the database
                #print 'this is finally goin to a work', form.hidden_id.data, 'ah ha'
                #send form to the database except for password
                user = User.query.filter(User.id == userid).first_or_404()
                #print 'Have set the user data here'
                #user.id             = form.hidden_id.data
                #user.nickname       = form.nickname.data
                user.title           = form.title.data
                user.street          = form.street.data
                user.city            = form.city.data
                user.state_prov      = form.state_prov.data
                user.country         = form.country.data
                user.postalcode      = form.postalcode.data
                user.weblink         = form.weblink.data
                user.weblink_title   = form.weblink_title.data
                user.phone1          = form.phone1.data
                user.phone2          = form.phone2.data
                user.fax             = form.fax.data
                user.secret_question = form.secret_question.data
                user.secret_answer   = form.secret_answer.data
                user.personal_text   = form.personal_text.data
                db.session.commit()
                flash('SUCCESS! User has been Updated!')
                return render_template('useredit.html',
                    but = but,
                    form = form
                    )
        guser = User.query.filter(User.id == userid).first_or_404()
        #print 'Setting up form'
        form.id.data              = guser.id
        form.hidden_id.data       = str(guser.id)
        form.nickname.data        = guser.nickname
        form.title.data           = guser.title
        form.first_name.data      = guser.first_name
        form.initial.data         = guser.initial
        form.last_name.data       = guser.last_name
        form.avatar.data          = guser.avatar
        form.street.data          = guser.street
        form.city.data            = guser.city
        form.state_prov.data      = guser.state_prov
        form.country.data         = guser.country
        form.postalcode.data      = guser.postalcode
        form.image.data           = guser.image
        form.weblink.data         = guser.weblink
        form.weblink_title.data   = guser.weblink_title
        form.ip_list.data         = guser.ip_list
        form.phone1.data          = guser.phone1
        form.phone2.data          = guser.phone2
        form.fax.data             = guser.fax
        form.email.data           = guser.email
        form.hide_email.data      = guser.hide_email
        form.karma_bad.data       = guser.karma_bad
        form.karma_good.data      = guser.karma_good
        form.messages.data        = guser.messages
        form.passwd.data          = guser.passwd
        form.trust.data           = guser.trust
        form.role.data            = guser.role
        form.first_date.data      = guser.first_date
        form.last_seen.data       = guser.last_seen
        form.last_ip.data         = guser.last_ip
        form.secret_question.data = guser.secret_question
        form.secret_answer.data   = guser.secret_answer
        form.validation_code.data = guser.validation_code
        form.status.data          = guser.status
        form.personal_text.data   = guser.personal_text
        form.confirm_email.data   = guser.confirm_email

        return render_template('useredit.html',
            but = but,
            form = form
            )
    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in USEREDIT",
            errstring = str(e),
            user      = tuser,
            place     = spot)
        subject = "Exception Error! In EDITPOST"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@edit.route('/venueedit', methods=['GET', 'POST'])
@login_required
def venueedit():
    #if current_user.role < 50:
    #    return redirect('')
    tuser = "none"
    spot = 5
    but   = current_app.config['DEFAULTBUTTON1']
    if current_user.is_authenticated:
        if current_user.role > 4:
            but[5] = 'food'
        if current_user.role > 49:
            but[5] = 'adminhome'
            #print 'admin on front page'
    else:
        but[5] = 'register'
    title       = "Edit Venue"
    form        = VenueForm()
    city        = current_app.config['CITY']
    state       = current_app.config['STATE']
    gmaps       = googlemaps.Client(key = current_app.config['GOOGLE_MAP_KEY'])

    spot = 10
    try: # if get first put the database data onto the form and display
         # if post check the request for "edit" edit comes from venpage as a hidden value contains the postnumber
        postnum = request.form['postnum']
        if request.values.get('edit'): # got here as a request from venpage and want to populate the page

            # print "Edit Here", request.values.get('edit')
            # print   request.form['postnum']
            spot = 15
            pge = db.session.query(Venue).filter(Venue.id == request.form['postnum']).first()
            if pge == None:
                flash('That post is not found!')
                return redirect('home.index')

            # print colorize('PGE Image0', ansi = 3), pge.image0

            spot = 17
            # print "pge.id  ", pge.id
            form.ident.data           = pge.id
            form.name.data            = pge.name
            form.vtype.data           = pge.vtype
            form.logo.data            = pge.logo
            form.street.data          = pge.street
            form.city.data            = pge.city
            form.state_prov.data      = pge.state_prov
            form.country.data         = pge.country
            form.postalcode.data      = pge.postalcode
            form.phone1.data          = pge.phone1
            form.phone2.data          = pge.phone2
            form.fax.data             = pge.fax
            form.email.data           = pge.email
            form.hide_email.data      = pge.hide_email
            form.title_image.data     = pge.title_image
            form.image.data           = pge.image
            form.weblink.data         = pge.weblink
            form.weblink_title.data   = pge.weblink_title
            form.lodging.data         = pge.lodging
            form.meeting_rooms.data   = pge.meeting_rooms
            form.room_size.data       = pge.room_size
            form.vip_service.data     = pge.vip_service
            form.other_services.data  = pge.other_services
            form.max_occupancy.data   = pge.max_occupancy
            form.googlemap.data       = pge.googlemap
            form.audio.data           = pge.audio
            form.visual.data          = pge.visual
            form.lighting.data        = pge.lighting
            form.stage.data           = pge.stage
            form.dance.data           = pge.dance
            form.dressing_rooms.data  = pge.dressing_rooms
            form.business_centre.data = pge.business_centre
            form.internet.data        = pge.internet
            form.storage.data         = pge.storage
            form.loading_dock.data    = pge.loading_dock
            form.showers.data         = pge.showers
            form.airconditioning.data = pge.airconditioning
            form.handicap.data        = pge.handicap
            form.parking.data         = pge.parking
            form.food_service.data    = pge.food_service
            form.kitchen.data         = pge.kitchen
            form.dining_room.data     = pge.dining_room
            form.bar.data             = pge.bar
            form.licence.data         = pge.licence
            form.price.data           = pge.price
            form.body.data            = pge.body
            form.secret_question.data = pge.secret_question
            form.secret_answer.data   = pge.secret_answer
            form.role.data            = pge.role
            form.owner.data           = pge.owner
            if pge.image0:
                image0   = '/static/uploads/venue/' + pge.imagedatename + '/' + 'thumbs' + '/' + os.path.basename(pge.image0)
                #print colorize(image0, ansi = 11)
            else:
                image0 = ''
            if pge.image1:
                image1   = '/static/uploads/venue/' + pge.imagedatename + '/' + 'thumbs' + '/' + os.path.basename(pge.image1)
            else:
                image1 = ''
            if pge.image2:
                image2   = '/static/uploads/venue/' + pge.imagedatename + '/' + 'thumbs' + '/' + os.path.basename(pge.image2)
            else:
                image2 = ''
            if pge.image3:
                image3   = '/static/uploads/venue/' + pge.imagedatename + '/' + 'thumbs' + '/' + os.path.basename(pge.image3)
            else:
                image3 = ''
            if pge.image4:
                image4   = '/static/uploads/venue/' + pge.imagedatename + '/' + 'thumbs' + '/' + os.path.basename(pge.image4)
            else:
                image4 = ''
            if pge.image5:
                image5   = '/static/uploads/venue/' + pge.imagedatename + '/' + 'thumbs' + '/' + os.path.basename(pge.image5)
            else:
                image5 = ''
            if pge.image6:
                image6   = '/static/uploads/venue/' + pge.imagedatename + '/' + 'thumbs' + '/' + os.path.basename(pge.image6)
            else:
                image6 = ''
            if pge.image7:
                image7   = '/static/uploads/venue/' + pge.imagedatename + '/' + 'thumbs' + '/' + os.path.basename(pge.image7)
            else:
                image7 = ''
            if pge.image8:
                image8   = '/static/uploads/venue/' + pge.imagedatename + '/' + 'thumbs' + '/' + os.path.basename(pge.image8)
            else:
                image8 = ''
            if pge.image9:
                image9   = '/static/uploads/venue/' + pge.imagedatename + '/' + 'thumbs' + '/' + os.path.basename(pge.image9)
            else:
                image9 = ''
            if pge.image10:
                image10   = '/static/uploads/venue/' + pge.imagedatename + '/' + 'thumbs' + '/' + os.path.basename(pge.image10)
            else:
                image10 = ''
            if pge.image11:
                image11   = '/static/uploads/venue/' + pge.imagedatename + '/' + 'thumbs' + '/' + os.path.basename(pge.image11)
            else:
                image11 = ''
            #print "image", pge.image
            spot = 20
            # have to check for new image
            return render_template('venueedit.html',
                title    = 'Edit Venue',
                form     = form,
                but      = but,
                image0   = image0,
                image1   = image1,
                image2   = image2,
                image3   = image3,
                image4   = image4,
                image5   = image5,
                image6   = image6,
                image7   = image7,
                image8   = image8,
                image9   = image9,
                image10  = image10,
                image11  = image11,
                gbut     = [pge.vtype],

                postnum = request.form['postnum'],
                ) #end of loading up all the old values

        else: # got here as a post from the venueedit page check the values, clean and update the database
            tmpconfig = current_app.config.get('UPLOADED_VENUE_IMAGES_DEST')
            # newfolder = datetime.datetime.today().strftime("%Y-%m-%d") # not needed as images are already done
            # print "Got Here  ", request.form['postnum'], "  wowser"
            if form.validate(): # the difficult one is the image
                #print "spot is 30 Validated"
                spot = 30
                #print request.form['postnum'], '  ', form.name.data
                ven = db.session.query(Venue).filter(Venue.id == request.form['postnum']).first_or_404()
                spot = 50
                #print request.files.get('image'), " Image"
                spot = 52
                # check to see if image is different than image file
                # if different replace image in database
                filename = ven.image
                try:
                    if request.files.get('image'):
                        # print "Image  ", request.files.get('image')
                        ext = os.path.splitext(request.files.get('image').filename)
                        # create a new folder each day
                        # print ext, type(ext)
                        if not os.path.isdir(tmpconfig + '/' + newfolder):
                            os.mkdir(tmpconfig + '/' + newfolder)
                        spot = 55
                        # print "here at 55", tmpconfig + '/' + newfolder
                        # generate random name for file
                        tmpname = str(uuid.uuid4().hex)
                        # save file to disk
                        filename = images.save(request.files.get('image'), folder = tmpconfig + '/' + newfolder + '/', name = tmpname + ".")
                        filename = "/static/uploads/venue/images/" + newfolder + '/' + tmpname + ext[1]
                        #print "Filename  ", filename
                    else:
                        # keep the old filename and do nothing
                        spot = 57
                        pass
                except:
                    #print "FALTED HERE"
                    spot = 59
                    filename = ""

                spot = 60
                #print spot
                try: # get the google map location
                    tmp = form.street.data
                    if tmp != '':
                        if form.city.data != '':
                            tmp = tmp + ', ' + form.city.data + ', ' + state
                        else:
                            tmp = tmp + ', ' + city + ', ' + state
                    else:
                        tmp = city + ', ' + state
                    geocode_result = gmaps.geocode(tmp)
                    place = geocode_result[0]['place_id']
                except: # default to whatever city place_id
                    spot = 70
                    place = "ChIJcWGw3Ytzj1QR7Ui7HnTz6Dg"

                #standard buttons
                if form.body.data != "" or form.body.data != None:
                    bleachbody = bleach.clean(unicode(form.body.data).encode("utf-8"), strip = True, tags=['b',
                        'strong', 'em', 'blockquote', 'a', 'blockquote', 'p', 'i', 'li', 'ul', 'br', 'ol' 'strike', 'u', 'sub', 'sup', 'img',
                        'table', 'tr', 'td', 'thead', 'th', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'button', 'center', 'col', 'colgroup', 'font',
                        'hr', 'link'])
                    instring  = re.compile(re.escape('fuck'), re.IGNORECASE)
                    instringa = instring.sub('....', bleachbody)
                    instring  = re.compile(re.escape('shit'), re.IGNORECASE)
                    instringb = instring.sub('....', instringa)
                    instring  = re.compile(re.escape('cunt'), re.IGNORECASE)
                    instringc = instring.sub('....', instringb)
                    bleachbody = instringc
                else:
                    bleachbody = ""

                ven.ident           = request.form['postnum']
                ven.name            = thwart(unicode(form.name.data).encode("utf-8"))
                ven.vtype           = thwart(unicode(form.vtype.data).encode("utf-8")) #venue type
                ven.logo            = thwart(unicode(form.logo.data).encode("utf-8")) # logo to be added later
                ven.street          = thwart(unicode(form.street.data).encode("utf-8"))
                ven.city            = thwart(unicode(form.city.data).encode("utf-8"))
                ven.state_prov      = thwart(unicode(form.state_prov.data).encode("utf-8"))
                ven.country         = thwart(unicode(form.country.data).encode("utf-8"))
                ven.postalcode      = thwart(unicode(form.postalcode.data).encode("utf-8"))
                ven.phone1          = thwart(unicode(form.phone1.data).encode("utf-8"))
                ven.phone2          = thwart(unicode(form.phone2.data).encode("utf-8"))
                ven.fax             = thwart(unicode(form.fax.data).encode("utf-8"))
                ven.email           = thwart(unicode(form.email.data).encode("utf-8"))
                ven.hide_email      = thwart(unicode(form.hide_email.data).encode("utf-8"))
                ven.title_image     = thwart(unicode(form.title_image.data).encode("utf-8")) # might not use this
                ven.image           = filename
                ven.weblink         = thwart(unicode(form.weblink.data).encode("utf-8")),
                ven.weblink_title   = thwart(unicode(form.weblink_title.data).encode("utf-8")),# might not use this
                ven.lodging         = thwart(unicode(form.lodging.data).encode("utf-8"))
                ven.meeting_rooms   = thwart(unicode(form.meeting_rooms.data).encode("utf-8"))
                ven.room_size       = thwart(unicode(form.room_size.data).encode("utf-8"))
                ven.vip_service     = thwart(unicode(form.vip_service.data).encode("utf-8"))
                ven.other_services  = thwart(unicode(form.other_services.data).encode("utf-8"))
                ven.max_occupancy   = thwart(unicode(form.max_occupancy.data).encode("utf-8"))
                ven.googlemap       = place
                ven.audio           = thwart(unicode(form.audio.data).encode("utf-8"))
                ven.visual          = thwart(unicode(form.visual.data).encode("utf-8"))
                ven.lighting        = thwart(unicode(form.lighting.data).encode("utf-8"))
                ven.stage           = thwart(unicode(form.stage.data).encode("utf-8"))
                ven.dance           = thwart(unicode(form.dance.data).encode("utf-8"))
                ven.dressing_rooms  = thwart(unicode(form.dressing_rooms.data).encode("utf-8"))
                ven.business_centre = thwart(unicode(form.business_centre.data).encode("utf-8"))
                ven.internet        = thwart(unicode(form.internet.data).encode("utf-8"))
                ven.storage         = thwart(unicode(form.storage.data).encode("utf-8"))
                ven.loading_dock    = thwart(unicode(form.loading_dock.data).encode("utf-8"))
                ven.showers         = thwart(unicode(form.showers.data).encode("utf-8"))
                ven.airconditioning = thwart(unicode(form.airconditioning.data).encode("utf-8"))
                ven.handicap        = thwart(unicode(form.handicap.data).encode("utf-8"))
                ven.parking         = thwart(unicode(form.parking.data).encode("utf-8"))
                ven.food_service    = thwart(unicode(form.food_service.data).encode("utf-8"))
                ven.kitchen         = thwart(unicode(form.kitchen.data).encode("utf-8"))
                ven.dining_room     = thwart(unicode(form.dining_room.data).encode("utf-8"))
                ven.bar             = thwart(unicode(form.bar.data).encode("utf-8"))
                ven.licence         = thwart(unicode(form.licence.data).encode("utf-8"))
                ven.price           = thwart(unicode(form.price .data).encode("utf-8"))
                ven.timetouched     = datetime.datetime.now()
                ven.body            = bleachbody
                #ven.owner           = current_user.id
                db.session.commit()
        spot = 100
        #print colorize("Here in editvenue", ansi=11)
        return redirect('venpage/' + postnum)
    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in VENUEEDIT",
            errstring = str(e),
            user      = tuser,
            place     = spot)
        subject = "Exception Error! In VENUEEDIT"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)

        return(str(e))

@edit.route('/venueimageedit/<int:postnum>')
@login_required
def venueimageedit(postnum):
    # find record from postnum
    # first check to see if allowed take postnum and check owner
    spot = 10
    form = VenueForm
    try:
        tuser = current_user.id
        pic = []
        #print colorize('here in venue image edit', ansi = 11)
        #print "here in venueimageedit", postnum
        venpost = Venue.query.filter(Venue.id == postnum).first()
        if venpost == None:
            flash('Venue Images Not Found!')
            return redirect('home.index')
        spot = 40
        #print colorize(type(venpost), ansi = 11), venpost.image0
        spot = 50
        if venpost.image0: # there are images here if image0 exists build the pic list
            newpath = '/static/uploads/venue/' + venpost.imagedatename +  '/thumbs/'
            if venpost.image0:
                pic.append(newpath + os.path.basename(venpost.image0))
            if venpost.image1:
                pic.append(newpath + os.path.basename(venpost.image1))
            if venpost.image2:
                pic.append(newpath + os.path.basename(venpost.image2))
            if venpost.image3:
                pic.append(newpath + os.path.basename(venpost.image3))
            if venpost.image4:
                pic.append(newpath + os.path.basename(venpost.image4))
            if venpost.image5:
                pic.append(newpath + os.path.basename(venpost.image5))
            if venpost.image6:
                pic.append(newpath + os.path.basename(venpost.image6))
            if venpost.image7:
                pic.append(newpath + os.path.basename(venpost.image7))
            if venpost.image8:
                pic.append(newpath + os.path.basename(venpost.image8))
            if venpost.image9:
                pic.append(newpath + os.path.basename(venpost.image9))
            if venpost.image10:
                pic.append(newpath + os.path.basename(venpost.image10))
            if venpost.image11:
                pic.append(newpath + os.path.basename(venpost.image11))
        else:
            #no images
            pass
        return render_template('venueimageedit.html', pic = pic, postnum = postnum)
    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in VENUEimageEDIT",
            errstring = str(e),
            user      = tuser,
            place     = spot)
        subject = "Exception Error! In VENUEEDIT"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@edit.route('/edit_pic', methods=['POST', 'GET'])# This uploads the new pics to the database and the folder
@login_required
def editpic():# The folder date is found from the database imagedatename fill in the holes
    but   = current_app.config['DEFAULTBUTTON1']
    form  = VenueForm()
    tuser = current_user.get_id()
    postnum = request.form.get('postnum')
    spot = 10
    #print colorize("here in editpic", ansi = 11)
    #print colorize('request.files.items ', ansi = 11), colorize(request.files.items, ansi = 123)
    #print colorize('request.query_string ', ansi = 11), colorize(request.query_string, ansi = 83)
    #print colorize('request.form.get ', ansi = 11), colorize(request.form.get('file'), ansi = 13)
    #print request.form.get('postnum')
    #print colorize("urlparse ", ansi = 11), urlparse(request.url).query
    try:# get the data from the database, check owner,
        pge = db.session.query(Venue).filter(Venue.id == request.form['postnum']).first() # this is the venue information
        # check to see if allowed
        if current_user.get_id() == pge.owner or current_user.role > 49: # can continue
            spot = 25
            if request.files.items():
                spot = 40
                dirpath        = current_app.config.get('UPLOADED_VENUE_IMAGES_DEST')  + pge.imagedatename + '/images/'
                thumbpath      = current_app.config.get('UPLOADED_VENUE_IMAGES_DEST')  + pge.imagedatename + '/thumbs/'
                images         = UploadSet('images', IMAGES)

                spot = 50
                for key, f in request.files.items():
                    infilename, extension = os.path.splitext(f.filename)
                    extension = extension.lower()
                    newname = str(uuid.uuid4().hex) # new unique name
                    spot = 70
                    if key.startswith('file'): # have a file here, make a new name, into the imagedatename named folder, first empty table cell
                        f.save(os.path.join(dirpath, newname + extension))
                        filename = dirpath + newname + extension
                        #print colorize("Updating ", ansi = 80), filename
                        # convert the image to thumbnail

                        imagebig = Image.open(filename)
                        size = (120, 120)
                        spot = 90
                        imagebig.thumbnail(size, Image.ANTIALIAS)
                        spot = 100
                        tpath = thumbpath + newname + extension
                        tpath = str(tpath)
                        #print colorize(tpath, ansi = 11)
                        imagebig.save(tpath)
                        spot = 120
                        #print colorize('/static/uploads/venue/' + pge.imagedatename + '/images/'+ newname + extension, ansi = 150)
                        # save filename to database, find a hole
                        if pge.image0 == '':
                            pge.image0 = '/static/uploads/venue/' + pge.imagedatename + '/images/'+ newname + extension
                        elif pge.image1 == '':
                            pge.image1 = '/static/uploads/venue/' + pge.imagedatename + '/images/'+ newname + extension
                        elif pge.image2 == '':
                            pge.image2 = '/static/uploads/venue/' + pge.imagedatename + '/images/'+ newname + extension
                        elif pge.image3 == '':
                            pge.image3 = '/static/uploads/venue/' + pge.imagedatename + '/images/'+ newname + extension
                        elif pge.image4 == '':
                            pge.image4 = '/static/uploads/venue/' + pge.imagedatename + '/images/'+ newname + extension
                        elif pge.image5 == '':
                            pge.image5 = '/static/uploads/venue/' + pge.imagedatename + '/images/'+ newname + extension
                        elif pge.image6 == '':
                            pge.image6 = '/static/uploads/venue/' + pge.imagedatename + '/images/'+ newname + extension
                        elif pge.image7 == '':
                            pge.image7 = '/static/uploads/venue/' + pge.imagedatename + '/images/'+ newname + extension
                        elif pge.image8 == '':
                            pge.image8 = '/static/uploads/venue/' + pge.imagedatename + '/images/'+ newname + extension
                        elif pge.image9 == '':
                            pge.image9 = '/static/uploads/venue/' + pge.imagedatename + '/images/'+ newname + extension
                        elif pge.image10 == '':
                            pge.image10 = '/static/uploads/venue/' + pge.imagedatename + '/images/'+ newname + extension
                        elif pge.image11 == '':
                            pge.image11 = '/static/uploads/venue/' + pge.imagedatename + '/images/'+ newname + extension
                        else:
                            flash('Something went wrong')
                            break
                        db.session.commit()
            else:
                pass
                #print "No File"
        else:
            flash('You do not have permissions to do this!')
            return redirect("home.index")
        #print colorize('Finished edit_pic ', ansi = 9)
        spot = 200
        return '', 204

    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in EDIT edit_pic",
            errstring = str(e),
            user      = tuser,
            place     = spot)
        subject = "Exception Error! In EDIT edit_pic"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

















