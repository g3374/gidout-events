#remove_html_markup csrf_error /_vgetgroup /_vgroupchoice /_venuimage /postvenue
#/upload_pic /dropimg /details/<int:postnum> /venpage/<int:gevent_id> /venue/<page>

from .              import venue
from flask          import Flask, render_template, request, flash, abort, jsonify, redirect, url_for, session, current_app
from flask_wtf.csrf import CSRFProtect, CSRFError
from calendar       import Calendar, HTMLCalendar
import datetime, time, uuid, socket, math
from xtermcolor     import colorize
from gidout            import  db
from gidout.models       import Venue, Vgidgroup, Vedge
from gidout.forms        import VenueForm
import os, math, bleach, re
from flask_login    import login_required, UserMixin, current_user
from gidout.admin.routes import load_user
from werkzeug.utils import secure_filename
from flask_uploads  import UploadSet, IMAGES, configure_uploads, patch_request_class
from flask_dropzone import Dropzone
import googlemaps
from MySQLdb        import escape_string as thwart
from flask_mail     import Mail
from gidout.entry.routes import send_email
from gidout.pages.routes import getIPx
from operator       import attrgetter
from PIL            import Image


def remove_html_markup(s):
    tag = False
    quote = False
    out = ""
    for c in s:
        if c == '<' and not quote:
            tag = True
        elif c == '>' and not quote:
            tag = False
        elif (c == '"' or c == "'") and tag:
            quote = not quote
        elif not tag:
            out = out + c
    return out

@venue.errorhandler(CSRFError)
def csrf_error(e):
    return e.description, 400

@venue.route('/_vgetgroup', methods=['GET', 'POST'])
def vgetgroup():
    spot = 5
    try:
        up = []
        dn = []
        b = request.args.get('a', type=str)
        a = b.split(" ",1)[0]
        edges = db.session.query(Vgidgroup).filter(Vgidgroup.name == a).first()
        spot = 25
        for it in edges.higher_neighbors():
            up.append(it.name)
        #sort alphabetically
        #print "early up", up
        up = sorted(up)
        #print "Up ", up
        spot = 50
        for it in edges.lower_neighbors():
            dn.append(it.name)
        #sort alphabetically
        dn = sorted(dn)
        #print "Down ", dn
        spot = 100
        return jsonify(up = up, dn = dn)
    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in _vgetgroup ",
            errstring = "Error String:  " + repr(e),
            user      = "tuser",
            place     = spot)
        subject = "Exception Error! In Page"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@venue.route('/_vgroupchoice')
def vgroupchoice():
    try:
        # use a try except loop to validate entry gets the nodes in the groups
        # print request.query_string, "request query string"
        b = request.args.get('a', type=str)
        a = b.split(" ",1)[0]
        # print b, "request args"
        # print a, "after split"
        up  = []
        dn  = []
        dn1 = []
        dn2 = []
        dn3 = []
        dn4 = []
        dn5 = []
        count = 0
        spot = 5
        group = db.session.query(Vgidgroup).filter(Vgidgroup.name == a).first()
        for it in group.higher_neighbors():
            up.append(it.name)
        #sort alphabetically
        up = sorted(up)
        # print "UP ", up
        # print 'Upper', colorize(up, ansi=11)
        for it in group.lower_neighbors():
            dn.append(it.name)
        dn = sorted(dn)
        # print "DOWN ", dn
        str1 = ''
        st1 = '<tr>'
        st2 = '<td  class="grouplink" id="'
        st3 = '" style="width:60px;height:60px;font-size:7.00pt;background-color:#E69999"><img src="/static/images/venue/buttons/'
        st3a = '" style="width:60px;height:60px;font-size:7.00pt;background-color:#96EE99"><img src="/static/images/venue/buttons/'
        st4 = '.png" title="'
        st5 = '" /></a>'
        st6 = '</td>'
        st7 = '</tr>'
        st8 = '</tbody>'
        str1+= '<tbody id="grouptbody"><tr>'
        count = 0
        spot = 10
        for x in up:
            count +=1
            #print 'x  ', x
            str1 += st2
            str1 += x
            str1 += st3
            str1 += x
            str1 += st4
            str1 += x
            str1 += st5
            str1 += x
            str1 += st6
            #print str1
        for y in dn:
            spot = 50
            count += 1
            #print 'y ', y
            if count >= 15:
                if count == 15:
                    str1 += st7
                    str1 += st1
                if count >= 29:
                    if count == 29:
                        str1 += st7
                        str1 += st1
                    if count >= 43:
                        if count == 43:
                            str1 += st7
                            str1 += st1
                        if count >= 57:
                            if count == 57:
                                str1 += st7
                                str1 += st1
                            str1 += st2
                            str1 += y
                            str1 += st3a
                            str1 += y
                            str1 += st4
                            str1 += y
                            str1 += st5
                            str1 += y
                            str1 += st6
                        else:
                            str1 += st2
                            str1 += y
                            str1 += st3a
                            str1 += y
                            str1 += st4
                            str1 += y
                            str1 += st5
                            str1 += y
                            str1 += st6
                    else:
                        str1 += st2
                        str1 += y
                        str1 += st3a
                        str1 += y
                        str1 += st4
                        str1 += y
                        str1 += st5
                        str1 += y
                        str1 += st6
                else:
                    str1 += st2
                    str1 += y
                    str1 += st3a
                    str1 += y
                    str1 += st4
                    str1 += y
                    str1 += st5
                    str1 += y
                    str1 += st6
            else:
                str1 += st2
                str1 += y
                str1 += st3a
                str1 += y
                str1 += st4
                str1 += y
                str1 += st5
                str1 += y
                str1 += st6
        str1 += st8
        str1 += st7
        spot =  100
        #print str1, "   str1"
        return jsonify(result=str1)
    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in _vgroupchoice ",
            errstring = "Error String:  " + repr(e),
            user      = "tuser",
            place     = spot)
        subject = "Exception Error! In Page"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

# the purpose of _venueimage is to update the images in postvenue.html
@venue.route('/_venuimage', methods=['GET', 'POST'])
def venuimage():
    # print colorize('Here in _venueimage', ansi = 190)
    spot = 10
    try:
        newlist = []
        # here I use the data result to find the images then I make the thumbs and build the html to display
        # print colorize("Made it into Venue Image json section yessssssssss ", ansi=11)
        # a in the request contains the tracking number get it to find the appropriate post
        b = request.args.get('a', type=str)
        # use b from the database to get the images
        ven = Venue.query.filter(Venue.track == b).first()
        # print colorize("Ven.image0", ansi = 196)
        # print ven.image0
        # print colorize("Ven.image0", ansi = 196)
        spot =30
        if ven.image0:
            newlist.append(ven.image0)
        if ven.image1:
            newlist.append(ven.image1)
        if ven.image2:
            newlist.append(ven.image2)
        if ven.image3:
            newlist.append(ven.image3)
        if ven.image4:
            newlist.append(ven.image4)
        if ven.image5:
            newlist.append(ven.image5)
        if ven.image6:
            newlist.append(ven.image6)
        if ven.image7:
            newlist.append(ven.image7)
        if ven.image8:
            newlist.append(ven.image8)
        if ven.image9:
            newlist.append(ven.image9)
        if ven.image10:
            newlist.append(ven.image10)
        if ven.image11:
            newlist.append(ven.image11)
        # print "newlist path from database", newlist[0]
        # make sure to return the tracking number
        strng  = '<input type="hidden" id="tracker" name="tracker" value="' + b +'"/><div id="topimg" class="w3-row"><fieldset><legend>Uploaded Photos:</legend><div class="row">'
        strng1 = '<div class="row">'
        strng3 = '</div>'
        strng4 = '</fieldset>'
        thumbpath = "/static/uploads/venue/"

        # thumbpath is base thumbpath plus datefolder plus thumbs
        # print "thumbpath", thumbpath
        spot = 40
        # build the thumbs, save the thumbs, insert the thumbs into html
        # the new folder by date contains images pdf and thumbs

        x = 0
        for li in newlist:
            bigfilename = os.path.basename(li) # this is the filename
            thumbdest = thumbpath + ven.imagedatename + '/' + 'thumbs' + '/' + bigfilename
            # print colorize("thumbdest", ansi = 10), colorize (thumbdest, ansi = 11)
            strng = strng + '<div class="w3-col s2 padsm"><img class="cursor imgbutton" src="%s" onclick="currentSlide(1)" alt="Pictureone"/></div>' %(thumbdest)
            if x == 5:
                strng += strng3
                strng += strng1
            x += 1
        strng += strng3
        strng += strng3
        # print colorize(strng, ansi = 11)
        spot = 100
        return jsonify(result = strng)
    except Exception as e:
        flash("Something Failed in postvenue")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in Post Venue",
            errstring = str(e),
            user      = "admin",
            place     = spot)
        subject = "Exception Error!"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@venue.route('/postvenue', methods=['GET', 'POST'])
@login_required
def postvenue():
    but   = current_app.config['DEFAULTBUTTON1']
    if current_user.role < 20:
        flash('You need to have advanced features to go there.')
        return redirect(url_for('home.index'))
    # setup session to track status of upload_pic and track the database
    # do it by tracking image0
    if current_user.is_authenticated:
        tuser = current_user.id
        if current_user.role > 49:
            but[5] = "adminhome"
        else:
            but[5] = "food"
    title = "Post Venue"
    form  = VenueForm()
    form.ident.data = tuser
    tracker = ''
    # print colorize("Post Venue setup form", ansi=9), request.method
    spot = 10
    try:
        spot = 15
        if request.method == 'POST':
            tracker = request.form.get('tracker')  # overload tracker
            spot = 20
            # print colorize("Here Beginning Posted here is tracker", ansi=11), tracker
            if form.validate(): # form data is accepted, process bleach and then save
                city         = current_app.config['CITY'] # get configuration value
                state        = current_app.config['STATE']
                spot = 30
                #print colorize("Form is Validated", ansi = 14)
                # Got here from submit: bleach and clean the html
                # clean the body
                bleachbody   = bleach.clean(unicode(form.body.data).encode("utf-8"), strip = True, tags=['b',
                            'strong', 'em', 'blockquote', 'a', 'blockquote', 'p', 'i', 'li', 'ul', 'br', 'ol' 'strike', 'u', 'sub', 'sup', 'img',
                            'table', 'tr', 'td', 'thead', 'th', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'button', 'center', 'col', 'colgroup', 'font',
                            'hr', 'link'])
                instring     = re.compile(re.escape('fuck'), re.IGNORECASE)
                instringa    = instring.sub('....', bleachbody)
                instring     = re.compile(re.escape('shit'), re.IGNORECASE)
                instringb    = instring.sub('....', instringa)
                instring     = re.compile(re.escape('cunt'), re.IGNORECASE)
                instringc    = instring.sub('....', instringb)
                bleachbody   = instringc
                newfolder    = datetime.datetime.today().strftime("%Y-%m-%d")
                tmpconfig    = current_app.config.get('UPLOADED_VENUE_IMAGES_DEST') # basedir + '/app/static/uploads/venue/'
                try:
                    spot = 35
                    if request.files.get('image').filename != '': # is the new filename here
                        ext = os.path.splitext(request.files.get('image').filename) #extension of the filename
                        if not os.path.isdir(tmpconfig + '/' + newfolder): # check to see if date folder is there
                            os.mkdir(tmpconfig + newfolder) # make the new folder and sub folders
                            os.mkdir(tmpconfig + newfolder + '/' + 'images')
                            os.mkdir(tmpconfig + newfolder + '/' + 'pdf')
                            os.mkdir(tmpconfig + newfolder + '/' + 'thumbs')
                        tmpname = str(uuid.uuid4().hex) # the new image name that is unique
                        filename = images.save(request.files['image'], folder = tmpconfig + newfolder + '/' + 'images' + '/' , name = tmpname + ".")
                        filename = "/static/uploads/venue/" + newfolder + '/' + 'images' + '/' + tmpname + ext[1]
                        spot = 40
                        # print colorize("got here made a directory  ", ansi = 14), filename
                    else:
                        filename = ""
                except:
                    filename  = ""
                try:
                    tmp = form.street.data
                    if tmp != '':
                        if form.city.data != '':
                            tmp = tmp + ', ' + form.city.data + ', ' + state
                        else:
                            tmp = tmp + ', ' + city + ', ' + state
                    else:
                        tmp = city + ', ' + state
                    geocode_result = gmaps.geocode(tmp)
                    place = geocode_result[0]['place_id']
                except:
                    spot = 45
                    place = "ChIJcWGw3Ytzj1QR7Ui7HnTz6Dg"
                spot = 50
                if tracker:
                    db.session.query(Venue).filter(Venue.track == tracker).update({
                        'ident'           : thwart(unicode(form.ident.data).encode("utf-8")),
                        'name'            : thwart(unicode(form.name.data).encode("utf-8")),
                        'vtype'           : thwart(unicode(form.vtype.data).encode("utf-8")), #venue type
                        'logo'            : thwart(unicode(form.logo.data).encode("utf-8")), # logo to be added later
                        'street'          : thwart(unicode(form.street.data).encode("utf-8")),
                        'city'            : thwart(unicode(form.city.data).encode("utf-8")),
                        'state_prov'      : thwart(unicode(form.state_prov.data).encode("utf-8")),
                        'country'         : thwart(unicode(form.country.data).encode("utf-8")),
                        'postalcode'      : thwart(unicode(form.postalcode.data).encode("utf-8")),
                        'phone1'          : thwart(unicode(form.phone1.data).encode("utf-8")),
                        'phone2'          : thwart(unicode(form.phone2.data).encode("utf-8")),
                        'fax'             : thwart(unicode(form.fax.data).encode("utf-8")),
                        'email'           : thwart(unicode(form.email.data).encode("utf-8")),
                        'hide_email'      : thwart(unicode(form.hide_email.data).encode("utf-8")),
                        'title_image'     : thwart(unicode(form.title_image.data).encode("utf-8")), # might not use this
                        'image'           : filename,
                        'weblink'         : thwart(unicode(form.weblink.data).encode("utf-8")),
                        'weblink_title'   : thwart(unicode(form.weblink_title.data).encode("utf-8")), # might not use this
                        'lodging'         : thwart(unicode(form.lodging.data).encode("utf-8")),
                        'meeting_rooms'   : thwart(unicode(form.meeting_rooms.data).encode("utf-8")),
                        'room_size'       : thwart(unicode(form.room_size.data).encode("utf-8")),
                        'vip_service'     : thwart(unicode(form.vip_service.data).encode("utf-8")),
                        'other_services'  : thwart(unicode(form.other_services.data).encode("utf-8")),
                        'max_occupancy'   : thwart(unicode(form.max_occupancy.data).encode("utf-8")),
                        'googlemap'       : place,
                        'audio'           : thwart(unicode(form.audio.data).encode("utf-8")),
                        'visual'          : thwart(unicode(form.visual.data).encode("utf-8")),
                        'lighting'        : thwart(unicode(form.lighting.data).encode("utf-8")),
                        'stage'           : thwart(unicode(form.stage.data).encode("utf-8")),
                        'dance'           : thwart(unicode(form.dance.data).encode("utf-8")),
                        'dressing_rooms'  : thwart(unicode(form.dressing_rooms.data).encode("utf-8")),
                        'business_centre' : thwart(unicode(form.business_centre.data).encode("utf-8")),
                        'internet'        : thwart(unicode(form.internet.data).encode("utf-8")),
                        'storage'         : thwart(unicode(form.storage.data).encode("utf-8")),
                        'loading_dock'    : thwart(unicode(form.loading_dock.data).encode("utf-8")),
                        'showers'         : thwart(unicode(form.showers.data).encode("utf-8")),
                        'airconditioning' : thwart(unicode(form.airconditioning.data).encode("utf-8")),
                        'handicap'        : thwart(unicode(form.handicap.data).encode("utf-8")),
                        'parking'         : thwart(unicode(form.parking.data).encode("utf-8")),
                        'food_service'    : thwart(unicode(form.food_service.data).encode("utf-8")),
                        'kitchen'         : thwart(unicode(form.kitchen.data).encode("utf-8")),
                        'dining_room'     : thwart(unicode(form.dining_room.data).encode("utf-8")),
                        'bar'             : thwart(unicode(form.bar.data).encode("utf-8")),
                        'licence'         : thwart(unicode(form.licence.data).encode("utf-8")),
                        'price'           : thwart(unicode(form.price.data).encode("utf-8")),
                        'timecreated'     : datetime.datetime.now(),
                        'timetouched'     : datetime.datetime.now(),
                        'body'            : bleachbody,
                        'owner'           : current_user.id,
                        'imagedatename'   : newfolder,
                        'subvenue'        : thwart(unicode(form.subvenue.data).encode("utf-8")),
                        })
                else:
                    venue = Venue(
                        ident           = thwart(unicode(form.ident.data).encode("utf-8")),
                        name            = thwart(unicode(form.name.data).encode("utf-8")),
                        vtype           = thwart(unicode(form.vtype.data).encode("utf-8")), #venue type
                        logo            = thwart(unicode(form.logo.data).encode("utf-8")), # logo to be added later
                        street          = thwart(unicode(form.street.data).encode("utf-8")),
                        city            = thwart(unicode(form.city.data).encode("utf-8")),
                        state_prov      = thwart(unicode(form.state_prov.data).encode("utf-8")),
                        country         = thwart(unicode(form.country.data).encode("utf-8")),
                        postalcode      = thwart(unicode(form.postalcode.data).encode("utf-8")),
                        phone1          = thwart(unicode(form.phone1.data).encode("utf-8")),
                        phone2          = thwart(unicode(form.phone2.data).encode("utf-8")),
                        fax             = thwart(unicode(form.fax.data).encode("utf-8")),
                        email           = thwart(unicode(form.email.data).encode("utf-8")),
                        hide_email      = thwart(unicode(form.hide_email.data).encode("utf-8")),
                        title_image     = thwart(unicode(form.title_image.data).encode("utf-8")), # might not use this
                        image           = filename,
                        weblink         = thwart(unicode(form.weblink.data).encode("utf-8")),
                        weblink_title   = thwart(unicode(form.weblink_title.data).encode("utf-8")), # might not use this
                        lodging         = thwart(unicode(form.lodging.data).encode("utf-8")),
                        meeting_rooms   = thwart(unicode(form.meeting_rooms.data).encode("utf-8")),
                        room_size       = thwart(unicode(form.room_size.data).encode("utf-8")),
                        vip_service     = thwart(unicode(form.vip_service.data).encode("utf-8")),
                        other_services  = thwart(unicode(form.other_services.data).encode("utf-8")),
                        max_occupancy   = thwart(unicode(form.max_occupancy.data).encode("utf-8")),
                        googlemap       = place,
                        audio           = thwart(unicode(form.audio.data).encode("utf-8")),
                        visual          = thwart(unicode(form.visual.data).encode("utf-8")),
                        lighting        = thwart(unicode(form.lighting.data).encode("utf-8")),
                        stage           = thwart(unicode(form.stage.data).encode("utf-8")),
                        dance           = thwart(unicode(form.dance.data).encode("utf-8")),
                        dressing_rooms  = thwart(unicode(form.dressing_rooms.data).encode("utf-8")),
                        business_centre = thwart(unicode(form.business_centre.data).encode("utf-8")),
                        internet        = thwart(unicode(form.internet.data).encode("utf-8")),
                        storage         = thwart(unicode(form.storage.data).encode("utf-8")),
                        loading_dock    = thwart(unicode(form.loading_dock.data).encode("utf-8")),
                        showers         = thwart(unicode(form.showers.data).encode("utf-8")),
                        airconditioning = thwart(unicode(form.airconditioning.data).encode("utf-8")),
                        handicap        = thwart(unicode(form.handicap.data).encode("utf-8")),
                        parking         = thwart(unicode(form.parking.data).encode("utf-8")),
                        food_service    = thwart(unicode(form.food_service.data).encode("utf-8")),
                        kitchen         = thwart(unicode(form.kitchen.data).encode("utf-8")),
                        dining_room     = thwart(unicode(form.dining_room.data).encode("utf-8")),
                        bar             = thwart(unicode(form.bar.data).encode("utf-8")),
                        licence         = thwart(unicode(form.licence.data).encode("utf-8")),
                        price           = thwart(unicode(form.price.data).encode("utf-8")),
                        timecreated     = datetime.datetime.now(),
                        timetouched     = datetime.datetime.now(),
                        body            = bleachbody,
                        owner           = current_user.id,
                        imagedatename   = newfolder,
                        subvenue        = thwart(unicode(form.subvenue.data).encode("utf-8")),
                        )
                    db.session.add(venue)
                spot = 55
                db.session.commit()
                flash("Venue " + form.name.data + " has been saved")
                spot = 60
                # print "Comitted"
                return redirect(url_for("home.index"))
                # form info has been saved to the database
                #redirect to home
            else:
                flash('Form did not Validate')
                #print colorize("Form not Validated", ansi = 14)
        spot = 100
        # got here from get, just setup page
        return render_template('postvenue.html',
            title = title,
            tracker = tracker,
            form  = form,
            but   = but,
            gbut  = [ 'school', 'hotel', 'motel', 'convention', 'indoor', 'outdoor', 'restaurant' ]
            )
    except Exception as e:
        flash("Something Failed in postvenue")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in Post Venue",
            errstring = str(e),
            user      = tuser,
            place     = spot)
        subject = "Exception Error!"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

# -----------------------------------------------------------------------------------------------------------------------------------
@venue.route('/upload_pic', methods=['POST'])
@login_required
def upload_pic():
    # submit tries this first Upload the pics to directory, make thumbnails and record in database when done send OK204 to javascript
    # load pic names to database and load the tracker number to database
    # print "tracker ", request.form.get("tracker")
    # print colorize("***********", ansi = 120)
    try:
        tuser = current_user.id
        spot = 10
        # print colorize("inside upload pic", ansi = 11)
        # go through the list of photos
        # make a thumbnail, make the new folders for pics and thumbs update database
        tracknum = request.form.get("tracker")
        file_obj = request.files
        spot = 20
        if file_obj:
            imgdata = ['', '', '', '', '', '', '', '', '', '', '', '']
            spot = 25
            dirpath        = current_app.config.get('UPLOADED_VENUE_IMAGES_DEST') #/app/static/uploads/venue/
            spot =28
            newfolder      = datetime.datetime.today().strftime("%Y-%m-%d")
            ven = Venue()
            spot = 30
            x = 0
            for f in file_obj:
                img = request.files.get(f)
                infilename, extension = os.path.splitext(img.filename) #split the input into file and ext
                extension = extension.lower()
                # check for destination folders
                if not os.path.isdir(dirpath + newfolder):
                    os.mkdir(dirpath + newfolder)
                    os.mkdir(dirpath + newfolder + '/' + 'images')
                    os.mkdir(dirpath + newfolder + '/' + 'pdf')
                    os.mkdir(dirpath + newfolder + '/' + 'thumbs')
                    # print colorize('Just made new folders  ',ansi = 51)
                    # just made new folders
                    spot = 60
                newname = str(uuid.uuid4().hex) # new unique name
                # newname is the new name for the file
                # use pillow to make a thumbnail
                spot = 70
                filename = images.save(img, folder = dirpath + newfolder + '/' + 'images' + '/', name = newname + ".")
                # print (dirpath + newfolder + '/' + 'images' + '/')
                imagebig = Image.open(filename)
                spot = 76
                size = (120, 120)
                imagebig.thumbnail(size, Image.ANTIALIAS)
                spot = 77
                ipath = dirpath + newfolder + '/'+ 'thumbs' + '/' + newname + extension
                # print colorize(ipath, ansi = 11)
                ipath = str(ipath)
                imagebig.save(ipath)
                spot = 78
                filename = "/static/uploads/venue/" + newfolder + '/' + 'images' + '/' + newname + extension
                spot = 80
                imgdata[x] = filename
                x += 1
            # check to see if Session
            if tracknum:
                ven = Venue(
                    image0  = imgdata[0],
                    image1  = imgdata[1],
                    image2  = imgdata[2],
                    image3  = imgdata[3],
                    image4  = imgdata[4],
                    image5  = imgdata[5],
                    image6  = imgdata[6],
                    image7  = imgdata[7],
                    image8  = imgdata[8],
                    image9  = imgdata[9],
                    image10 = imgdata[10],
                    image11 = imgdata[11],
                    track   = tracknum,
                    imagedatename = newfolder
                    )
                session['imagetrack'] = imgdata[0]
                db.session.add(ven)
                db.session.commit()
                # save the filename to the database to a venue save a connector to the session
        else:
            # print colorize("No File", ansi = 220)
            spot = 90
        spot = 100
        return '', 204
    except Exception as e:
        flash("Something Failed in uploadpic")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in uploadpic",
            errstring = str(e),
            user      = tuser,
            place     = spot)
        subject = "Exception Error!"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@venue.route('/dropimg')
def dropimg():
    return render_template('dropimg.html', tracker = str(uuid.uuid4().hex))

@venue.route('/details/<int:postnum>', methods = ['GET', 'POST'])
def details(postnum):
    tuser = "unknown"
    title = 'Venue Details'
    # print colorize('Post Num ', ansi = 3), postnum
    but = current_app.config['DEFAULTBUTTON1']
    if current_user.is_authenticated:
        tuser = current_user.id
        if current_user.role > 49:
            but[5] = "adminhome"
        else:
            but[5] = "food"
    spot = 10
    try:
        pge = db.session.query(Venue).filter(Venue.id == postnum).first()
        if pge == None:
            flash ('Details not found')
            return redirect('home.index')
        spot = 20
        return render_template('details.html',
            postnum = postnum,
            title = title,
            but   = but,
            ident           = pge.ident,
            name            = pge.name,
            vtype           = pge.vtype,
            logo            = pge.logo,
            street          = pge.street,
            city            = pge.city,
            state_prov      = pge.state_prov,
            country         = pge.country,
            postalcode      = pge.postalcode,
            phone1          = pge.phone1,
            phone2          = pge.phone2,
            fax             = pge.fax,
            email           = pge.email,
            hide_email      = pge.hide_email,
            title_image     = pge.title_image,
            image           = pge.image,
            weblink         = pge.weblink,
            weblink_title   = pge.weblink_title,
            lodging         = pge.lodging,
            meeting_rooms   = pge.meeting_rooms,
            room_size       = pge.room_size,
            vip_service     = pge.vip_service,
            other_services  = pge.other_services,
            max_occupancy   = pge.max_occupancy,
            googlemap       = pge.googlemap,
            audio           = pge.audio,
            visual          = pge.visual,
            lighting        = pge.lighting,
            stage           = pge.stage,
            dance           = pge.dance,
            dressing_rooms  = pge.dressing_rooms,
            business_centre = pge.business_centre,
            internet        = pge.internet,
            storage         = pge.storage,
            loading_dock    = pge.loading_dock,
            showers         = pge.showers,
            airconditioning = pge.airconditioning,
            handicap        = pge.handicap,
            parking         = pge.parking,
            food_service    = pge.food_service,
            kitchen         = pge.kitchen,
            dining_room     = pge.dining_room,
            bar             = pge.bar,
            licence         = pge.licence,
            price           = pge.price,
            timecreated     = pge.timecreated,
            timetouched     = pge.timetouched,
            body            = pge.body,
            owner           = pge.owner,
            )
    except Exception as e:
        flash("Something Failed in Venue Details")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in Venue Details",
            errstring = str(e),
            user      = tuser,
            place     = spot)
        subject = "Exception Error!"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

# ************************************************************************************************************************
@venue.route('/venpage/<int:gevent_id>', methods = ['GET'])
def venpage(gevent_id):
    spot = 20
    tuser = "none"
    but = current_app.config['DEFAULTBUTTON1']
    form = VenueForm
    if current_user.is_authenticated:
        tuser = current_user.id
        if current_user.role > 49:
            but[5] = "adminhome"
        else:
            but[5] = "food"
    try:
        # take the post number and get the row data from the database
        # if there is contact data use that as contact
        # otherwise use the user info as the contact data
        # if there is google map data show the map, best to use the venue if it exists
        # if there is pdf or external website show them
        # the contact will say no if no contact wanted outherwise the id of contact info
        if current_user.is_authenticated:
            form.role.data = current_user.role
            tuser = current_user.id
        spot = 25
        pge = db.session.query(Venue).filter(Venue.id == gevent_id).first()
        if pge == None:
            # maybe capture ip here so we can block hackers
            return redirect(url_for('home.index'))
        spot = 30
        # check for price
        if pge.price == "":
            prc = "Call"
        elif pge.price == "0":
            prc = "Free"
        else:
            prc = pge.price
        spot = 40
        # check for image
        flag = False
        if pge.image0 == None or pge.image0 == '':   # data is blank
            image0 = '' # full size image
            img0a = ''
        else: # pic here
            flag = True
            image0  = pge.image0
            img0a = '/static/uploads/venue/' + pge.imagedatename + '/thumbs/' + os.path.basename(image0)

        if pge.image1 == None or pge.image1 == '' :
            image1 = ""
            img1a = ""
        else:
            flag = True
            image1 = pge.image1
            img1a = '/static/uploads/venue/' + pge.imagedatename + '/thumbs/' + os.path.basename(image1)

        if pge.image2 == None or pge.image2 == '' :
            image2 = ""
            img2a = ""
        else:
            flag = True
            image2 = pge.image2
            img2a = '/static/uploads/venue/' + pge.imagedatename + '/thumbs/' + os.path.basename(image2)

        if pge.image3 == None or pge.image3 == '' :
            image3 = ""
            img3a = ""
        else:
            flag = True
            image3 = pge.image3
            img3a = '/static/uploads/venue/' + pge.imagedatename + '/thumbs/' + os.path.basename(image3)

        if pge.image4 == None or pge.image4 == '' :
            image4 = ""
            img4a = ""
        else:
            flag = True
            image4 = pge.image4
            img4a = '/static/uploads/venue/' + pge.imagedatename + '/thumbs/' + os.path.basename(image4)

        if pge.image5 == None or pge.image5 == '' :
            image5 = ""
            img5a = ""
        else:
            flag = True
            image5 = pge.image5
            img5a = '/static/uploads/venue/' + pge.imagedatename + '/thumbs/' + os.path.basename(image5)

        if pge.image6 == None or pge.image6 == '' :
            image6 = ""
            img6a = ""
        else:
            flag = True
            image6 = pge.image6
            img6a = '/static/uploads/venue/' + pge.imagedatename + '/thumbs/' + os.path.basename(image6)

        if pge.image7 == None or pge.image7 == '' :
            image7 = ""
            img7a = ""
        else:
            flag = True
            image7 = pge.image7
            img7a = '/static/uploads/venue/' + pge.imagedatename + '/thumbs/' + os.path.basename(image7)

        if pge.image8 == None or pge.image8 == '' :
            image8 = ""
            img8a = ""
        else:
            flag = True
            image8 = pge.image8
            img8a = '/static/uploads/venue/' + pge.imagedatename + '/thumbs/' + os.path.basename(image8)

        if pge.image9 == None or pge.image9 == '' :
            image9 = ""
            img9a = ""
        else:
            flag = True
            image9 = pge.image9
            img9a = '/static/uploads/venue/' + pge.imagedatename + '/thumbs/' + os.path.basename(image9)

        if pge.image10 == None or pge.image10 == '' :
            image10 = ""
            img10a = ""
        else:
            flag = True
            image10 = pge.image10
            img10a = '/static/uploads/venue/' + pge.imagedatename + '/thumbs/' + os.path.basename(image10)

        if pge.image11 == None or pge.image11 == '' :
            image11 = ""
            img11a = ""
        else:
            flag = True
            image11 = pge.image11
            img11a = '/static/uploads/venue/' + pge.imagedatename + '/thumbs/' + os.path.basename(image11)
        # check flag, if false img0 = pge.vtype image
        spot = 55
        if flag == False:
            image0  = '/static/images/venue/group/' + pge.vtype + '.png'

        spot = 60
        # setup the iframe for the Googlemap
        if pge.googlemap == "" or pge.googlemap == '12' or pge.googlemap == '40':
            frame = '<iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDfSbsLUsk61pi6QOq8o4NVkgOMoBxIiTE&q=place_id:' + 'ChIJcWGw3Ytzj1QR7Ui7HnTz6Dg' + '"></iframe>'
        else:
            frame = '<iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDfSbsLUsk61pi6QOq8o4NVkgOMoBxIiTE&q=place_id:' + pge.googlemap + '"></iframe>'
        # check to see if there is a current user can edit
        if current_user.is_authenticated:
            if current_user.id == pge.owner or current_user.role >=49:
                edit = pge.owner
            else:
                edit = ""
        else:
            edit = ""
        spot = 70
        # Weblink
        weblink = ''
        if pge.weblink != "":
            weblink = pge.weblink
        spot = 80
        image = ""
        # print colorize('gevent id #', ansi=10), gevent_id
        return render_template('venpage.html',
            but             = but,
            name            = pge.name,
            email           = pge.email,
            phone           = pge.phone1,
            image           = image,
            image0          = image0,
            image1          = image1,
            image2          = image2,
            image3          = image3,
            image4          = image4,
            image5          = image5,
            image6          = image6,
            image7          = image7,
            image8          = image8,
            image9          = image9,
            image10         = image10,
            image11         = image11,
            image0a         = img0a,
            image1a         = img1a,
            image2a         = img2a,
            image3a         = img3a,
            image4a         = img4a,
            image5a         = img5a,
            image6a         = img6a,
            image7a         = img7a,
            image8a         = img8a,
            image9a         = img9a,
            image10a        = img10a,
            image11a        = img11a,
            price           = prc,
            weblink         = weblink,
            street          = pge.street,
            body            = pge.body,
            lodging         = pge.lodging,
            meeting_rooms   = pge.meeting_rooms,
            room_size       = pge.room_size,
            vip_service     = pge.vip_service,
            other_services  = pge.other_services,
            max_occupancy   = pge.max_occupancy,
            audio           = pge.audio,
            visual          = pge.visual,
            lighting        = pge.lighting,
            stage           = pge.stage,
            dance           = pge.dance,
            dressing_rooms  = pge.dressing_rooms,
            business_centre = pge.business_centre,
            internet        = pge.internet,
            storage         = pge.storage,
            loading_dock    = pge.loading_dock,
            showers         = pge.showers,
            airconditioning = pge.airconditioning,
            handicap        = pge.handicap,
            parking         = pge.parking,
            food_service    = pge.food_service,
            kitchen         = pge.kitchen,
            dining_room     = pge.dining_room,
            bar             = pge.bar,
            licence         = pge.licence,
            postnum         = pge.id,
            edit            = edit,
            frame           = frame,
            gevent          = gevent_id,
            form            = form,
            subvenue        = pge.subvenue,
            )
    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in VenPage ",
            errstring = "Error String:  " + repr(e) + "venue id  " + str(gevent_id),
            user      = tuser,
            place     = spot)
        subject = "Exception Error! In Ven Page"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

#  *****************************************************************************************************************************
@venue.route('/venue/<page>', methods=['GET', 'POST'])
def venue(page):
    but   = current_app.config['DEFAULTBUTTON1']
    if current_user.is_authenticated:
        if current_user.role > 49:
            but[5] = "adminhome"
        else:
            but[5] = "food"
    title      = "Venue"
    form       = VenueForm()
    txtlistb   = []
    par        = []
    pgea       = []
    # print colorize("Here in venue  ", ansi=11), page
    spot = 10
    try: # present the main venuegroups, setup and display 50 venues
        group = db.session.query(Vgidgroup).filter(Vgidgroup.name == page).first()
        if group == None:
            return redirect(url_for('home.index'))
        # get list and display upper and lower neighbours
        # print "Group Name ",group.name, colorize("venue  ", ansi=11)
        for it in group.lower_neighbors():
            par.append(it.name)
            # print "name, ", it.name
        # append stuff to pgea and then sort
        # print "more"
        par.append(page)
        for ggroup in par:
            # print "inside  ", ggroup
            tmpgroup = Venue.query.filter(Venue.vtype == ggroup)
            for tmp in tmpgroup:
                # print "appending results ", tmp
                pgea.append(tmp)
        cnt = 0
        for pg in pgea:
            # This is the list of venues
            txtlistb += [[]]
            txtlistb[cnt].append(pg.vtype)
            txtlistb[cnt].append(pg.name)
            txtlistb[cnt].append(pg.phone1)
            txtlistb[cnt].append(pg.email)
            txtlistb[cnt].append(pg.max_occupancy)
            txtlistb[cnt].append(pg.weblink)
            txtlistb[cnt].append(pg.id)
            cnt += 1
        for it in group.higher_neighbors():
            par.append(it.name)
        par = sorted(par)
        spot = 100
        length =  int(math.ceil(len(par) / 10.0 ))
        return render_template('venue.html',
            title = title,
            form  = form,
            but   = but,
            gbut  = ["federal", "civics", "adult"],
            tstlistb = txtlistb,
            par = par,
            length = length,
            )
    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in Venuepage ",
            errstring = "Error String:  " + repr(e),
            user      = "tuser",
            place     = spot)
        subject = "Exception Error! In Page"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))









