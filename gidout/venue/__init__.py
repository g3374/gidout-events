from flask import Blueprint

venue = Blueprint('venue', __name__, template_folder='templates')

from . import routes, errors
