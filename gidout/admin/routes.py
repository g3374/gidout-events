#is_safe_url on_idenity_loaded load_user /adminhome /adminuser /accountinfo /resetpass /login
#/logout /adminpost /admingidgroup /adminmail /admincomments /admincontact /adminvenue /reauth
#/allposts /adminvgidgroup

from . import admin
from flask import render_template, current_app, request, redirect, url_for, flash, session, abort, jsonify
from flask_login import login_user, logout_user, login_required, LoginManager, current_user, fresh_login_required, login_fresh, confirm_login
from gidout.models import User, Post, Gidgroup, Edge, Vgidgroup, Vedge
from gidout.forms import LoginForm, UserForm, PostForm, GidGroupForm, NewpassForm
from flask_wtf import FlaskForm
from wtforms.validators import Required, Length, Email
from werkzeug.security import generate_password_hash, check_password_hash
try:
    from urllib.parse import urlparse, urljoin
except:
    from urlparse import urlparse, urljoin
from gidout import login_manager, db
from xtermcolor import colorize
import time
from sqlalchemy import and_, update
from gidout.entry.routes import generate_confirmation_token, send_email
from sqlalchemy import desc
from datetime import datetime
from flask_mail     import Mail

def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and ref_url.netloc == test_url.netloc


@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


@admin.route('/adminhome', methods=['GET', 'POST'])
@login_required
def adminhome():
    try:
        spot = 10
        if current_user.role < 50:
            return redirect('')
        but     = ["adminhome", "adminuser", "allposts", "adminmail", "admincomments", "adminhome"]
        title   = "Admin Home"
        form    = UserForm()
        role    = current_user.role
        tmp     = ""
        form2   = GidGroupForm()
        form1   = PostForm()
        #Check the request info for post and then check whatt data came in
        #email first, redirect to adminuser with the email info attached
        #nickname second redirect to adminuser with nickname attached
        #post redirect to a post by number
        #venue by number redirect to venue edit
        #venue by name redirect to edit venue
        #comment by number redirect to edit comment
        #group by name redirect to group edit
        #print "here in admin home"

        if request.method == 'POST':
            if form.is_submitted():
                #print "admin home submitted!!"
                if form.email.data != '':
                    return render_template('adminuser.html',
                        title = 'AdminUser',
                        form = form,
                        but = but,
                        )
                elif form.nickname.data != '':
                    return render_template('adminuser.html',
                        title = 'AdminUser',
                        form = form,
                        but = but,
                        )
                elif form.post.data != '':
                    #print "here in editpost"
                    tmp   = form.post.data

                    #tpost = Post.query.filter(Post.post == tmp).first_or_404()
                    form1.postnum.data = tmp
                    #get the post and fill out the form with the post
                    pge = db.session.query(Post).filter(Post.id == tmp).first_or_404()
                    #print pge.user_id
                    form1.user_id.data         = pge.user_id
                    form1.cemail.data          = pge.contactemail
                    form1.vemail.data          = pge.venueemail
                    form1.cphone.data          = pge.contactphone
                    form1.vphone.data          = pge.venuephone
                    form1.cname.data           = pge.contact
                    form1.vname.data           = pge.venuename
                    form1.cavatar.data         = pge.contactavatar
                    form1.chide_email.data     = pge.contacthideemail
                    form1.cweblink.data        = pge.weblink
                    #form1.cstreet.data        = pge.street
                    form1.vaddress.data        = pge.venueaddress
                    form1.ccity.data           = pge.city
                    form1.vcity.data           = pge.city
                    #form1.cstate_prov.data    = pge.contactemail
                    #form1.ccountry.data       = pge.contactemail
                    #form1.cpostalcode.data    = pge.contactemail
                    #form1.name.data           = pge.user_id
                    form1.posttitle.data       = pge.posttitle
                    form1.image.data           = pge.image
                    form1.pdfbrochure.data     = pge.pdfbrochure
                    form1.externalwebsite.data = pge.weblink_title
                    #form1.websitetitle.data    = pge.weblink_title
                    form1.postbody.data        = pge.body
                    form1.relay.data           = pge.relay
                    form1.venuename.data       = pge.venuename
                    #form1.street.data          = pge.contactemail
                    #form1.city.data            = pge.contactemail
                    #form1.other.data           = pge.contactemail
                    form1.price.data           = pge.price
                    form1.postgroup.data       = pge.postgroup
                    gbut                       = [pge.postgroup]
                    form1.googlemap.data       = pge.googlemapa
                    form1.mapchoice.data       = pge.mapchoice
                    start                      = pge.start
                    form1.startyr.data         = start.year
                    form1.startmth.data        = start.month
                    form1.startday.data        = start.day
                    form1.starthr.data         = start.hour
                    form1.startmin.data        = start.minute / 15
                    end                        = pge.end
                    form1.endyr.data           = end.year
                    form1.endmth.data          = end.month
                    form1.endday.data          = end.day
                    form1.endhr.data           = end.hour
                    form1.endmin.data          = end.minute / 15
                    #form1.role.data           = pge.role

                    return render_template('adminpost.html',
                        title = 'Admin Post',
                        form = form1,
                        but = but,
                        gbut = gbut,
                        role = role
                        )
                elif form.venue.data != '':
                    pass
                    form.venue.data = ""
                    return render_template('adminhome.html',
                        title = title,
                        form  = form,
                        but   = but,
                        )#go to admin venue
                elif form.comment.data != '':
                    pass
                    form.comment.data = ""
                    return render_template('adminhome.html',
                        title = title,
                        form  = form,
                        but   = but,
                        )#go to admin venue
                    #go to admin comment
                elif form.contact.data != '':
                    pass
                    form.contact.data = ""
                    return render_template('adminhome.html',
                        title = title,
                        form  = form,
                        but   = but,
                        )#go to admin venue
                    #go to admin contact
                elif form.mail.data != '':
                    pass
                    form.mail.data = ""
                    return render_template('adminhome.html',
                        title = title,
                        form  = form,
                        but   = but,
                        )#go to admin venue
                    #go to admin mail
                elif form.group.data != '': #Admin Gid Group
                    tmp = form.group.data

                    form2.basegroup.data = tmp
                return render_template('admingidgroup.html',
                        title = 'Admin Gid Group',
                        form = form2,
                        but = but,
                        gbut = [tmp],
                        role = role
                        )

        return render_template('adminhome.html',
            title = title,
            form  = form,
            but   = but
            )
    except Exception as e:
        html = render_template('adminerrortemplate.html',
            exception = "Failed in adminhome ",
            errstring = "Error String:  " + repr(e),
            user      = "admin",
            place     = spot)
        subject = "Exception Error! In admin"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@admin.route('/adminuser', methods=['GET', 'POST'])
@login_required
def adminuser():
    try:
        spot = 10
        if current_user.role < 50:
            return redirect('/')
        title       = "Admin User"
        form        = UserForm()
        but         = ["adminhome", "adminuser", "allposts", "adminmail", "admincomments", "adminhome", "adminvenue", "admingidgroup"]
        #print("Here in adminuser")
        #post method is when this is submitted, Get is original click
        if request.method == 'POST':
            if form.is_submitted():
                #print form.hidden_id.data
                #print "nickname", form.nickname.data
                #print form.email.data
                #print form.passwd.data
                #print form.role.data
                #print form.status.data
                #print colorize('SUBMITTED BY POST ', ansi=9), request.form
                # Check the user stuff submitted, check the database for all the info on the user, fill out the form info
                if request.form['sub'] == 'user':
                    if form.email.data != '':
                        guser = User.query.filter(User.email == form.email.data).first()
                    else:
                        if form.nickname.data != '':
                            guser = User.query.filter(User.nickname == form.nickname.data).first()
                            #print "nickname info here"
                        else:
                            if form.id.data != '':
                                guser = User.query.filter(User.id == form.id.data).first()
                            #print "id info here"
                    #print 'Setting up form'
                    form.id.data              = guser.id
                    form.hidden_id.data       = str(guser.id)
                    form.nickname.data        = guser.nickname
                    form.title.data           = guser.title
                    form.first_name.data      = guser.first_name
                    form.initial.data         = guser.initial
                    form.last_name.data       = guser.last_name
                    form.avatar.data          = guser.avatar
                    form.street.data          = guser.street
                    form.city.data            = guser.city
                    form.state_prov.data      = guser.state_prov
                    form.country.data         = guser.country
                    form.postalcode.data      = guser.postalcode
                    form.image.data           = guser.image
                    form.weblink.data         = guser.weblink
                    form.weblink_title.data   = guser.weblink_title
                    form.ip_list.data         = guser.ip_list
                    form.phone1.data          = guser.phone1
                    form.phone2.data          = guser.phone2
                    form.fax.data             = guser.fax
                    form.email.data           = guser.email
                    form.hide_email.data      = guser.hide_email
                    form.karma_bad.data       = guser.karma_bad
                    form.karma_good.data      = guser.karma_good
                    form.messages.data        = guser.messages
                    form.passwd.data          = guser.passwd
                    form.trust.data           = guser.trust
                    form.role.data            = guser.role
                    form.first_date.data      = guser.first_date
                    form.last_seen.data       = guser.last_seen
                    form.last_ip.data         = guser.last_ip
                    form.secret_question.data = guser.secret_question
                    form.secret_answer.data   = guser.secret_answer
                    form.validation_code.data = guser.validation_code
                    form.status.data          = guser.status
                    form.personal_text.data   = guser.personal_text
                    form.confirm_email.data   = guser.confirm_email
                    #print 'ready to send'
                    return render_template('adminuser.html',
                        but = but,
                        form = form
                        )
                #db.session.add(post)
                #db.session.commit()
                #db.session.query(Post).filter(Post.id == gevent_id):
                # update the form info and then update the database

                if request.form['sub'] == 'update':
                    #print 'this is finally goin to a work', form.hidden_id.data, 'ah ha'
                    #send form to the database except for password
                    user = User.query.filter(User.id == form.hidden_id.data).first_or_404()
                    #print 'Have set the user data here'
                    #user.id             = form.hidden_id.data
                    #user.nickname       = form.nickname.data
                    user.title           = form.title.data
                    user.first_name      = form.first_name.data
                    user.initial         = form.initial.data
                    user.last_name       = form.last_name.data
                    user.avatar          = form.avatar.data
                    user.street          = form.street.data
                    user.city            = form.city.data
                    user.state_prov      = form.state_prov.data
                    user.country         = form.country.data
                    user.postalcode      = form.postalcode.data
                    user.image           = form.image.data
                    user.weblink         = form.weblink.data
                    user.weblink_title   = form.weblink_title.data
                    user.ip_list         = form.ip_list.data
                    user.phone1          = form.phone1.data
                    user.phone2          = form.phone2.data
                    user.fax             = form.fax.data
                    #user.email          = form.email.data
                    user.hide_email      = form.hide_email.data
                    user.karma_bad       = form.karma_bad.data
                    user.karma_good      = form.karma_good.data
                    user.messages        = form.messages.data
                    user.trust           = form.trust.data
                    user.role            = form.role.data
                    user.first_date      = form.first_date.data
                    user.last_seen       = form.last_seen.data
                    user.last_ip         = form.last_ip.data
                    user.secret_question = form.secret_question.data
                    user.secret_answer   = form.secret_answer.data
                    user.validation_code = form.validation_code.data
                    user.status          = form.status.data
                    user.personal_text   = form.personal_text.data

                    if form.confirm_email.data == "True":
                        user.confirm_email   = True
                    else:
                        user.confirm_email = False
                    #print 'Have set the user data'
                    #db.session.add(user)
                    db.session.commit()
                    flash('SUCCESS! User has been Updated!')
                    return render_template('adminuser.html',
                        but = but,
                        form = form
                        )

        return render_template('adminuser.html',
            but = but,
            form = form
            )
    except Exception as e:
        html = render_template('adminerrortemplate.html',
            exception = "Failed in adminuser ",
            errstring = "Error String:  " + repr(e),
            user      = "admin",
            place     = spot)
        subject = "Exception Error! In admin"
        send_email(currebt_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@admin.route('/accountinfo')
@login_required
    #account changing, password reset, doublecheck info
def accountinfo():
    #print 'Deal with the account settings here'
    form = "form"
    return render_template('accountinfo.html', form=form)

@admin.route('/resetpass', methods=['GET', 'POST'])
    #password reset, doublecheck info
def resetpass():
    but     = current_app.config['DEFAULTBUTTON1']
    spot = 10
    try:
        form = NewpassForm()
        mail = session.get('mail', None)
        resetpass = current_app.config['RESET_PASS']
        usernormal = current_app.config['USER_NORMAL']
        #print "here   ", mail
        #submitted page check to see what was clicked by id
        if form.validate_on_submit():
            spot = 30
            #print 'Deal with the account settings here'
            #print form.email.data, '  ', form.password.data
            # check the status to see if it is 20 then proceed
            user = User.query.filter_by(email=form.email.data).first_or_404()
            if user.status == resetpass:
                #print "status is good resetting password"
                tpass = generate_password_hash(form.password.data)
                user.status = usernormal
                user.passwd = tpass
                db.session.commit()
                flash("Password Has Been Reset")
            else:
                flash("Failed Please Try Again!")
            return redirect(url_for('admin.login'), but = but)
        spot = 40
        form.email.data = mail
        #print form.errors
        return render_template('resetpass.html',
            form = form,
            but = but
            )
    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in RESETPASS",
            errstring = str(e),
            user      = "ADMIN",
            place     = spot)
        subject = "Exception Error!"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return redirect(url_for('home.index'))

@admin.route('/login', methods=['GET', 'POST'])
def login():
    but     = current_app.config['DEFAULTBUTTON1']
    #print "in Login"
    spot = 100
    try:
        spot = 200
        form = LoginForm()
        if request.values.get('resend'):
            #print 'regeneration email  ', form.email.data
            token = generate_confirmation_token(form.email.data)
            confirm_url = url_for('entry.confirm_email', token=token, _external=True)
            html = render_template('activate.html', confirm_url=confirm_url)
            subject = "Please confirm your email"
            send_email(form.email.data, subject, html)
            flash('A confirmation email has been sent via email.', 'success')
            return redirect(url_for("home.index"))
        #check to see if debug is false, and testing is false, and request is insecure, if true reset the page

        if request.values.get('reset'):
            spot = 300
            #has chosen to reset password, send confirmation email to email address
            #check for the email address and send to that address
            #If no email address available ask for email
            #send confirmation
            token = generate_confirmation_token(form.email.data)
            confirm_url = url_for('entry.reset_email', token=token, _external=True)
            html = render_template('reset_email.html', confirm_url=confirm_url)
            subject = "Requested change of Password"
            send_email(form.email.data, subject, html)
            flash('A Password Reset email has been sent via email.')
            return redirect(url_for("home.index"))

        if request.method == 'POST':
            if form.validate_on_submit():
                spot = 400
                #print 'validated'
                user = User.query.filter_by(email=form.email.data).first()
                #print 'got here in admin.login, ', form.email.data, '  ', form.password.data
                spot = 450
                if user is None:
                    flash('Invalid email or password please try again, or register as new user.')
                    return redirect(url_for('admin.login',
                            but = but,
                            ))
                if user.confirm_email == False:
                    spot = 500
                    flash('Your account is not yet confirmed, please check your email.')
                    form.password.data = ""
                    return render_template('login.html', resend = 1, form = form)
                #User exists, now check password here
                #print check_password_hash(user.passwd, form.password.data)
                if not check_password_hash(user.passwd, form.password.data):
                    spot = 600
                    #print 'bad password'
                    #not correct password
                    flash('Invalid email or password please try again, or register as new user.')
                    return render_template('login.html',
                            rpass = 1,
                            form = form,
                            but = but,
                            )
                #check to see if person wants to stay logged in permanent
                login_user(user, remember = form.remember_me)

                user.last_ip         = str(request.environ['REMOTE_ADDR'])
                user.last_seen       = datetime.now()
                db.session.commit()
                spot = 700
                #print 'Login success' #next takes the user back to where they entered here first check to see if false address
                next = request.args.get('next')
                but[5] = "/entertainment"
                if not is_safe_url(next):
                    return flask.abort(400)
                return redirect(next or url_for('home.index'))
            flash('Invalid email or password please try again, or register as new user.')
        return render_template('login.html',
                form = form,
                but  = but
                )
    except Exception as e:
        html = render_template('adminerrortemplate.html',
            exception = "Failed in admin ",
            errstring = "Error String:  " + repr(e),
            user      = "tuser",
            place     = spot)
        subject = "Exception Error! In admin"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@admin.route('/logout')
@login_required
def logout():
    spot = 10
    try:
        logout_user()
        for key in ('identity.name', 'identity.auth_type'):
            session.pop(key, None)

        # Tell Flask-Principal the user is anonymous
        #identity_changed.send(current_app._get_current_object(), identity=AnonymousIdentity())

        flash('You have been logged out.')
        return redirect(url_for('home.index'))
    except Exception as e:
        html = render_template('adminerrortemplate.html',
            exception = "Failed in logout ",
            errstring = "Error String:  " + repr(e),
            user      = "admin",
            place     = spot)
        subject = "Exception Error! In admin"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@admin.route('/adminpost', methods=['GET', 'POST'])
@login_required
def adminpost():
    try:
        spot = 10
        if current_user.role < 50:
            return redirect('')
        now          = datetime.now()
        title        = "Admin Post"
        form         = PostForm()
        but          = ["adminhome", "adminuser", "allposts", "adminmail", "admincomments", "adminhome", "adminvenue", "admingidgroup"]
        butb         = ""
        tuser        = current_user.nickname
        userid       = current_user.id
        role         = current_user.role
        #post method is when this is submitted, Get is original click
        if request.method == 'POST':
            #print form.postnum.data
            if form.is_submitted():
                #print colorize('Admin Post SUBMITTED BY POST ', ansi=9)
                # convert the time to something useful
                startyr          = form.startyr.data #
                startmth         = form.startmth.data #
                startday         = form.startday.data #
                starthr          = form.starthr.data #
                startmin         = form.startmin.data #
                endyr            = form.endyr.data #
                endmth           = form.endmth.data #
                endday           = form.endday.data #
                endhr            = form.endhr.data #
                endmin           = form.endmin.data #
                startqtr         = (startmin - 1) * 15
                startyrfx        = startyr + 2014
                endqtr           = (endmin - 1) * 15
                endyrfx          = endyr + 2014
                # here I convert the separate time pieces to unix time
                starttime        = datetime(startyrfx,startmth,startday,starthr,startqtr,)
                endtime          = datetime(endyrfx,endmth,endday,endhr,endqtr,)
                tmptime          = endtime + datetime.timedelta(minutes=46)
                if tmptime       <= starttime:
                    endtime      = starttime + datetime.timedelta(hours=1)
                form.endyr.data  = endtime.year - 2014
                form.endmth.data = endtime.month
                form.endday.data = endtime.day
                form.endhr.data  = endtime.hour
                form.endmin.data = (endtime.minute / 15)+ 1

                #get all the info on the user to tst so I can use the id
                # validate the data from the web, flash an error for weird stuff
                # organize in the proper way
                # instantate a post object , fill the data and send to the database
                # this is the post data collection in the model includes all the cells in the data model
                db.session.query(Post).filter(Post.id == form.postnum.data).update({
                    'user_id'          : form.user_id.data,
                    'contact'          : form.cname.data,
                    'contactemail'     : form.cemail.data,
                    'contactphone'     : form.cphone.data,
                    'contactavatar'    : 1,
                    'contacthideemail' : 1,
                    'posttitle'        : form.posttitle.data,
                    'image'            : form.image.data,
                    'email'            : form.cemail.data,
                    'googlemapa'       : form.googlemap.data,
                    'googlemapb'       : form.googlemap.data,
                    'mapchoice'        : 1,
                    'metatag'          : 'data',
                    'fax'              : 'data',
                    'weblink'          : form.externalwebsite.data,
                    'weblink_title'    : form.websitetitle.data,
                    'title'            : form.posttitle.data,
                    'price'            : form.price.data,
                    'body'             : form.postbody.data,
                    'postgroup'        : form.postgroup.data,
                    'venuename'        : form.vname.data,
                    'relay'            : 1,
                    'pdfbrochure'      : form.pdfbrochure.data,
                    'timestamp'        : now,
                    'start'            : starttime,
                    'end'              : endtime,
                    'repeats'          : '0',
                    'language'         : 1,
                    'approved'         : '1',
                    'postgroup'        : form.postgroup.data,
                    'venueaddress'     : form.vaddress.data,
                    'venuephone'       : form.vphone.data,
                    })
                db.session.commit()
                flash('SUCCESS! Post has been Updated!')
            else:
                flash('Nothing Happened')
                #return redirect( url_for ('home.index'))
        else: #GET
            #Should not be here
            #abort(404)
            title = "Admin Home"
            form = UserForm()
            return render_template('adminhome.html',
                title = title,
                form  = form,
                but   = but,
                )
        return render_template('adminpost.html',
            title = "Admin Post",
            but = but,
            form = form,
            role = role,
            )
    except Exception as e:
        html = render_template('adminerrortemplate.html',
            exception = "Failed in adminpost ",
            errstring = "Error String:  " + repr(e),
            user      = "admin",
            place     = spot)
        subject = "Exception Error! In admin"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@admin.route('/admingidgroup', methods=['GET', 'POST'])
@login_required
def admingidgroup():
    print ( colorize("here in admingidgroup", ansi=140))
    try:
        spot = 10
        if current_user.role < 50:
            return redirect('')
        but   = ["adminhome", "adminuser", "allposts", "adminmail", "admincomments", "adminhome", "adminvenue", "admingidgroup"]
        title = "Admin Gid Group"
        form  = GidGroupForm()
        role  = current_user.role
        if request.method == 'POST': #submitted page check to see what was clicked by id
            print (colorize("POSTED data from the form ", ansi = 11), form.hid.data)
            fl = "OOPS FAILED Nothing Happened!!!"
            # if delete delete the connection
            # if submit check to see if word is here, add word and connection if not
            # if word exists check for if connection exists
            # make new connection
            # if submit, first check to see if already done,
            # then if not there add a new word and/or add connection
            # print "form is submitted"
            spot     = 50
            if form.is_submitted():
                print (colorize("data form submitted  ", ansi = 130), form.hid.data)
                if form.hid.data != "Norway": # if Norway true then it is an empty submission if something here check to see if delete
                    if form.sub.data == "<-delete": #Here delete connection Upper
                        spot     = 100
                        #print ("delete upper")
                        highid = db.session.query(Gidgroup).filter(Gidgroup.name == form.text.data).first()
                        lowid  = db.session.query(Gidgroup).filter(Gidgroup.name == form.hid.data).first()
                        db.session.query(Edge).filter(Edge.lower_id == lowid.id, Edge.higher_id == highid.id).delete()
                        db.session.commit()
                        fl  = "SUCCESS! "
                        fl += (form.hid.data)
                        fl += (" has been UPDATED ")
                        fl += (form.text.data)
                        fl += (" has been UNRELATED")
                    elif form.sub.data == "<delete": # Here delete connection Lower find the ids and then delete
                        spot     = 200
                        highid = db.session.query(Gidgroup).filter(Gidgroup.name == form.hid.data).first()
                        lowid = db.session.query(Gidgroup).filter(Gidgroup.name == form.text.data).first()
                        db.session.query(Edge).filter(Edge.lower_id == lowid.id, Edge.higher_id == highid.id).delete()
                        db.session.commit()
                        fl  = "SUCCESS! "
                        fl += (form.hid.data)
                        fl += (" has been UPDATED ")
                        fl += (form.text.data)
                        fl += (" has been UNRELATED")
                        #delete gedge
                    elif form.sub.data == "<-subUP": # Here add a new Upper edge
                        spot     = 300
                        #print (colorize("Submit UP", ansi=155))
                        if form.text.data == "":
                            flash ( "oops clicked on an empty box!!")
                            return render_template('admingidgroup.html',
                                title = title,
                                form  = form,
                                but   = but,
                                gbut  = ["root", "education", "employment","environment","exercise","recreation","food", "health", "hobby", "holiday","sport", "adult", "entertainment","civics"]
                                )
                        a1 = form.hid.data  # always exists
                        a2 = form.text.data # might be a new word or just a new edge
                        gnode = db.session.query(Gidgroup).filter(Gidgroup.name == a2).first() # check to see if word a2 exists, if not add to nodes
                        #print ("GNODE  ", gnode)
                        if gnode == None:# add this word to node
                            tmp = Gidgroup(a2, "", "")
                            db.session.add(tmp)
                            db.session.commit()
                            gnode = db.session.query(Gidgroup).filter(Gidgroup.name == a2).first() # get a handle to this new group
                        #print " Gnode Submit U", " ", colorize(gnode.name, ansi=3)
                        # add as higher neighbour
                        a1id = db.session.query(Gidgroup).filter(Gidgroup.name == a1).first()
                        #print ("a1id.name  ", a1id.name)
                        a1id.add_neighbors(gnode)
                        db.session.commit()
                        fl  = "SUCCESS! "
                        fl += (form.hid.data)
                        fl += (" has been UPDATED ")
                        fl += (form.text.data)
                        fl += (" has been Added and RELATED")
                    elif form.sub.data == "<sbDWN": # Here add a the word as a new LOWER Edge
                        spot     = 40
                        #print ("Submit DOWN")
                        if form.text.data == "":
                            flash ( "oops clicked on an empty box!!")
                            return render_template('admingidgroup.html',
                                title = title,
                                form  = form,
                                but   = but,
                                gbut  = ["root", "education", "employment","environment","exercise","recreation","food", "health", "hobby", "holiday","sport", "adult", "entertainment","civics"]
                                )
                        a1 = form.hid.data
                        a2 = form.text.data
                        #print (" A1 ", a1 ) #the main word and is the lower
                        #print (" A2 ", a2 ) #the word to be added as an higher
                        #check to see if the new word exists in the database
                        gnode = db.session.query(Gidgroup).filter(Gidgroup.name == a2).first()
                        #print (colorize(gnode, ansi = 44))
                        if gnode == None:
                            #print  ("Nobody Home new name not here adding name!!")
                            # add this word to gidgroup
                            tmp = Gidgroup(a2, "", "")
                            db.session.add(tmp)
                            db.session.commit()
                            # added name to gidgroup now get a pointer to it
                            gnode = db.session.query(Gidgroup).filter(Gidgroup.name == a2).first()
                            #print (colorize(gnode, ansi = 77))
                        # the word exists, just add the new edges
                        a1id = db.session.query(Gidgroup).filter(Gidgroup.name == a1).first()
                        #print (colorize(a1id, ansi = 44))
                        gnode.add_neighbors(a1id)
                        db.session.commit()
                        fl  = "SUCCESS! "
                        fl += (form.hid.data)
                        fl += (" has been UPDATED ")
                        fl += (form.text.data)
                        fl += (" has been Added and RELATED DOWN")
                        #check to see if word exists, if not add to nodes
                        # add as a lower neighbour
                    flash(fl)
        else: # got here by GET so place BaseGroup as entertainment
            form.basegroup.data = "entertainment"
            #print (colorize("admingidgroup  GET GET", ansi=9))
        return render_template('admingidgroup.html',
            title = title,
            form  = form,
            but   = but,
            gbut  = ["root", "wine", "employment","environment","exercise","recreation","food", "health", "hobby", "holiday","sport", "adult", "entertainment","civics"]
            )
    except Exception as e:
        flash("Something Failed ouch in ADMINGETGROUP")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in ADMINGIDGROUP",
            errstring = str(e),
            user      = "ADMIN",
            place     = spot)
        subject = "Exception Error!"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@admin.route('/adminmail', methods=['GET', 'POST'])
@login_required
def adminmail():
    spot = 10
    try:
        if current_user.role < 50:
            return redirect('')
        but   = ["adminhome", "adminuser", "allposts", "adminmail", "admincomments", "adminhome", "adminvenue", "admingidgroup"]
        title = "Admin Mail"
        form  = UserForm()
        role  = current_user.role
        return render_template('adminmail.html',
            title = title,
            form  = form,
            but   = but,
            gbut  = ["federal", "civics", "adult"]
            )
    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in ADMINMAIL",
            errstring = str(e),
            user      = "ADMIN",
            place     = spot)
        subject = "Exception Error!"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@admin.route('/admincomments', methods=['GET', 'POST'])
@login_required
def admincomments():
    spot = 10
    try:
        if current_user.role < 50:
            return redirect('')
        but   = ["adminhome", "adminuser", "allposts", "adminmail", "admincomments", "adminhome", "adminvenue", "admingidgroup"]
        title = "Admin Comments"
        form  = UserForm()
        role  = current_user.role
        return render_template('admincomments.html',
            title = title,
            form  = form,
            but   = but,
            gbut  = ["federal", "civics", "adult"]
            )
    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in ADMINCOMMENTS",
            errstring = str(e),
            user      = "ADMIN",
            place     = spot)
        subject = "Exception Error!"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@admin.route('/admincontact', methods=['GET', 'POST'])
@login_required
def admincontact():
    spot = 10
    try:
        if current_user.role < 50:
            return redirect('')
        but   = ["adminhome", "adminuser", "allposts", "adminmail", "admincomments", "admincontact", "adminvenue", "admingidgroup"]
        title = "Admin Contact"
        form  = UserForm()
        role  = current_user.role
        return render_template('admincontact.html',
            title = title,
            form  = form,
            but   = but,
            gbut  = ["federal", "civics", "adult"]
            )
    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in ADMINCOMMENTS",
            errstring = str(e),
            user      = "ADMIN",
            place     = spot)
        subject = "Exception Error!"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@admin.route('/adminvenue', methods=['GET', 'POST'])
@login_required
def adminvenue():
    spot = 10
    try:
        if current_user.role < 50:
            return redirect('')
        but   = ["adminhome", "adminuser", "adminpost", "adminmail", "admincomments", "adminhome", "adminvenue", "admingidgroup"]
        title = "Admin Venue"
        form  = UserForm()
        role  = current_user.role
        return render_template('adminvenue.html',
            title = title,
            form  = form,
            but   = but,
            gbut  = ["federal", "civics", "adult"]
            )
    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in ADMINVENUE",
            errstring = str(e),
            user      = "ADMIN",
            place     = spot)
        subject = "Exception Error!"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@admin.route("/reauth", methods=["GET", "POST"])
@login_required
def reauth():
    but = current_app.config['DEFAULTBUTTON1']
    if current_user.is_authenticated:
        if current_user.role > 4:
            but[5] = 'food'
        if current_user.role > 49:
            but[5] = 'adminhome'
            #print 'admin on front page'
    else:
        but[5] = 'register'
    #print "reauth"
    spot = 10
    try:
        # if the session is `fresh` just redirect to the page the user has requested
        if not login_fresh():
            spot = 20
            form = LoginForm()
            if form.validate_on_submit():
                spot = 30
                user = User.query.filter_by(email=form.email.data).first()
                if user is None:
                    spot = 40
                    flash('Invalid email or password please try again, or register as new user.')
                    return redirect(url_for('admin.login'))
                if user.confirm_email == False:
                    flash('Your account is not yet confirmed, please check your email.')
                    form.password.data = ""
                    return render_template('login.html',
                        resend = 1,
                        form = form
                        )
                #User exists, now check password here
                #print check_password_hash(user.passwd, form.password.data)
                if not check_password_hash(user.passwd, form.password.data):
                    spot = 50
                    #print 'bad password'
                    #not correct password
                    flash('Invalid email or password please try again, or register as new user.')
                    return render_template('login.html',
                        rpass = 1,
                        form = form
                        )
                #check to see if person wants to stay logged in permanent
                confirm_login()
                next = request.args.get('next')
                spot = 120
                if not is_safe_url(next):
                    return flask.abort(400)
                return redirect(next or url_for("home.index"))
            spot = 150
            return render_template("reauth.html",
                form=form,
                but = but,
                )
        next = request.args.get('next')
        spot = 80
        if not is_safe_url(next):
            return flask.abort(400)
        return redirect( next or url_for("home.index"))
    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in Admin Reauth ",
            errstring = "Error String:  " + repr(e),
            user      = "admin",
            place     = spot)
        subject = "Exception Error! In ALLPOST"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@admin.route('/allposts', methods=['GET'])
def allposts():
    if current_user.role < 50:
        return redirect('')
    #print current_user.id
    spot = 5
    #print "here"
    form         = PostForm()
    txtlist   = [["" for x in range(6)] for x in range(500)]
    try:
        if current_user.role < 5:
            return redirect(url_for('home.index'))
        myposts = Post.query.filter(Post.user_id >= 1).order_by(desc(Post.id)).limit(500)
        spot = 10
        it = 0
        for pg in myposts:
            txtlist[it][0] = pg.id
            txtlist[it][1] = pg.posttitle
            txtlist[it][2] = pg.venuename
            txtlist[it][3] = datetime.strftime(pg.start,'%a %b %d %-I:%M %p')
            txtlist[it][4] = datetime.strftime(pg.end,'%a %b %d %-I:%M %p')
            txtlist[it][5] = pg.bodystripped
            it += 1
        return render_template('allposts.html',
            title = "All Posts",
            form = form,
            myposts = myposts,
            txtlist = txtlist,
            )

    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in Admin AllPosts ",
            errstring = "Error String:  " + repr(e),
            user      = "admin",
            place     = spot)
        subject = "Exception Error! In ALLPOST"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))
    else:
        return redirect(url_for('home.index'))

@admin.route('/adminvgidgroup', methods=['GET', 'POST'])
def adminvgidgroup():
    if current_user.role < 50:
        return redirect('')
    spot = 5
    but   = ["adminhome", "adminuser", "allposts", "adminmail", "admincomments", "adminhome"]
    #print "adminvgidgroup"
    try:
        spot = 10
        title = "Admin Vgid Group"
        form  = GidGroupForm()
        role  = current_user.role
        #print "data ", form.hid.data
        if request.method == 'POST': #submitted page check to see what was clicked by id
            #print "data ", form.hid.data
            fl = "Nothing Happened!!!"
            # if delete delete the connection
            # if submit check to see if word is here, add word and connection if not
            # if word exists check for if connection exists
            # make new connection
            # if submit, first check to see if already done,
            # then if not there add a new word and/or add connection
            # print "form is submitted"
            spot     = 15
            if form.is_submitted():
                spot = 20
                #print "submitted data", form.hid.data
                if form.hid.data != "Norway":
                    #print "Not Norway"
                    #there is something here check to see if delete
                    if form.sub.data == "<-delete": #Here delete connection Upper
                        spot     = 25
                        #delete connection
                        #print "delete upper"
                        highid = db.session.query(Vgidgroup).filter(Vgidgroup.name == form.text.data).first()
                        lowid  = db.session.query(Vgidgroup).filter(Vgidgroup.name == form.hid.data).first()
                        #print highid.id, highid.name, " Highid"
                        #print lowid.id, lowid.name, " Lowid"
                        gedge = db.session.query(Vedge).filter(Vedge.lower_id == lowid.id, Vedge.higher_id == highid.id).first()
                        #print "got here  ", gedge
                        #print gedge.id, " id ", gedge.lower_id, " low ", gedge.higher_id, " high "
                        #delete gedge
                        db.session.query(Vedge).filter(Vedge.id == gedge.id).delete()
                        db.session.commit()
                        fl  = "SUCCESS! "
                        fl += (form.hid.data)
                        fl += (" has been UPDATED ")
                        fl += (form.text.data)
                        fl += (" has been UNRELATED")
                    elif form.sub.data == "<--delete": # Here delete connection Lower
                        spot = 40
                        #print "delete lower"
                        highid = db.session.query(Vgidgroup).filter(Vgidgroup.name == form.hid.data).first()
                        lowid = db.session.query(Vgidgroup).filter(Vgidgroup.name == form.text.data).first()
                        #print highid.id, highid.name, " Higher id"
                        #print lowid.id, lowid.name, " Lower id"
                        gedge = db.session.query(Vedge).filter(Vedge.lower_id == lowid.id, Vedge.higher_id == highid.id).first()
                        #print "got here  ", gedge
                        #print gedge.id, " id ", gedge.lower_id, " low ", gedge.higher_id, " high "
                        db.session.query(Vedge).filter(Vedge.id == gedge.id).delete()
                        db.session.commit()
                        fl  = "SUCCESS! "
                        fl += (form.hid.data)
                        fl += (" has been UPDATED ")
                        fl += (form.text.data)
                        fl += (" has been UNRELATED")
                        #delete gedge
                    elif form.sub.data == "<-subUP": # Here add a new Upper
                        spot     = 50
                        #print "Submit U"
                        if form.text.data == "":
                            flash ( "oops clicked on an empty box!!")
                            return render_template('adminvgidgroup.html',
                                title = title,
                                form  = form,
                                but   = but,
                                gbut  = ["arena","home", 'school', 'hotel', 'motel', 'convention', 'indoor', 'outdoor', 'restaurant']
                                )
                        a1 = form.hid.data #always exists
                        a2 = form.text.data # might be a new word or just a new edge
                        #print " A1 ", a1 #the main word and is the lower
                        #print " A2 ", a2 #the word to be added as an higher
                        # check to see if word a2 exists, if not add to nodes
                        gnode = db.session.query(Vgidgroup).filter(Vgidgroup.name == a2).first()
                        if gnode == None:
                            #print "Nobody Home!!"
                            # add this word to node
                            tmp = Vgidgroup(a2, "")
                            db.session.add(tmp)
                            db.session.commit()
                            gnode = db.session.query(Vgidgroup).filter(Vgidgroup.name == a2).first()
                        #print " Gnode Submit U", " ", colorize(gnode.name, ansi=3)
                        # add as higher neighbour
                        a1id = db.session.query(Vgidgroup).filter(Vgidgroup.name == a1).first()
                        #print "a1id.namme  ", a1id.name
                        a1id.add_neighbors(gnode)
                        db.session.commit()
                        fl  = "SUCCESS! "
                        fl += (form.hid.data)
                        fl += (" has been UPDATED ")
                        fl += (form.text.data)
                        fl += (" has been Added and RELATED")
                    elif form.sub.data == "<subDWN": # Here add a new lower
                        spot     = 60
                        #print "Submit DOWN"
                        if form.text.data == "":
                            flash ( "oops clicked on an empty box!!")
                            return render_template('adminvgidgroup.html',
                                title = title,
                                form  = form,
                                but   = but,
                                gbut  = ["arena","home", 'school', 'hotel', 'motel', 'convention', 'indoor', 'outdoor', 'restaurant']
                                )
                        a1 = form.hid.data
                        a2 = form.text.data
                        #print " A1 ", a1  #the main word and is the lower
                        #print " A2 ", a2  #the word to be added as an higher
                        gnode = db.session.query(Vgidgroup).filter(Vgidgroup.name == a2).first()
                        if gnode == None:
                            #print  "Nobody Home!!"
                            # add this word to node
                            tmp = Vgidgroup(a2, "", "")
                            db.session.add(tmp)
                            db.session.commit()
                            gnode = db.session.query(Vgidgroup).filter(Vgidgroup.name == a2).first()
                        a1id = db.session.query(Vgidgroup).filter(Vgidgroup.name == a1).first()
                        gnode.add_neighbors(a1id)
                        db.session.commit()
                        spot = 70
                        fl  = "SUCCESS! "
                        fl += (form.hid.data)
                        fl += (" has been UPDATED ")
                        fl += (form.text.data)
                        fl += (" has been Added and RELATED")
                        #check to see if word exists, if not add to nodes
                        # add as a lower neighbour
                    flash(fl)
        #id = db.session.query(Vgidgroup).filter(Vgidgroup.name == a).first()
        #for it in id.higher_neighbors():
        #    up.append(it.name)
        #print 'Upper', colorize(up, ansi=9)
        #for it in id.lower_neighbors():
        #    dn.append(it.name)
        else: # Got here from Get
            form.basegroup.data = "entertainment"
            #print "adminvgidgroup GET"
            #print dir(request.data)
            #print request.args

        return render_template('adminvgidgroup.html',
            title = title,
            form  = form,
            but   = but,
            gbut  = ["arena","home", 'school', 'hotel', 'motel', 'convention', 'indoor', 'outdoor', 'restaurant']
            )

    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in Admin AdminVgidgroup ",
            errstring = "Error String:  " + repr(e),
            user      = "admin",
            place     = spot)
        subject = "Exception Error! In ADMINVGIDGROUP"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))






