
#/gcalendar

from .                   import gcalendar
from flask               import Flask, render_template, request, flash, abort, jsonify, redirect, url_for, session, current_app
from calendar            import Calendar
import datetime, time
from xtermcolor          import colorize
from gidout              import db
from gidout.models       import Gidgroup, Edge, Post, User
from gidout.forms        import PostForm
import os, math, socket
from flask_login         import login_required, UserMixin, current_user
from gidout.admin.routes import load_user
from werkzeug.utils      import secure_filename
from flask_mail          import Mail
from gidout.entry.routes import send_email
from gidout.gregorian_calendar import GregorianCalendar
from sqlalchemy          import extract

@gcalendar.route('/gcalendar', methods = ['GET', 'POST'])
def gcalendar():
    form = PostForm
    if request.form.get('date'):
        temp = datetime.datetime( year = int(request.form.get('year')), month = int(request.form.get('cmonth')), day = int(request.form.get('date')))
        return redirect( url_for('pages.pagedate', date = temp,))
    posts = [[]]
    current_day, current_month, current_year = GregorianCalendar.current_date()
    if request.method == "GET":
        month = current_month
        year = current_year
    else:
        month = int(request.form.get('cmonth'))    # this is this real month
        year = int(request.form.get('year'))       # this is this year
    spot = 20
    #print "request date   ", request.form.get('date')  # this date is the day to show
    #print "request month  ", request.form.get('month') # this is back or next have to be able to change year
    #print "request cmonth ", request.form.get('cmonth') # this is the month being displayed now
    #print "request year   ", request.form.get('year') # this is the year being displayed now
    # Range is from today to the end of the month
    # range is the full month for future months
    # do a query for events on a each day
    # select the group and the post id for the event link for the icon
    # if month is next add a month to month if month over 12 add a year and month to 1
    # if month is previous check to see if current month, if so do nothing
    # check to see if back a year if so reduce the year and month to 12
    if request.form.get('month') == "next":
        month = month + 1
        if month > 12:
            month = 1
            year = year + 1
            #print month
            #print year
    if request.form.get('month') == 'back':
        if month == current_month and year == current_year: # do nothing
            pass
        else:
            month = month -1
            if month < 1:
                month = 12
                year = year - 1
    month_name  = GregorianCalendar.MONTH_NAMES[month - 1]
    #print current_day
    try:
        gmonth = GregorianCalendar.month_days(year=year, month=month)
        cnt = 0
        for iter1 in gmonth:
            # print iter1.month
            # print type(iter1)
            if iter1 >= datetime.datetime.now().date():
                posts += [[]]
                pge = Post.query.filter(extract('month', Post.start) == iter1.month, extract('year', Post.start) == iter1.year, extract('day', Post.start) == iter1.day).order_by(Post.start)
                for iter2 in pge:
                    posts += [[]]
                    posts[cnt].append(iter1.day)
                    posts[cnt].append(iter2.postgroup)
                    posts[cnt].append(iter2.id)
                    posts[cnt].append(iter1.month)
                    #print iter1.day, "  ", iter2.postgroup, "  ", iter1.month
                    cnt += 1
        return render_template('calendar.html',
                form           = form,
                year           = year,
                month          = month,
                month_name     = month_name,
                current_year   = current_year,
                current_month  = current_month,
                current_day    = current_day,
                month_days     = GregorianCalendar.month_days(year=year, month=month),
                posts          = posts,
                )
    except Exception as e:
        flash("Something Failed ouch")
        html      = render_template('adminerrortemplate.html',
        exception = "Failed in CALENDAR ",
        errstring = "Error String:  " + repr(e),
        user      = "tuser",
        place     = spot)
        subject   = "Exception Error! In CALENDAR"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

