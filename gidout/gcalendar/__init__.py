from flask import Blueprint

gcalendar = Blueprint('gcalendar', __name__, template_folder='templates')

from . import routes, errors
