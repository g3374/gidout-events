from flask import Blueprint

pages = Blueprint('pages', __name__, template_folder='templates')

from . import routes, errors
