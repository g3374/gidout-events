# getIPx /_groupchoice /_getgroupadmin /_getgroup /_autocomplete /<page> /<int:gevent_id> /agreement /list
#/pagedate /privacy /tos /about

from .              import pages
from flask          import Flask, render_template, request, flash, abort, jsonify, redirect, url_for, session, current_app
from calendar       import Calendar, HTMLCalendar
import datetime, time, os, math, socket, googlemaps
from xtermcolor     import colorize
from gidout         import db
from gidout.models  import Gidgroup, Edge, Post, User
from gidout.forms   import PostForm
from flask_login    import login_required, UserMixin, current_user
from gidout.admin.routes import load_user
from werkzeug.utils import secure_filename
from flask_uploads  import UploadSet, IMAGES, configure_uploads, patch_request_class
from MySQLdb        import escape_string as thwart
from operator       import attrgetter
from flask_mail     import Mail
from gidout.entry.routes import send_email
from datetime       import datetime
from sqlalchemy     import extract
from sqlalchemy.orm import sessionmaker

def getIPx(d):
    try:
        spot = 10
        data = socket.gethostbyname_ex(d)
        ipx = repr(data[2])
        return ipx
    except Exception as e:
        html = render_template('adminerrortemplate.html',
            exception = "Failed in getIPx ",
            errstring = "Error String:  " + repr(e),
            user      = "admin",
            place     = spot)
        subject = "Exception Error! In Pages"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@pages.route('/_groupchoice')
def groupchoice():
    spot = 5
    #print (colorize("request args", ansi=222))
    try:
        spot = 10
        # use a try except loop to validate entry gets the nodes in the groups
        # print (request.query_string, "request query string")
        b = request.args.get('a', type=str)
        if b == "":
            return render_template('404.html'), 404
        #print (b, colorize("request args", ansi=222))
        spot = 20
        a = b.split(" ",1)[0]
        # print (a, "after split")
        up = []
        dn = []
        spot = 30
        # get the higher neighbours from the request data
        id = db.session.query(Gidgroup).filter(Gidgroup.name == a).first_or_404()
        for it in id.higher_neighbors():
            up.append(it.name)
        # sort alphabetically
        up = sorted(up)
        # print ("Type UP ", type(up))
        # print ('Upper', colorize(up, ansi=55))
        for it in id.lower_neighbors():
            dn.append(it.name)
        dn = sorted(dn)
        newlist = up + dn
        # print (newlist)
        # print ("DOWN ", dn)
        # build up a single string that is the table html
        # maximum 12 buttons in a row
        spot = 40
        strng  = '<div id="grouptbody" class="w3-row">'
        strng1 = '<div class="w3-row">'
        strng3 = '</div>'

        count = 0
        # print (count)
        for x in newlist:
            count += 1
            # print (count)
            strng += '<div class="w3-col m1 font7"><img class="grouplink" id="%s" src="/static/images/buttons/%s.png" title="%s" />%s</div>' %(x,x,x,x)
            if count == 12 or count == 24 or count == 36 or count == 48 or count == 60:
              strng += strng3
              strng += strng1
        strng += strng3
        # strng += strng3
        # print (strng)
        return jsonify(result=strng)
    except Exception as e:
        html = render_template('adminerrortemplate.html',
            exception = "Failed in groupchoice ",
            errstring = "Error String:  " + repr(e),
            user      = "admin",
            place     = spot)
        subject = "Exception Error! In Pages"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@pages.route('/_getgroup', methods=['GET', 'POST'])
def getgroup():
    #print (colorize("Here in getgroup ", ansi=120))
    spot = 10
    try:
        up = []
        dn = []
        b = request.args.get('a', type=str) # Get the requested group from argument "a"
        #print (colorize("Requested Group ", ansi = 11), b)
        spot = 50
        a = b.split(" ",1)[0]
        handle = db.session.query(Gidgroup).filter(Gidgroup.name == a).first() # get a gidgroup handle to get the id for the request
        #print("Handle here  ", handle.id, handle.name)
        spot = 100
        for x in handle.higher_neighbors():
            #print (colorize('Higher Neighbors  ', ansi=11), colorize(x, ansi=9))
            up.append(x.name)
        up = sorted(up)  #sort alphabetically
        spot = 200
        for it in handle.lower_neighbors():
            #print ('GROUP Down  ', colorize(it, ansi=11))
            #print("Type  ", type(it))
            dn.append(it.name)
        dn = sorted(dn)  #sort alphabetically
        return jsonify(up = up, dn = dn)
    except Exception as e:
        html = render_template('adminerrortemplate.html',
            exception = "Failed in getgroup ",
            errstring = "Error String:  " + repr(e),
            user      = "admin",
            place     = spot)
        subject = "Exception Error! In Page"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@pages.route('/_autocomplete',methods=['GET'])
def _autocomplete():
    try:
        spot = 10
        #print ('autocompletefirst')
        glst =[]
        try:
            for pge in db.session.query(Gidgroup):
                glst.append(pge.name)
                #print ('PGE ', pge.name)
        except Exception as e:
            #print ('something failed in _autocomplete')
            pass
        #print (glst)
        spot = 100
        search = request.args.get('term')

        return jsonify(json_list=glst)
    except Exception as e:
        html = render_template('adminerrortemplate.html',
            exception = "Failed in autocomplete ",
            errstring = "Error String:  " + repr(e),
            user      = "admin",
            place     = spot)
        subject = "Exception Error! In Pages"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@pages.route('/<page>')
def firstpage(page):
    spot = 10
    #print(colorize('Here in firstpage', ansi=199))
    if page == "robots.txt":
        return send_static_file("robots.txt")
    check = db.session.query(Gidgroup).filter(Gidgroup.name == page).first_or_404() # check that page is in group if not the return not found 404
    try:
        groupslist = []
        eventrow1    = []
        eventrow2   = []
        eventrow3   = []
        postrows   = []
        but        = current_app.config['DEFAULTBUTTON1']
        if current_user.is_authenticated:
            if current_user.role > 4:
                but[5] = 'food'
            if current_user.role > 49:
                but[5] = 'adminhome'
        else:
            but[5] = 'register'
        # first get the related group buttons
        spot = 200
        if not current_user.is_authenticated and str(page) in current_app.config['ADULT_GROUPS']:  # if adult and not authenticated send to home
            flash("You need to be logged in to see this group '" + page + "'")
            return redirect(url_for("home.index"))
        spot = 250
        try:
            groupslist.append(str(page))
            outid = db.session.query(Gidgroup).filter(Gidgroup.name == page).first()
            #print(colorize(outid, ansi=199))
            spot = 4270
            if outid.lower_neighbors():
                spot = 4000
                for it in outid.lower_neighbors():
                    groupslist.append(it.name)
            # Get posts from the different groups to postrows and then sort
            # copy groupslist to a temp list to determine groups that exist
            # need a list of groups that can be empty
            #print("Grouplist ", colorize(groupslist, ansi=179))
            spot = 4275
            protectedgroup = current_app.config['PROTECTED_GROUPS'] # protected groups can be empty
            protpar = groupslist[:]
            spot = 4280
            for ggroup in groupslist:
                #print ("inside Ggroup  ", ggroup)
                # get the posts in each group
                tmpgroup = Post.query.filter(Post.postgroup == ggroup, Post.start >= datetime.now()).limit(100)
                # append the posts to a list
                cnt = 0
                for tmp in tmpgroup:
                    # print ("appending results ", tmp)
                    postrows.append(tmp)
                    cnt += 1
                # if the group is empty and does not exist in protected groups delete from list
                spot = 300
                if cnt == 0 and ggroup not in protectedgroup:
                     protpar.remove(ggroup)
            for it in outid.higher_neighbors():
                groupslist.append(it.name)
                #print("Grouplist with higher ", colorize(groupslist, ansi=129))
            groupslist = sorted(groupslist)
            #print("Grouplist sorted ", colorize(groupslist, ansi=169))
            # print "GROUPLIST  ", grouplist
            # check everyone in groupslist, then sort by date and time
            protpar = sorted(protpar)
            postrows = sorted(postrows, key =  attrgetter('start'))
            cnt  = 0
            spot = 400
            for pg in postrows:
                #Start by building the first four big pics
                if cnt <= 3:
                    eventrow1 += [[]]
                    eventrow1[cnt].append(pg.postgroup.lower())
                    #print ( pg.postgroup.lower() )
                    if pg.bodystripped:
                        if len(pg.bodystripped) > 250:
                            info = (pg.bodystripped[:250] + ' ...')
                        else:
                            info = pg.bodystripped
                    else:
                        if len(pg.body) > 250:
                            info = (pg.body[:250] + ' ...')
                        else:
                            info = pg.body
                    spot = 450
                    eventrow1[cnt].append(info)
                    eventrow1[cnt].append(pg.posttitle)
                    eventrow1[cnt].append(pg.id)
                    #either group image or uploaded image
                    if pg.image:
                        eventrow1[cnt].append( '/static' + pg.image)
                    else:
                        eventrow1[cnt].append('/static/images/post/' + pg.postgroup.lower() + '.png')
                    eventrow1[cnt].append(datetime.strftime(pg.start,'%a %b %d %-I:%M %p'))
                    eventrow1[cnt].append(pg.venuename)
                # here build the next 4
                elif cnt <=7:
                    spot = 600
                    eventrow2 += [[]]
                    eventrow2[cnt - 4].append(pg.postgroup.lower())
                    if pg.bodystripped:
                        if len(pg.bodystripped) > 250:
                            info = (pg.bodystripped[:250] + ' ...')
                        else:
                            info = pg.bodystripped
                    else:
                        if len(pg.body) > 250:
                            info = (pg.body[:250] + ' ...')
                        else:
                            info = pg.body
                    eventrow2[cnt - 4].append(info)
                    eventrow2[cnt - 4].append(pg.posttitle)
                    eventrow2[cnt - 4].append(pg.id)
                    if pg.image:
                        eventrow2[cnt - 4].append('/static' + pg.image)
                    else:
                        eventrow2[cnt -4].append('/static/images/post/' + pg.postgroup.lower() + '.png')
                    eventrow2[cnt - 4].append(datetime.strftime(pg.start,'%a %b %d %-I:%M %p'))
                    eventrow2[cnt - 4].append(pg.venuename)
                else:
                    # if there is more than 8 add to a text list
                    eventrow3 += [[]]
                    eventrow3[cnt - 8].append(pg.postgroup.lower())
                    if pg.bodystripped:
                        if len(pg.bodystripped) > 180:
                            info = (pg.bodystripped[:180] + ' ...')
                        else:
                            info = pg.bodystripped
                    else:
                        if len(pg.body) > 180:
                            info = (pg.body[:180] + ' ...')
                        else:
                            info = pg.body
                    eventrow3[cnt - 8].append(info)
                    eventrow3[cnt - 8].append(pg.posttitle)
                    eventrow3[cnt - 8].append(pg.id)
                    eventrow3[cnt - 8].append(datetime.strftime(pg.start,'%a %b %d %-I:%M %p'))
                    eventrow3[cnt - 8].append(pg.karma_good)
                cnt  = cnt + 1
            # if cnt <> 4 or < 8 then fill out with stock ads
            # if over 8 then have a list of text events
            length =  int(math.ceil(len(groupslist) / 10.0 ))
            spot = 800
            butb  = current_app.config['DEFAULTBUTTON2']
            spot = 1000
        except Exception as e:
            html = render_template('adminerrortemplate.html',
                exception = "Failed in firstpage ",
                errstring = "Error String:  " + repr(e) + id.lower_neighbors() + " here is the middle",
                user      = "admin",
                place     = spot)
            subject = "Exception Error! In Pages"
            send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
            return(str(e))
        return render_template('page.html',
            title = page,
            par = protpar, # this is the icon list for associated groups
            length = length,
            tstlist = eventrow1,
            tstlista = eventrow2,
            tstlistb = eventrow3,
            but  = but,
            butb  = butb)
    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in firstpage ",
            errstring = "Error String:  " + repr(e)  + " here is the end" + outid,
            user      = "tuser",
            place     = spot)
        subject = "Exception Error! In Page"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@pages.route('/<int:gevent_id>', methods = ['GET', 'POST'])
def gevent(gevent_id):
    page = 10
    tuser = "none"
    form = PostForm()
    but = current_app.config['DEFAULTBUTTON1']
    try:
        spot = 22
        if current_user.is_authenticated:
            form.role.data = current_user.role
            tuser = current_user.id
            if current_user.role > 4:
                but[5] = 'food'
            if current_user.role > 49:
                but[5] = 'adminhome'
                #print ('admin on front page')
        else:
            but[5] = 'register'
        # take the post number and get the row data from the database
        # if there is contact data use that as contact
        # otherwise use the user info as the contact data
        # if there is google map data show the map, best to use the venue if it exists
        # if there is pdf or external website show them
        # the contact will say no if no contact wanted outherwise the id of contact info
        spot = 25
        pge = db.session.query(Post).filter(Post.id == gevent_id).first()

        if not pge:
            return redirect(url_for('home.index'))
        spot = 30
        # time
        if pge.start.date() == datetime.now().date():
            start = "Today " + datetime.strftime(pge.start,'%-I:%M %p')
            end   = datetime.strftime(pge.end,  '%-I:%M %p')
        else:
            start = datetime.strftime(pge.start,'%a %b %d %-I:%M %p')
            end   = datetime.strftime(pge.end,  '%a %b %d %-I:%M %p')

        # contact
        if pge.contact == "admin" or pge.contact == "info":
            contact = ""
        else:
            contact = pge.contact

        # check for price
        if pge.price == "":
            prc = "Call"
        elif pge.price == "0":
            prc = "Free"
        else:
            prc = pge.price

        # check for image
        if pge.image != "":
            image = '/static' + pge.image
        else:
            #image has to be groupimage
            image = ""

        # setup the iframe for the Googlemap
        if pge.googlemapa == "" or pge.googlemapa == '12' or pge.googlemapa == '40':
            frame = '<iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDfSbsLUsk61pi6QOq8o4NVkgOMoBxIiTE&q=place_id:' + 'ChIJcWGw3Ytzj1QR7Ui7HnTz6Dg' + '"></iframe>'
        else:
            frame = '<iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDfSbsLUsk61pi6QOq8o4NVkgOMoBxIiTE&q=place_id:' + pge.googlemapa + '"></iframe>'

        # check for pdf
        if pge.pdfbrochure != "":
            pdffile = pge.pdfbrochure
        else:
            pdffile = ""

        #check to see if there is a current user can edit
        if current_user.is_authenticated:
            if current_user.id == pge.user_id or current_user.role >=49:
                edit = pge.user_id
            else:
                edit = ""
        else:
            edit = ""

        promote = False
        if current_user.is_authenticated:
            if current_user.role >=49:
                promote = True

        # Weblink
        if pge.weblink != "":
            weblink = pge.weblink
        else:
            pdffile = ""
        spot = 80
        # Venuename
        if pge.venuename != "":
            venuename = pge.venuename
        else:
            venuename = ""

        # Venueemail
        if pge.venueemail != "":
            venueemail = pge.venueemail
        else:
            venueemail = ""

        # Venueaddress
        if pge.venueaddress != "":
            venueaddress = pge.venueaddress
        else:
            venueaddress = ""

        # Agegroup
        agegroup = "Everyone"
        if pge.agegroup == "value_1":
            agegroup = "Everyone"
        elif pge.agegroup == "value_2":
            agegroup = "Senior"
        elif pge.agegroup == "value_3":
            agegroup = "Adult"
        elif pge.agegroup == "value_4":
            agegroup = "Student"
        elif pge.agegroup == "value_5":
            agegroup = "K-12"
        elif pge.agegroup == "value_6":
            agegroup = "Pre-School"
        spot = 100
        #print (colorize('gevent id #', ansi=10), gevent_id)
        return render_template('gevent.html',
            but          = but,
            butb         = current_app.config['DEFAULTBUTTON2'],
            contact      = contact,
            contactemail = pge.contactemail,
            contactphone = pge.contactphone,
            image        = image,
            posttitle    = pge.posttitle,
            price        = prc,
            weblink      = weblink,
            body         = pge.body,
            start        = start,
            end          = end,
            postgroup    = pge.postgroup.lower(),
            venue        = pge.venuename,
            karma        = pge.karma_good,
            frame        = frame,
            postnum      = pge.id,
            pdffile      = pdffile,
            edit         = edit,
            promote      = promote,
            venuename    = venuename,
            venueemail   = venueemail,
            venueaddress = venueaddress,
            agegroup     = agegroup,
            form         = form,
            gevent       = gevent_id,
            )
    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in GEVENT ",
            errstring = "Error String:  " + repr(e) + "event id  " + str(gevent_id),
            user      = str(tuser) + "  " + str(request.remote_addr) + "  " + str(getIPx(str(request.remote_addr))),
            place     = spot)
        subject = "Exception Error! In GEVENT"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@pages.route('/agreement', methods=['GET', 'POST'])
def agreement(): # Display agreement
    spot = 10
    but = current_app.config['DEFAULTBUTTON1']
    if current_user.is_authenticated:
        tuser = current_user.id
        if current_user.role > 4:
            but[5] = 'food'
        if current_user.role > 49:
            but[5] = 'adminhome'
            #print ('admin on front page')
    else:
        but[5] = 'register'
    try:
        return render_template('agreement.html', but = but, butb = current_app.config['DEFAULTBUTTON2'])
    except Exception as e:
        html = render_template('adminerrortemplate.html',
            exception = "Failed in agreement ",
            errstring = "Error String:  " + repr(e),
            user      = "admin",
            place     = spot)
        subject = "Exception Error! In Pages"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@pages.route('/list', methods=['GET', 'POST'])
def list(): # get group list from database and arrange by alphabetic
    page = 10
    but = current_app.config['DEFAULTBUTTON1']
    if current_user.is_authenticated:
        if current_user.role > 4:
            but[5] = 'food'
        if current_user.role > 49:
            but[5] = 'adminhome'
            #print ('admin on front page')
    else:
        but[5] = 'register'
    try:
        lst= []
        output = db.session.query(Gidgroup).order_by(Gidgroup.name)
        for it in output:
            lst.append(it.name)
        length =  int(math.ceil(len(lst) / 10.0 ))
        return render_template('list.html',
            but = but,
            butb = current_app.config['DEFAULTBUTTON2'],
            output = lst,
            length = length
            )
    except Exception as e:
        html = render_template('adminerrortemplate.html',
            exception = "Failed in list ",
            errstring = "Error String:  " + repr(e),
            user      = "admin",
            place     = spot)
        subject = "Exception Error! In Pages"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@pages.route('/pagedate', methods=['GET'])
def pagedate(): # Display daily events
    page = 10
    but = current_app.config['DEFAULTBUTTON1']
    if current_user.is_authenticated:
        if current_user.role > 4:
            but[5] = 'food'
            if current_user.role > 49:
                but[5] = 'adminhome'
        else:
            but[5] = 'register'
    spot = 5

    iter1 = datetime.strptime(request.args.get('date'), '%Y-%m-%d %H:%M:%S')
    form = PostForm()
    txtlist   = [["" for x in range(7)] for x in range(200)]
    try:
        #myposts = Post.query.filter(Post.start == datetime.now()).order_by(Post.start).limit(200)
        myposts = Post.query.filter(extract('month', Post.start) == iter1.month, extract('year', Post.start) == iter1.year, extract('day', Post.start) == iter1.day).order_by(Post.start).limit(200)
        spot = 10
        it = 0
        for pg in myposts:
            if pg.bodystripped:
                if len(pg.bodystripped) > 250:
                    info = (pg.bodystripped[:250] + ' ...')
                else:
                    info = pg.bodystripped

            txtlist[it][0] = pg.postgroup
            txtlist[it][1] = pg.posttitle
            txtlist[it][2] = pg.venuename
            txtlist[it][3] = datetime.strftime(pg.start,'%a %b %d %-I:%M %p')
            txtlist[it][4] = datetime.strftime(pg.end,'%a %b %d %-I:%M %p')
            txtlist[it][5] = info
            txtlist[it][6] = pg.id
            it += 1
        return render_template('pagedate.html',
            title = "Page Date",
            form = form,
            txtlist = txtlist,
            but = but,
            )
    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in Utility MyPosts ",
            errstring = "Error String:  " + repr(e),
            user      = "admin",
            place     = spot)
        subject = "Exception Error! In EDITPOST"
        send_email(current_.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@pages.route('/privacy', methods=['GET', 'POST'])
def privacy(): # Display privacy policy
    spot = 10
    but = current_app.config['DEFAULTBUTTON1']
    if current_user.is_authenticated:
        tuser = current_user.id
        if current_user.role > 4:
            but[5] = 'food'
        if current_user.role > 49:
            but[5] = 'adminhome'
            #print ('admin on front page')
    else:
        but[5] = 'register'
    try:
        return render_template('privacy.html', but = but, butb = current_app.config['DEFAULTBUTTON2'])
    except Exception as e:
        html = render_template('adminerrortemplate.html',
            exception = "Failed in privacy ",
            errstring = "Error String:  " + repr(e),
            user      = "admin",
            place     = spot)
        subject = "Exception Error! In Pages"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@pages.route('/tos', methods=['GET'])
def tos(): # Display terms of service
    spot = 10
    but = current_app.config['DEFAULTBUTTON1']
    if current_user.is_authenticated:
        tuser = current_user.id
        if current_user.role > 4:
            but[5] = 'food'
        if current_user.role > 49:
            but[5] = 'adminhome'
            #print ('admin on front page')
    else:
        but[5] = 'register'
    try:
        return render_template('tos.html', but = but, butb = current_app.config['DEFAULTBUTTON2'])
    except Exception as e:
        html = render_template('adminerrortemplate.html',
            exception = "Failed in tos ",
            errstring = "Error String:  " + repr(e),
            user      = "admin",
            place     = spot)
        subject = "Exception Error! In Pages"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@pages.route('/about', methods=['GET', 'POST'])
def about(): # Display about text
    spot = 10
    but = current_app.config['DEFAULTBUTTON1']
    if current_user.is_authenticated:
        tuser = current_user.id
        if current_user.role > 4:
            but[5] = 'food'
        if current_user.role > 49:
            but[5] = 'adminhome'
            #print ('admin on front page')
    else:
        but[5] = 'register'
    try:
        return render_template('about.html', but = but, butb = current_app.config['DEFAULTBUTTON2'])
    except Exception as e:
        html = render_template('adminerrortemplate.html',
            exception = "Failed in about ",
            errstring = "Error String:  " + repr(e),
            user      = "admin",
            place     = spot)
        subject = "Exception Error! In Pages"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))








