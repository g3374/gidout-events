from gidout import db
from hashlib import md5
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.collections import attribute_mapped_collection
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash

KARMA       = 0
ROLE_GUEST  = 0
ROLE_USER   = 5
ROLE_ADULT  = 10
ROLE_PREM   = 20
ROLE_MOD    = 50
ROLE_ADMIN  = 100

NORMAL_TRUST = 0

Base = declarative_base()

class User(db.Model):
    __tablename__ = 'user'
    id              = db.Column(db.Integer, primary_key=True)
    nickname        = db.Column(db.String(64), index=True, unique=True)
    title           = db.Column(db.String(4))
    first_name      = db.Column(db.String(64))
    initial         = db.Column(db.String(1))
    last_name       = db.Column(db.String(64))
    avatar          = db.Column(db.Integer, db.ForeignKey('avatar.id'))
    street          = db.Column(db.String(64))
    city            = db.Column(db.String(64))
    state_prov      = db.Column(db.String(32))
    country         = db.Column(db.String(32))
    postalcode      = db.Column(db.String(10))
    image           = db.Column(db.String(128))
    weblink         = db.Column(db.String(255))
    weblink_title   = db.Column(db.String(255))
    ip_list         = db.Column(db.Integer, db.ForeignKey('ip.id'))
    phone1          = db.Column(db.String(16))
    phone2          = db.Column(db.String(16))
    fax             = db.Column(db.String(16))
    email           = db.Column(db.String(120), index=True, unique=True)
    hide_email      = db.Column(db.Integer)
    karma_bad       = db.Column(db.Integer)
    karma_good      = db.Column(db.Integer)
    messages        = db.Column(db.Integer)
    passwd          = db.Column(db.String(200))
    trust           = db.Column(db.Integer, default=NORMAL_TRUST)
    role            = db.Column(db.SmallInteger, default=ROLE_USER)
    first_date      = db.Column(db.DateTime)
    last_seen       = db.Column(db.DateTime)
    last_ip         = db.Column(db.String(20))
    secret_question = db.Column(db.String(255))
    secret_answer   = db.Column(db.String(255))
    validation_code = db.Column(db.String(10))
    status          = db.Column(db.Integer)
    personal_text   = db.Column(db.String(1024))
    confirm_email   = db.Column(db.Boolean)
    account         = db.Column(db.Float)
    payment_due     = db.Column(db.DateTime)
    subscr_id       = db.Column(db.String(24))
    total_paid      = db.Column(db.Float)

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    def follow(self, user):
        if not self.is_following(user):
            self.followed.append(user)
            return self

    def unfollow(self, user):
        if self.is_following(user):
            self.followed.remove(user)
            return self

    def is_following(self, user):
        return self.followed.filter(followers.c.followed_id == user.id).count() > 0

    def followed_posts(self):
        return Post.query.join(followers, (followers.c.followed_id == Post.user_id)).filter(followers.c.follower_id == self.id).order_by(Post.timestamp.desc())

    @staticmethod
    def make_unique_nickname(nickname):
        if User.query.filter_by(nickname = nickname).first() == None:
            return nickname
        version = 2
        while True:
            new_nickname = nickname + str(version)
            if User.query.filter_by(nickname = new_nickname).first() == None:
                break
            version += 1
        return new_nickname

    def __repr__(self):
        return '<User %r>' % (self.nickname)

class Avatar(db.Model):
    __tablename__    = "avatar"
    id               = db.Column(db.Integer, primary_key=True)
    imagefile        = db.Column(db.String(128))

class Language(db.Model):
    __tablename__    = "language"
    id               = db.Column(db.Integer, primary_key=True)
    language         = db.Column(db.String(128))

class Ip(db.Model):
    __tablename__    = "ip"
    id               = db.Column(db.Integer, primary_key=True)
    ip               = db.Column(db.String(128))

class Post(db.Model):
    __tablename__    = 'post'
    id               = db.Column(db.Integer, primary_key=True)
    user_id          = db.Column(db.Integer, db.ForeignKey('user.id'))
    contact          = db.Column(db.String(64))
    contactemail     = db.Column(db.String(64))
    contactphone     = db.Column(db.String(16))
    contactavatar    = db.Column(db.Integer, db.ForeignKey('avatar.id'))
    contacthideemail = db.Column(db.Integer)
    city             = db.Column(db.String(24))
    posttitle        = db.Column(db.String(64))
    image            = db.Column(db.String(128))
    email            = db.Column(db.String(120), index=True)
    googlemapa       = db.Column(db.String(128))
    googlemapb       = db.Column(db.String(128))
    mapchoice        = db.Column(db.Integer)
    metatag          = db.Column(db.String(128))
    fax              = db.Column(db.String(16))
    weblink          = db.Column(db.String(128))
    weblink_title    = db.Column(db.String(255))
    title            = db.Column(db.String(64))
    price            = db.Column(db.String(64))
    body             = db.Column(db.String(2000))
    bodystripped     = db.Column(db.String(2000))
    postgroup        = db.Column(db.String(24))
    agegroup         = db.Column(db.String(24))
    venuename        = db.Column(db.String(64))
    venue            = db.Column(db.Integer, db.ForeignKey('venue.id'))
    relay            = db.Column(db.Integer)
    venueemail       = db.Column(db.String(64))
    venuephone       = db.Column(db.String(16))
    venueaddress     = db.Column(db.String(64))
    pdfbrochure      = db.Column(db.String(128))
    timecreated      = db.Column(db.DateTime)
    timestamp        = db.Column(db.DateTime)
    start            = db.Column(db.DateTime)
    end              = db.Column(db.DateTime)
    repeats          = db.Column(db.String(250))
    language         = db.Column(db.Integer, db.ForeignKey('language.id'))
    approved         = db.Column(db.Integer)
    karma_bad        = db.Column(db.Integer, default=KARMA)
    karma_good       = db.Column(db.Integer, default=KARMA)

    def __repr__(self):
        return '<Post %r>' % (self.body)

class Contact(db.Model):
    __tablename__  = 'contact'
    id             = db.Column(db.Integer, primary_key=True)
    creator_id     = db.Column(db.Integer)
    nickname       = db.Column(db.String(64), index=True, unique=True)
    first_name     = db.Column(db.String(64))
    initial        = db.Column(db.String(1))
    last_name      = db.Column(db.String(64))
    avatar         = db.Column(db.Integer, db.ForeignKey('avatar.id'))
    street         = db.Column(db.String(64))
    city           = db.Column(db.String(64))
    state_prov     = db.Column(db.String(32))
    country        = db.Column(db.String(32))
    postalcode     = db.Column(db.String(32))
    phone1         = db.Column(db.String(20))
    phone2         = db.Column(db.String(20))
    fax            = db.Column(db.String(20))
    email          = db.Column(db.String(120), index=True, unique=True)
    hide_email     = db.Column(db.Integer)
    weblink        = db.Column(db.String(255))
    weblink_title  = db.Column(db.String(255))

    def __repr__(self):
        return '<User %r>' % (self.nickname)

# Gidgroup is damn important as it is the foundation of the system it is a graph
class Gidgroup(db.Model):
    __tablename__  = 'gidgroup'
    id             = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name           = db.Column(db.String(64), index=True, unique=True)
    avatar         = db.Column(db.String(64))
    scheme         = db.Column(db.String(64))

    def __init__(self, nam, av, sch):
        self.name   = nam
        self.avatar = av
        self.scheme = sch

    def add_neighbors(self, *nodes):
        for node in nodes:
            Edge(self, node)
        return self

    def higher_neighbors(self):
        return [x.higher_node for x in self.lower_edges]

    def lower_neighbors(self):
        return [x.lower_node for x in self.higher_edges]

    def __repr__(self):
        return '<gidgroup %r>' % (self.name)

# edge is the graph edge relationships to go with gidgroup
class Edge(db.Model):
    __tablename__ = 'edge'
    id            = db.Column(db.Integer, primary_key=True, autoincrement=True)
    lower_id      = db.Column(db.Integer, db.ForeignKey('gidgroup.id'), primary_key=True)
    higher_id     = db.Column(db.Integer, db.ForeignKey('gidgroup.id'), primary_key=True)
    lower_node    = db.relationship(Gidgroup, primaryjoin = lower_id == Gidgroup.id, backref='lower_edges')
    higher_node   = db.relationship(Gidgroup, primaryjoin = higher_id == Gidgroup.id, backref='higher_edges')

    def __init__(self, a1, a2):
        self.lower_node = a1
        self.higher_node = a2

# Scheme is a tree structure of schema relationships
class Scheme(db.Model):
    __tablename__ = 'scheme'
    id           = db.Column(db.Integer, primary_key=True)
    parent_id    = db.Column(db.Integer, db.ForeignKey(id))
    name         = db.Column(db.String(50), nullable=False)

    #this cascades deletions children are a dict
    children = db.relationship("Scheme", cascade="all, delete-orphan", backref=db.backref("parent", remote_side=id), collection_class=attribute_mapped_collection('name'))

    def __init__(self, name, parent=None):
        self.name = name
        self.parent = parent

    def __repr__(self):
        return "Scheme(name=%r, id=%r, parent_id=%r)" % ( self.name, self.id, self.parent_id)

    def dump(self, _indent=0):
        return "   " * _indent + repr(self) + "\n" + "".join([ c.dump(_indent + 1) for c in self.children.values()])

class PendingEmail(db.Model):
    __tablename__ = 'pending_emails'
    id            = db.Column(db.Integer, primary_key=True)
    name          = db.Column(db.String(64))
    email         = db.Column(db.String(64), index=True)
    subject       = db.Column(db.String(128))
    body_text     = db.Column(db.Text())
    body_html     = db.Column(db.Text())
    talk_id = db.Column(db.Integer)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    @staticmethod
    def already_in_queue(email, talk):
        return PendingEmail.query\
            .filter(PendingEmail.talk_id == talk.id)\
            .filter(PendingEmail.email == email).count() > 0

    @staticmethod
    def remove(email):
        PendingEmail.query.filter_by(email=email).delete()

class Venue(db.Model):
    __tablename__   = 'venue'
    id              = db.Column(db.Integer, primary_key=True)
    name            = db.Column(db.String(120), index=True)
    vtype           = db.Column(db.String(64))
    logo            = db.Column(db.String(128))
    street          = db.Column(db.String(64))
    city            = db.Column(db.String(64))
    state_prov      = db.Column(db.String(64))
    country         = db.Column(db.String(64))
    postalcode      = db.Column(db.String(20))
    phone1          = db.Column(db.String(20))
    phone2          = db.Column(db.String(20))
    fax             = db.Column(db.String(20))
    email           = db.Column(db.String(64))
    hide_email      = db.Column(db.String(6))
    title_image     = db.Column(db.String(128))
    image           = db.Column(db.String(128))
    image0          = db.Column(db.String(128))
    image1          = db.Column(db.String(128))
    image2          = db.Column(db.String(128))
    image3          = db.Column(db.String(128))
    image4          = db.Column(db.String(128))
    image5          = db.Column(db.String(128))
    image6          = db.Column(db.String(128))
    image7          = db.Column(db.String(128))
    image8          = db.Column(db.String(128))
    image9          = db.Column(db.String(128))
    image10         = db.Column(db.String(128))
    image11         = db.Column(db.String(128))
    weblink         = db.Column(db.String(64))
    weblink_title   = db.Column(db.String(64))
    lodging         = db.Column(db.String(64))
    meeting_rooms   = db.Column(db.String(64))
    room_size       = db.Column(db.String(64))
    vip_service     = db.Column(db.String(64))
    other_services  = db.Column(db.String(64))
    max_occupancy   = db.Column(db.String(64))
    googlemap       = db.Column(db.String(64))
    audio           = db.Column(db.String(64))
    visual          = db.Column(db.String(64))
    lighting        = db.Column(db.String(64))
    stage           = db.Column(db.String(64))
    dance           = db.Column(db.String(64))
    dressing_rooms  = db.Column(db.String(64))
    business_centre = db.Column(db.String(64))
    internet        = db.Column(db.String(64))
    storage         = db.Column(db.String(64))
    loading_dock    = db.Column(db.String(64))
    showers         = db.Column(db.String(64))
    airconditioning = db.Column(db.String(64))
    handicap        = db.Column(db.String(64))
    parking         = db.Column(db.String(64))
    food_service    = db.Column(db.String(64))
    kitchen         = db.Column(db.String(64))
    dining_room     = db.Column(db.String(64))
    bar             = db.Column(db.String(64))
    licence         = db.Column(db.String(64))
    price           = db.Column(db.String(64))
    secret_question = db.Column(db.String(64))
    secret_answer   = db.Column(db.String(64))
    role            = db.Column(db.String(64))
    timecreated     = db.Column(db.DateTime)
    timetouched     = db.Column(db.DateTime)
    body            = db.Column(db.Text())
    ident           = db.Column(db.String(20))
    owner           = db.Column(db.String(20))
    track           = db.Column(db.String(60))
    imagedatename   = db.Column(db.String(20))
    subvenue        = db.Column(db.String(64))

class Ipn(db.Model):
    __tablename__           = 'ipn'
    id                      = db.Column(db.Integer, primary_key=True)
    unix                    = db.Column(db.DateTime)
    payment_date            = db.Column(db.DateTime)
    first_name              = db.Column(db.String(64))
    last_name               = db.Column(db.String(64))
    payer_business_name     = db.Column(db.String(127))
    payer_email             = db.Column(db.String(127))
    payer_id                = db.Column(db.String(13))
    payer_status            = db.Column(db.String(2))
    address_city            = db.Column(db.String(40))
    address_country         = db.Column(db.String(64))
    address_state           = db.Column(db.String(40))
    address_status          = db.Column(db.String(2))
    address_country_code    = db.Column(db.String(2))
    address_name            = db.Column(db.String(128))
    address_street          = db.Column(db.String(200))
    address_zip             = db.Column(db.String(20))
    custom                  = db.Column(db.String(256))
    contact_phone           = db.Column(db.String(20))
    handling_amount         = db.Column(db.Float)
    item_name               = db.Column(db.String(127))
    item_number             = db.Column(db.String(127))
    mc_currency             = db.Column(db.String(30))
    mc_fee                  = db.Column(db.Float)
    mc_gross                = db.Column(db.Float)
    auth_amount             = db.Column(db.Float)
    auth_id                 = db.Column(db.String(19))
    auth_status             = db.Column(db.String(30))
    exchange_rate           = db.Column(db.String(30))
    payment_gross           = db.Column(db.Float)
    payment_net             = db.Column(db.Float)
    payment_status          = db.Column(db.String(15))
    payment_type            = db.Column(db.String(2))
    payment_fee             = db.Column(db.Float)
    protection_eligibility  = db.Column(db.String(2))
    quantity                = db.Column(db.Integer)
    residence_country       = db.Column(db.String(5))
    shipping                = db.Column(db.Float)
    subscr_id               = db.Column(db.String(15))
    receiver_id             = db.Column(db.String(15))
    tax                     = db.Column(db.Float)
    txn_id                  = db.Column(db.String(25))
    period3                 = db.Column(db.String(10))
    recurring               = db.Column(db.String(10))
    reattempt               = db.Column(db.String(10))
    recur_times             = db.Column(db.Integer)
    subscr_date             = db.Column(db.DateTime)
    mc_amount3              = db.Column(db.Float)
    txn_type                = db.Column(db.String(128))
    ipn_track_id            = db.Column(db.String(25))
    gid_user_id             = db.Column(db.Integer)

class Paypal(db.Model):
    __tablename__           = 'paypal'
    id                      = db.Column(db.Integer, primary_key=True)
    ipndata                 = db.Column(db.String(2048))

class Paymenttmp(db.Model):
    __tablename__           = 'paymenttmp'
    id                      = db.Column(db.Integer, primary_key=True)
    email                   = db.Column(db.String(32))
    gid_user_id             = db.Column(db.Integer)
    flag                    = db.Column(db.Integer)
    timestamp               = db.Column(db.DateTime)

class Carosel(db.Model):
    __tablename__           = 'carosel'
    id                      = db.Column(db.Integer, primary_key=True)
    one                     = db.Column(db.String(32))
    two                     = db.Column(db.String(32))
    three                   = db.Column(db.String(32))
    four                    = db.Column(db.String(32))
    five                    = db.Column(db.String(32))
    six                     = db.Column(db.String(32))
    seven                   = db.Column(db.String(32))
    order                   = db.Column(db.String(32))
    dateone                 = db.Column(db.DateTime)
    datetwo                 = db.Column(db.DateTime)
    datethree               = db.Column(db.DateTime)
    datefour                = db.Column(db.DateTime)
    datefive                = db.Column(db.DateTime)
    datesix                 = db.Column(db.DateTime)
    expireone               = db.Column(db.DateTime)
    expiretwo               = db.Column(db.DateTime)
    expirethree             = db.Column(db.DateTime)
    expirefour              = db.Column(db.DateTime)
    expirefive              = db.Column(db.DateTime)
    expiresix               = db.Column(db.DateTime)
    expireseven             = db.Column(db.DateTime)

class Vgidgroup(db.Model):
    __tablename__ = 'vgidgroup'
    id             = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name           = db.Column(db.String(64), index=True, unique=True)
    avatar         = db.Column(db.String(64))
    scheme         = db.Column(db.String(64))

    def __init__(self, nam, av, sch):
        self.name = nam
        self.avatar = av
        self.scheme = sch

    def add_neighbors(self, *nodes):
        for node in nodes:
            Vedge(self, node)
        return self

    def higher_neighbors(self):
        return [x.higher_node for x in self.lower_edges]

    def lower_neighbors(self):
        return [x.lower_node for x in self.higher_edges]

    def __repr__(self):
        return '<vgidgroup %r>' % (self.name)

# edge is the graph edge relationships to go with gidgroup
class Vedge(db.Model):
    __tablename__ = 'vedge'
    id            = db.Column(db.Integer, primary_key=True, autoincrement=True)
    lower_id      = db.Column(db.Integer, db.ForeignKey('vgidgroup.id'))
    higher_id     = db.Column(db.Integer, db.ForeignKey('vgidgroup.id'))
    lower_node    = db.relationship(Vgidgroup, primaryjoin=lower_id==Vgidgroup.id, backref='lower_edges')
    higher_node   = db.relationship(Vgidgroup, primaryjoin=higher_id==Vgidgroup.id, backref='higher_edges')

    # here we have lower.node_id <= higher.node_id
    def __init__(self, a1, a2):
        #print "a1: ", a1, a1.id, "  a2: ", a2, a2.id
        self.lower_node = a1
        self.higher_node = a2









