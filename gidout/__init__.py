import os

from flask            import Flask, current_app, g
from flask_sqlalchemy import SQLAlchemy
from flask_wtf        import CSRFProtect
from flask_login      import LoginManager
from flask_moment     import Moment
from flask_pagedown   import PageDown
from flask_mail       import Mail, Message
from flask_migrate    import Migrate
from flask_dropzone   import Dropzone
from flask_ckeditor   import CKEditor
from flask_uploads    import UploadSet, IMAGES, configure_uploads, patch_request_class
from paypal           import PayPalConfig, PayPalInterface


csrf           = CSRFProtect()
dropzone       = Dropzone()
moment         = Moment()
pagedown       = PageDown()
mail           = Mail()
ckeditor       = CKEditor()

login_manager  = LoginManager()
login_manager.login_view                     = 'admin.login'
login_manager.refresh_view                   = "admin.reauth"
login_manager.needs_refresh_message          = (u"To protect your account, please reauthenticate to access this page.")
login_manager.needs_refresh_message_category = "info"

ppconfig = PayPalConfig(API_USERNAME  = "mccarter-facilitator_api1.uniserve.com",
                        API_PASSWORD  = "S3QD344Q6859GCPT",
                        API_SIGNATURE = "A2YnYs6LuOd-R8BHIdbWTA6xHgalAPjfRAWnmss0rkTk0hoj4JSgUoHM",
                        DEBUG_LEVEL   = 0)
ppinterface = PayPalInterface(config = ppconfig)


app            = Flask(__name__, instance_relative_config=True)
app.config.from_pyfile('config.py', silent=True)

db             = SQLAlchemy(app)
migrate        = Migrate(app, db)

patch_request_class(app, 2 * 1024 * 1024)

db.init_app(app)
csrf.init_app(app)
dropzone.init_app(app)
moment.init_app(app)
pagedown.init_app(app)
mail.init_app(app)
login_manager.init_app(app)
ckeditor.init_app(app)

pdf    = UploadSet('pdf', extensions=('pdf'))
images = UploadSet('images', IMAGES)
configure_uploads(app, (images, pdf))

#Setup Blueprints
from .admin     import admin
from .home      import home
from .entry     import entry
from .pages     import pages
from .payment   import payment
from .post      import post
from .venue     import venue
from .edit      import edit
from .utility   import utility
from .search    import search
from .gcalendar import gcalendar
app.register_blueprint(admin)
app.register_blueprint(home)
app.register_blueprint(entry)
app.register_blueprint(pages)
app.register_blueprint(payment)
app.register_blueprint(post)
app.register_blueprint(venue)
app.register_blueprint(edit)
app.register_blueprint(utility)
app.register_blueprint(search)
app.register_blueprint(gcalendar)
from .models import *

@app.teardown_appcontext
def shutdown_session(exception=None):
    db.session.remove()
'''
@app.before_first_request
def before_first_request():
    start_email_thread()
'''
