from . import payment
from flask import render_template

@payment.app_errorhandler(500)
def internal_server_error(e):
    return render_template('errors/five_humdred.html'), 500

@payment.app_errorhandler(404)
def page_not_found(e):
    return render_template('errors/page_not_found.html'), 404
