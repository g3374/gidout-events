#send_email  /paypal /account /ipn /paypal/redirect /paypal/redirect_subscribe
# /paypal/confirm /paypal/do/<string:token>  /paypal/status/<string:token>
# /paypal/cancel /paypal_success
from .              import payment
from flask          import render_template, abort, session, current_app, request, flash, url_for, redirect
from gidout         import db, csrf, app
from gidout.models  import Gidgroup, Edge, Post, User, Ipn, Paypal, Paymenttmp
from gidout.forms   import UserForm
from sqlalchemy.sql import func, exists
from datetime       import datetime
from flask_login    import login_required, current_user, fresh_login_required, login_fresh, confirm_login, UserMixin
from werkzeug.datastructures import ImmutableOrderedMultiDict
from datetime       import datetime
from paypal         import PayPalConfig, PayPalInterface
import paypalrestsdk
import logging
import requests
import MySQLdb
import os
from MySQLdb        import escape_string as thwart
from dateutil       import parser
from flask_mail     import Message, Mail
from xtermcolor     import colorize
from gidout         import mail, ppinterface

'''
with app.app_context():
    config = PayPalConfig(API_USERNAME  = current_app.config['PAYPAL_API_USERNAME'],
                          API_PASSWORD  = current_app.config['PAYPAL_API_PASSWORD'],
                          API_SIGNATURE = current_app.config['PAYPAL_API_SIGNATURE'],
                          DEBUG_LEVEL   = 0)
ppinterface = PayPalInterface(config=config)
'''
def send_email(to, subject, template):
    msg = Message(
        subject,
        recipients=[to],
        html=template,
        sender=current_app.config['MAIL_DEFAULT_SENDER']
    )
    mail.send(msg)

@payment.route('/paypal')
@login_required
def paypal():
    #print "Paypal"
    try:
        title     = 'Payment'
        but       = current_app.config['DEFAULTBUTTON1']
        #print  current_app.config['PAYPAL_CLIENT_ID']
        paypalrestsdk.configure({
        "mode": current_app.config['PAYPAL_MODE'], # sandbox or live
        "client_id": current_app.config['PAYPAL_CLIENT_ID'],
        "client_secret": current_app.config['PAYPAL_CLIENT_SECRET'] })
        return render_template('payment.html',
            title = title,
            but = but,
            )
    except Exception as e:
        return(str(e))

@payment.route('/account')
@fresh_login_required
def account():
    # print "Account"
    try:
        title      = 'Account Payment'
        form       = UserForm()
        but        = current_app.config['DEFAULTBUTTON1']
        business   = current_app.config['PAYPAL_API_BUSINESS']
        wwwreturn  = current_app.config['PAYPAL_NOTIFY_URL']
        wwwcancel  = current_app.config['PAYPAL_API_CANCEL']
        wwwsuccess = current_app.config['PAYPAL_API_SUCCESS']

        #print current_app.config['PAYPAL_VALIDATE_URL']
        person     = current_user.id
        return render_template('account.html',
            title      = title,
            but        = but,
            form       = form,
            business   = business,
            wwwreturn  = wwwreturn,
            wwwcancel  = wwwcancel,
            wwwsuccess = wwwsuccess,
            person     = person,
            linkbuy    = url_for('payment.paypal_redirect')
            )
    except Exception as e:
        return(str(e))

@csrf.exempt
@payment.route('/ipn',methods=['POST'])
def ipn():
    # print "Here in IPN  "
    # print colorize("Here IN IPN ", ansi=9)
    spot = 10
    try:
        arg = ''
        request.parameter_storage_class = ImmutableOrderedMultiDict
        values = request.form
        for x, y in values.iteritems():
            arg += "&{x}={y}".format(x=x,y=y)
        tms = current_app.config['PAYPAL_VALIDATE_URL']
        validate_url = tms.format(arg=arg)
        r = requests.get(validate_url)
        spot = 20
        if r.text == 'VERIFIED':
            # If verified save the complete form data to the database
            # print 'Verified'
            # print request.form
            payp = Paypal()
            payp.ipndata = request.form
            db.session.add(payp)
            db.session.commit()
            # print "This is the reply from PayPal  ", r.text
            # print "Transaction Type  ", colorize(request.form.get('txn_type'), ansi=180)
            """
             Transaction Types to handle
                *** express_checkout
                *** subscr_payment
                *** subscr_signup
                *** web_accept
                merch_pmt
                mp_cancel
                recurring_payment
                recurring_payment_expired
                recurring_payment_failed
                recurring_payment_profile_cancel
                recurring_payment_profile_created
                recurring_payment_skipped
                recurring_payment_suspended
                recurring_payment_suspended_due_to_max_failed_payment
                subscr_cancel
                subscr_eot
                subscr_failed
                subscr_modify
                cart
            """
            spot = 30
            try:
                # TXN_TYPE is instant is instant is instant is instant is instant is instant is instant is instant is instant is instant is instant is instant
                if request.form.get('txn_type') == 'instant': # this is a subscription payment
                    # find the right user from the sub id and add to their running total
                    # record this ipn communication and add mc_gross to user.account
                    #print "instant"
                    spot = 40
                    guser = User.query.filter(User.subscr_id == request.form.get('subscr_id')).first()
                    guser.account = float(guser.account) + float(request.form.get('mc_gross'))
                    unix        = datetime.now()
                    fixdate     = parser.parse(request.form.get('payment_date'))
                    ipn = Ipn(
                        unix                 = unix,
                        payer_id             = request.form.get('payer_id'),
                        tax                  = request.form.get('tax'),
                        address_street       = request.form.get('address_street'),
                        payment_date         = fixdate,
                        first_name           = request.form.get('first_name'),
                        last_name            = request.form.get('last_name'),
                        mc_fee               = request.form.get('mc_fee'),
                        address_country_code = request.form.get('address_country_code'),
                        address_name         = request.form.get('address_name'),
                        custom               = request.form.get('custom'),
                        address_country      = request.form.get('address_country'),
                        address_city         = request.form.get('address_city'),
                        payer_email          = request.form.get('payer_email'),
                        txn_id               = request.form.get('txn_id'),
                        address_state        = request.form.get('address_state'),
                        receiver_id          = request.form.get('receiver_id'),
                        txn_type             = request.form.get('txn_type'),
                        item_name            = request.form.get('item_name'),
                        mc_currency          = request.form.get('mc_currency'),
                        residence_country    = request.form.get('residence_country'),
                        address_zip          = request.form.get('address_zip'),
                        ipn_track_id         = request.form.get('ipn_track_id'),
                        mc_gross             = request.form.get('mc_gross'),
                        subscr_id            = request.form.get('subscr_id'),
                        )
                    db.session.add(ipn)
                    db.session.commit()
                    html = render_template('acctmonthsubthanks.html',
                            first_name  = request.form.get('first_name'),
                            last_name   = request.form.get('last_name'),
                            amount      = request.form.get('mc_gross'),
                            date        = fixdate,
                            transaction = request.form.get('txn_id')
                            )
                    subject = "Thank you for your monthly payment"
                    send_email(request.form.get('payer_email'), subject, html)
                    with open('/tmp/ipnout.txt','a') as f:
                        data = 'Instant\n'+str(values)+'\n'
                        f.write(data)

                # TXN TYPE is express_checkout express_checkout express_checkout express_checkout express_checkout express_checkout express_checkout express_checkout
                elif request.form.get('txn_type') == 'express_checkout':                                           # this is the one month checkout
                    #print "Express Checkout  ", colorize("Inside Express Checkout", ansi=180)
                    # use transaction_id txn_id to find transaction in the database and use info here to complete we do not have a current user for IPN
                    # fill out the ipn add to the list, change the amount in the user
                    # build an IPN entry, add money to the user.account
                    # check to see if email is different if it is, mail reply to both emails.
                    # Get email from current user, get email from transaction, compare
                    # add the gid_user_id to the ipn
                    # search the database for this transaction
                    spot = 50
                    tmptxn_id = request.form.get('txn_id')
                    gtxn = Ipn.query.filter(Ipn.txn_id == tmptxn_id).first()
                    guser = User.query.filter(User.id == gtxn.gid_user_id).first()
                    #print guser
                    if guser.account == None:
                        guser.account = 0.0
                    guser.account = float(guser.account) + float(gtxn.mc_gross)
                    #print "after database search"
                    fixdate             = parser.parse(request.form.get('payment_date'))
                    gtxn.mc_fee         = request.form.get('mc_fee')
                    gtxn.payment_date   = fixdate
                    gtxn.item_name      = request.form.get('item_name')
                    gtxn.item_number    = request.form.get('item_number')
                    gtxn.payment_fee    = request.form.get('payment_fee')
                    gtxn.receiver_fee   = request.form.get('receiver_fee')
                    gtxn.receiver_id    = request.form.get('receiver_id')
                    gtxn.ipn_track_id   = request.form.get('ipn_track_id')
                    db.session.commit()
                    # If emails are not the same email both emails emails emails emails emails emails emails emails emails
                    useremail = db.session.query(exists().where(User.email == request.form.get('payer_email'))).scalar() # results in true or false
                    #print useremail
                    if useremail == False: # The Paypal email is not in the database, raise an error!
                        # email to the paypal account
                        html       = render_template('errormonthpaypalbuy.html', txnid = tmptxn_id)
                        subject    = "Please confirm your payment. This email not in our database!"
                        send_email(request.form.get('payer_email'), subject, html)
                        # Email to the gidout admin
                        html       = render_template('adminerrorpaypalbuy.html',
                                address = request.form.get('payer_email'),
                                txnid = request.form.get('ipn_track_id')
                        )
                        subject    = "Please confirm your email. Your email not in our database!"
                        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
                        # email to the gidout account
                        html      = render_template('acctmonththankspaypal.html',
                                first_name  = request.form.get('first_name'),
                                last_name   = request.form.get('last_name'),
                                amount      = request.form.get('mc_gross'),
                                date        = fixdate,
                                transaction = request.form.get('txn_id')
                        )
                        subject   = "Thank you for your payment for one month"
                        send_email(request.form.get('payer_email'), subject, html)
                    else: # all is good just send a thankyou
                        html = render_template('acctmonththanks.html',
                                first_name  = request.form.get('first_name'),
                                last_name   = request.form.get('last_name'),
                                amount      = request.form.get('mc_gross'),
                                date        = fixdate,
                                transaction = request.form.get('txn_id')
                        )
                        subject = "Thank you for your payment for one month"
                        send_email(request.form.get('payer_email'), subject, html)
                        # check the role and then update
                        #print "About to test"
                    with open('/tmp/ipnout.txt','a') as f:
                        data = 'Express Checkout\n'+str(values)+'\n'
                        f.write(data)

                # TXN_TYPE is WEB_accept WEB_accept WEB_accept WEB_accept WEB_accept WEB_accept WEB_accept WEB_accept WEB_accept WEB_accept WEB_accept
                elif request.form.get('txn_type') == 'web_accept': #web_accept buynow one month
                    #print 'here in web_accept'
                    spot = 60
                    unix        = datetime.now()
                    fixdate     = parser.parse(request.form.get('payment_date'))
                    fixpaygross = request.form.get('payment_gross')
                    if fixpaygross == '':
                        fixpaygross = 0.00
                    else:
                        fixpaygross = float(fixpaygross)
                    fixpayfee   = request.form.get('payment_fee')
                    if fixpayfee == '':
                        fixpayfee = 0.00
                    else:
                        fixpayfee            = float(fixpayfee)
                    ipn = Ipn(
                        unix                 = unix,
                        payer_id             = request.form.get('payer_id'),
                        tax                  = request.form.get('tax'),
                        address_street       = request.form.get('address_street'),
                        payment_date         = fixdate,
                        first_name           = request.form.get('first_name'),
                        mc_fee               = request.form.get('mc_fee'),
                        address_country_code = request.form.get('address_country_code'),
                        address_name         = request.form.get('address_name'),
                        custom               = request.form.get('custom'),
                        address_country      = request.form.get('address_country'),
                        address_city         = request.form.get('address_city'),
                        payer_email          = request.form.get('payer_email'),
                        txn_id               = request.form.get('txn_id'),
                        last_name            = request.form.get('last_name'),
                        address_state        = request.form.get('address_state'),
                        payment_fee          = fixpayfee,
                        receiver_id          = request.form.get('receiver_id'),
                        txn_type             = request.form.get('txn_type'),
                        item_name            = request.form.get('item_name'),
                        mc_currency          = request.form.get('mc_currency'),
                        payment_gross        = fixpaygross,
                        residence_country    = request.form.get('residence_country'),
                        address_zip          = request.form.get('address_zip'),
                        ipn_track_id         = request.form.get('ipn_track_id'),
                        mc_gross             = request.form.get('mc_gross')

                        #payment_net          = float(mc_gross) - float(mc_fee)
                        ) # this is the post data collection in the model includes all the cells in the data model
                    useremail = db.session.query(exists().where(User.email == request.form.get('payer_email'))).scalar() # results in true or false
                    # print useremail
                    if useremail == False: # The Paypal email is not in the database, raise an error!
                        # email to the paypal account

                        spot = 70
                        html       = render_template('errormonthpaypalbuy.html', txnid = tmptxn_id)
                        subject    = "Please confirm your payment. This email not in our database!"
                        send_email(request.form.get('payer_email'), subject, html)
                        # Email to the gidout admin
                        html       = render_template('adminerrorpaypalbuy.html',
                                address = request.form.get('payer_email'),
                                txnid = request.form.get('ipn_track_id')
                        )
                        subject    = "Please confirm your email. Your email not in our database!"
                        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
                        # email to the gidout account
                        html      = render_template('acctmonththankspaypal.html',
                                first_name  = request.form.get('first_name'),
                                last_name   = request.form.get('last_name'),
                                amount      = request.form.get('mc_gross'),
                                date        = fixdate,
                                transaction = request.form.get('txn_id')
                        )
                        subject   = "Thank you for your payment for one month"
                        send_email(request.form.get('payer_email'), subject, html)
                    else: # all is good just send a thankyou
                        spot = 80
                        html = render_template('acctmonththanks.html',
                                first_name  = request.form.get('first_name'),
                                last_name   = request.form.get('last_name'),
                                amount      = request.form.get('mc_gross'),
                                date        = fixdate,
                                transaction = request.form.get('txn_id')
                        )
                        subject = "Thank you for your payment for one month"
                        send_email(request.form.get('payer_email'), subject, html)

                    guser = User.query.filter(User.email == request.form.get('payer_email')).first_or_404()
                    #print "Userrole Role  ", guser.role
                    if guser.role <= 20: # change user role to Premium (20)
                        guser.role = current_app.config['ROLE_PREM']

                        #current_app.config['ROLE_PREM']
                        #print "Got here and need to update status and role"

                    db.session.add(ipn)
                    db.session.commit()

                # TXN_TYPE is subscr_signup subscr_signup subscr_signup subscr_signup subscr_signup subscr_signup subscr_signup subscr_signup
                elif request.form.get('txn_type') == 'subscr_signup': #subscription signup
                    # get the user from the custom
                    # print colorize("Here IN subscr_signup ", ansi=9)
                    spot = 90
                    guser = db.session.query(User).filter(User.id == int(request.form.get("custom"))).first()
                    # check to see if user exists if nor email complete data to the admin
                    if guser == None:
                        html = render_template('adminerrortemplate.html',
                        exception = "Failed in Paypal IPN",
                        errstring = "Payment Error in subscr_signup " + request.form,
                        user      = "IPN should be a user  " + request.form.get("custom"),
                        place     = spot)
                        subject = "Payment Error!"
                        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
                        return r.text

                    # print colorize("Here IN subscr_signup ", ansi=9), "Custom is userid  ", request.form.get("custom")
                    # compare emails if different email both accounts
                    # if the same email just the one
                    fixdate                  = parser.parse(request.form.get('subscr_date'))
                    ipn = Ipn(
                        unix                 = datetime.now(),
                        payer_id             = request.form.get('payer_id'),
                        subscr_id            = request.form.get('subscr_id'),
                        address_street       = request.form.get('address_street'),
                        address_zip          = request.form.get('address_zip'),
                        first_name           = request.form.get('first_name'),
                        address_country_code = request.form.get('address_country_code'),
                        address_name         = request.form.get('address_name'),
                        address_country      = request.form.get('address_country'),
                        address_city         = request.form.get('address_city'),
                        payer_email          = request.form.get('payer_email'),
                        last_name            = request.form.get('last_name'),
                        address_state        = request.form.get('address_state'),
                        txn_type             = request.form.get('txn_type'),
                        item_name            = request.form.get('item_name'),
                        mc_currency          = request.form.get('mc_currency'),
                        residence_country    = request.form.get('residence_country'),
                        period3              = request.form.get('period3'),
                        recurring            = request.form.get('recurring'),
                        reattempt            = request.form.get('reattempt'),
                        recur_times          = request.form.get('recur_times'),
                        subscr_date          = fixdate,
                        mc_amount3           = request.form.get('mc_amount3'),
                        ipn_track_id         = request.form.get('ipn_track_id')
                        )
                    db.session.add(ipn)
                    # update the user file
                    #print 'subscription signup'
                    # check the email to see  if correct if not email admin and user
                    # check the permissions and update
                    # send email thanking for payment
                    guser.subscr_id  = request.form.get('subscr_id')
                    guser.role       = 20

                    db.session.commit()
                    #print colorize("Here IN commited ", ansi=9)
                    # email here
                    # send an email to account to thank for the subscription
                    spot = 100
                    html = render_template('subscriptionbuy.html',
                        first_name  = guser.first_name,
                        last_name   = guser.lastname,
                        date        = fixdate,
                        transaction = request.form.get('txn_id')
                        )
                    subject = "Thank you for Subscribing to our Service"
                    send_email(guser.email, subject, html)

                    # debug add text from paypal to temp file
                    """
                    with open('/tmp/ipnout.txt','a') as f:
                        data = 'SUCCESS\n'+str(values)+'\n'
                        f.write(data)
                    """

                # TXN_TYPE is subscr_payment subscr_payment subscr_payment subscr_payment subscr_payment subscr_payment subscr_payment subscr_payment
                elif request.form.get('txn_type') == 'subscr_payment': #subscription payment
                    spot =105
                    guser = db.session.query(User).filter(User.id == int(request.form.get("custom"))).first()
                    # custom might not be here check for subscr_id
                    if guser == None:
                        spot = 106
                        guser = db.session.query(User).filter(User.subscr_id == int(request.form.get("subscr_id"))).first()
                        if guser == None:
                            spot = 107
                            html        = render_template('adminerrortemplate.html',
                                exception   = "Failed in Paypal IPN",
                                errstring   = "Payment Error in subscr_payment no custom no subscr_id" + str(request.form),
                                user        = "IPN should be a user  " + str(request.form.get("custom")),
                                place       = spot
                                )
                            subject     = "Payment Error!"
                            send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
                            return r.text
                    spot = 109
                    fixdate     = parser.parse(str(request.form.get('payment_date')))
                    spot = 1095
                    if request.form.get('payment_fee'):
                        fixpayfee = float(request.form.get('payment_fee'))
                    spot = 110
                    if fixpayfee == '':
                        fixpayfee = 0.00
                    spot = 120
                    ipn = Ipn(
                        payer_id                = request.form.get('payer_id'),
                        payment_date            = fixdate,
                        subscr_id               = request.form.get('subscr_id'),
                        first_name              = request.form.get('first_name'),
                        last_name               = request.form.get('last_name'),
                        payer_email             = request.form.get('payer_email'),
                        payer_status            = request.form.get('payer_status'),
                        custom                  = request.form.get('custom'),
                        item_name               = request.form.get('item_name'),
                        item_number             = request.form.get('item_number'),
                        mc_currency             = request.form.get('mc_currency'),
                        mc_fee                  = request.form.get('mc_fee'),
                        mc_gross                = request.form.get('mc_gross'),
                        payment_gross           = request.form.get('payment_gross'),
                        protection_eligibility  = request.form.get('protection_eligibility'),
                        payment_fee             = fixpayfee,
                        receiver_id             = request.form.get('receiver_id'),
                        txn_type                = request.form.get('txn_type'),
                        payment_status          = request.form.get('payment_status'),
                        payment_type            = request.form.get('payment_type'),
                        txn_id                  = request.form.get('txn_id'),
                        residence_country       = request.form.get('residence_country'),
                        ipn_track_id            = request.form.get('ipn_track_id'),
                        unix = datetime.now()
                    )
                    # print 'subscription payment'
                    # add the payment to the ipn
                    # check the email and make sure that the email exists
                    # if not send email with details to the admin and email the user
                    # make sure the permissions are right
                    spot = 130
                    guser.total_paid   = guser.total_paid + float(request.form.get('mc_gross')),
                    guser.account      = guser.account + float(request.form.get('mc_gross')),
                    guser.payment_due  = datetime.now()
                    if guser.role < 20:
                        guser.role = 20
                    db.session.add(ipn)
                    db.session.commit()
                    spot = 150

                    html = render_template('submonthbuy.html',
                        first_name  = guser.first_name,
                        last_name   = guser.last_name,
                        date        = fixdate,
                        transaction = request.form.get('txn_id')
                        )
                    spot = 160
                    subject = "Thank You for Subscribing to our Service"
                    send_email(guser.email, subject, html)

                    spot = 200
                else: # some other communication that is verified
                    #print 'malformed'
                    with open('/tmp/ipnout.txt','a') as f:
                        data = 'Some ODD UNHANDLED IPN Communication\n' + str(values) + '\n'
                        f.write(data)
                    html    = "<p>Unhandled IPN Communication </p>" + "<pre>" + str(values) + "</pre>"
                    subject    = "Unhandled Verified IPN Communication"
                    send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
                    return r.text
            except Exception as e:
                # debug file is in /tmp
                with open('/tmp/ipnout.txt','a') as f:
                    data = 'ERROR WITH IPN DATA\n'+str(values)+'\n'
                    f.write(data)
                html = render_template('adminerrortemplate.html',
                exception = "Failed in Paypal IPN",
                errstring = str(e) + str(request.form),
                user      = "IPN",
                place     = spot)
                subject = "Exception Error!"
                send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        else:
             with open('/tmp/ipnout.txt','a') as f:
                data = 'FAILURE\n'+str(values)+'\n'
                f.write(data)

        return r.text
    except Exception as e:
        flash("Something Failed in IPN")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in Paypal IPN",
            errstring = str(e),
            user      = "IPN",
            place     = spot)
        subject = "Exception Error!"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@payment.route("/paypal/redirect") # step 1 send data to paypal
def paypal_redirect():
    #print "paypal redirect"
    #print colorize("Here IN redirect ", ansi=155)
    if current_user.is_authenticated:
        kw = {
            'amt'           : int(current_app.config['PAYPAL_PRICE']),
            'currencycode'  : current_app.config['PAYPAL_CURRENCY'],
            'returnurl'     : url_for('payment.paypal_confirm', _external=True),
            'cancelurl'     : url_for('payment.paypal_cancel', _external=True),
            'paymentaction' : 'Sale'
        }
        setexp_response = ppinterface.set_express_checkout(**kw)
        return redirect(ppinterface.generate_express_checkout_redirect_url(setexp_response.token))
    else:
        flash("You do not belong Here")
        return redirect(url_for('home.index'))

@payment.route("/paypal/redirect_subscribe") # step 1 send data to paypal
def paypal_redirect_subscribe():
    #print "paypal redirect_subscribe"
    #print colorize("Here IN redirect subscribe ", ansi=55)

    if current_user.is_authenticated:
        kw = {
            'amt'           : int(current_app.config['PAYPAL_PRICE']),
            'currencycode'  : current_app.config['PAYPAL_CURRENCY'],
            'returnurl'     : url_for('payment.paypal_confirm', _external=True),
            'cancelurl'     : url_for('payment.paypal_cancel', _external=True),
            'paymentaction' : 'Sale'
        }
        setexp_response = ppinterface.set_express_checkout(**kw)
        return redirect(ppinterface.generate_express_checkout_redirect_url(setexp_response.token))
    else:
        flash("You do not belong Here")
        return redirect(url_for('home.index'))

@payment.route("/paypal/confirm") # step 2 get confirm response from paypal. check to see if confirmed
def paypal_confirm():
    #print "paypal confirm"
    #if current_user.is_authenticated:
    #    print "Authenticated is True"
    getexp_response = ppinterface.get_express_checkout_details(token=request.args.get('token', ''))
    if getexp_response['ACK'] == 'Success':
        return """
            Everything looks good! <br />
            <a href="%s">Click here to complete the payment.</a>
        """ % url_for('payment.paypal_do', token=getexp_response['TOKEN'])
    else:
        return """
            Oh no! PayPal returned an error code. <br />
            <pre>
                %s
            </pre>
            Click <a href="%s">here</a> to try again.
        """ % (getexp_response['ACK'], url_for('index'))

@payment.route("/paypal/do/<string:token>") # payment is a go now checkout
def paypal_do(token):
    #print "paypal_do"
    #if current_user.is_authenticated:
    #    print "Authenticated is True"
    getexp_response = ppinterface.get_express_checkout_details(token=token)
    kw = {
        'amt': getexp_response['AMT'],
        'paymentaction': 'Sale',
        'payerid': getexp_response['PAYERID'],
        'token': token,
        'currencycode': getexp_response['CURRENCYCODE']
    }
    ppinterface.do_express_checkout_payment(**kw)
    return redirect(url_for('payment.paypal_status', token=kw['token']))

@payment.route("/paypal/status/<string:token>") # check to see if payment is completed
def paypal_status(token):
    #print "Paypal status"
    # Here I have to create the document for the transaction to then be updated by the IPN information
    # in the IPN I do not know the current user as it is an asyncronous communication
    checkout_response = ppinterface.get_express_checkout_details(token=token)
    #print "checkout response email  ", checkout_response['EMAIL']
    with open('/tmp/allipnout.txt','a') as f:
        data = 'STATUS Communication\n'+str(checkout_response)+'\n\n' # STATUS Communication STATUS Communication STATUS Communication STATUS Communication STATUS Communication STATUS Communication
        f.write(data)
        f.close
    try:
        if checkout_response['CHECKOUTSTATUS'] == 'PaymentActionCompleted':
            # Here we have confirmation of the transaction, upgrade the account role now and transfer to main page
            # wait for the ipn to arrive to make the proper database updates
            auser = User.query.filter(User.id == current_user.get_id()).first()
            if auser.role <= 20:
                auser.role = 20
            unix           = datetime.now()
            fixpaygross    = checkout_response['AMT']
            if fixpaygross == '':
                fixpaygross = 0.00
            else:
                fixpaygross = float(fixpaygross)
            ipn = Ipn(
                    unix                   = unix,
                    first_name             = checkout_response['FIRSTNAME'],
                    last_name              = checkout_response['LASTNAME'],
                    payer_email            = checkout_response['EMAIL'],
                    payer_id               = checkout_response['PAYERID'],
                    address_city           = checkout_response['PAYMENTREQUEST_0_SHIPTOCITY'],
                    address_country        = checkout_response['PAYMENTREQUEST_0_SHIPTOCOUNTRYNAME'],
                    address_state          = checkout_response['PAYMENTREQUEST_0_SHIPTOSTATE'],
                    address_country_code   = checkout_response['PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE'],
                    address_street         = checkout_response['PAYMENTREQUEST_0_SHIPTOSTREET'],
                    address_zip            = checkout_response['PAYMENTREQUEST_0_SHIPTOZIP'],
                    handling_amount        = checkout_response['HANDLINGAMT'],
                    mc_currency            = checkout_response['CURRENCYCODE'],
                    mc_gross               = checkout_response['AMT'],
                    payment_gross          = fixpaygross,
                    quantity               = 1,
                    residence_country      = checkout_response['SHIPTOCOUNTRYCODE'],
                    shipping               = checkout_response['SHIPPINGAMT'],
                    subscr_id              = checkout_response['PAYERID'],
                    tax                    = checkout_response['TAXAMT'],
                    txn_id                 = checkout_response['TRANSACTIONID'],
                    period3                = 0,
                    recurring              = 0,
                    reattempt              = 0,
                    recur_times            = 0,
                    mc_amount3             = 0,
                    gid_user_id            = int(current_user.get_id())
                    )
                    # this is the post data collection in the model includes all the cells in the data model
                    # this data set does not have the mc fee finish off when the ipn arrives
            db.session.add(ipn)
            db.session.commit()
            flash("Thank You for your purchase of $ %s %s."% (checkout_response['AMT'], checkout_response['CURRENCYCODE']))
            return redirect ( url_for('home.index'))
        else:
            flash("Oh no! PayPal doesn't acknowledge the transaction. Here's the status: %s." % (checkout_response['CHECKOUTSTATUS']))
            return redirect( url_for('home.index'))
    except Exception as e:
        return str(e)

@payment.route("/paypal/cancel") # cancel a one month
def paypal_cancel():
    #print "paypal.cancel"
    # check the paypalpayment tmp table to see if there is a entry that needs to be deleted
    if current_user.is_authenticated:
        test = Paymenttmp.query.filter( Paymenttmp.gid_user_id == current_user.id).scalar()
        if test: # there is a thingie, delete it
            Paymenttmp.query.filter(Paymenttmp.gid_user_id == current_user.id ).delete()
            db.session.commit()
        #print "Authenticated is True"
        flash("Payment Canceled!")
    return redirect(url_for('home.index'))

@payment.route('/paypal_success') # success for one month
def paypal_success():
    #print "Paypal Success"
    if current_user.is_authenticated:
        try:
            title     = 'Payment Success'
            but       = current_app.config['DEFAULTBUTTON1']
            butb      = current_app.config['DEFAULTBUTTON2']
            #print  "paypal client ID", current_app.config['PAYPAL_CLIENT_ID']
            return render_template('payment_success.html',
                title = title,
                but = but,
                butb = butb,
                )
        except Exception as e:
            return(str(e))
    else:
        flash("You must be loggeed in to use that Link.")
        return redirect(url_for("home.index"))




