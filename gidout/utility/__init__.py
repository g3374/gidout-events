from flask import Blueprint

utility = Blueprint('utility', __name__, template_folder='templates')

from . import routes, errors
