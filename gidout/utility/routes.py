#_promote /myposts
from .              import utility
from flask          import Flask, render_template, request, flash, abort, jsonify, redirect, url_for, session, current_app
import datetime, time, uuid
from xtermcolor     import colorize
from gidout.forms import PostForm
from gidout            import db
import os, math, operator
from flask_login    import login_required, UserMixin, current_user
from werkzeug.utils import secure_filename
from gidout.models       import Carosel, Post
from flask_mail     import Mail
from gidout.entry.routes import send_email
from datetime       import datetime
from sqlalchemy     import desc

@utility.route('/_promote', methods=['GET'])
def promote():
    if current_user.is_authenticated:
        if current_user.role < 50:
            return redirect(url_for('home.index'))
        try:
            spot = 10
            postnum = request.args.get('name')
            position = request.args.get('param')
            #print ("search  postnum  ", postnum, ' position ', position)
            caro = Carosel.query.filter_by( id = 1 ).first_or_404() # get a handle for the carosel
            spot = 20
            # Change Caro to be the chosen post in the chosen spot
            if position == "0":
                caro.one = postnum
            elif position == "1":
                caro.two = postnum
            elif position == "2":
                caro.three = postnum
            elif position == "3":
                caro.four = postnum
            elif position == "4":
                caro.five = postnum
            elif position == "5":
                caro.six = postnum
            elif position == "6":
                caro.seven = postnum
            db.session.commit()
            # print("commited")
            # Check the dates for the oldest or empty and replace
            # make a list and sort
            # check for empty first and place there
            # check first for expired and put there
            # if none expired change the oldest
            spot = 150
            # Take the number of the post from the request check to see if it is in the carosel
            # Rotate the carosel Add the post to the top of the carosel
            # keep a value of what the order of the carosel is
            # update the carosel number to the post number
            # 7 posts in the carosel?
            spot = 100
            return ('', 204)
        except Exception as e:
            flash("Something Failed ouch")
            html = render_template('adminerrortemplate.html',
                exception = "Failed in Utility _Promote ",
                errstring = "Error String:  " + repr(e),
                user      = "admin",
                place     = spot)
            subject = "Exception Error! In Promote"
            send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
            return(str(e))
    else:
        return redirect(url_for('home.index'))

def takefirst(elem):
    return elem[0]

@utility.route('/myposts', methods=['GET'])
def myposts():
    but = current_app.config['DEFAULTBUTTON1']
    if current_user.is_authenticated:
        if current_user.role > 49:
            but[5] = "/adminhome"
        #print current_user.id
        spot = 5
        #print "here"
        form         = PostForm()
        txtlist   = [["" for x in range(6)] for x in range(200)]
        try:
            if current_user.role < 5:
                return redirect(url_for('home.index'))
            myposts = Post.query.filter(Post.user_id == current_user.id).order_by(desc(Post.timecreated)).limit(200)
            spot = 10
            it = 0
            for pg in myposts:
                txtlist[it][0] = pg.id
                txtlist[it][1] = pg.posttitle
                txtlist[it][2] = pg.venuename
                txtlist[it][3] = datetime.strftime(pg.start,'%a %b %d %-I:%M %p')
                txtlist[it][4] = datetime.strftime(pg.end,'%a %b %d %-I:%M %p')
                txtlist[it][5] = pg.bodystripped
                it += 1

            txtlist.sort( key=takefirst, reverse = True)
            return render_template('myposts.html',
                title = "My Posts",
                form = form,
                myposts = myposts,
                txtlist = txtlist,
                but = but,
            )
        except Exception as e:
            flash("Something Failed ouch")
            html = render_template('adminerrortemplate.html',
                exception = "Failed in Utility MyPosts ",
                errstring = "Error String:  " + repr(e),
                user      = "admin",
                place     = spot)
            subject = "Exception Error! In EDITPOST"
            send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
            return(str(e))
    else:
        return redirect(url_for('home.index'))


