#/ /index /home

from . import home
from flask import render_template, abort, flash, current_app
from gidout import  db
from gidout.models import Gidgroup, Edge, Post, User, Carosel
from sqlalchemy.sql import func
from datetime import datetime
from flask_login import login_required, LoginManager, current_user
from gidout.forms import VenueForm
import os.path, socket
from flask_mail     import Mail
from gidout.entry.routes import send_email
from xtermcolor     import colorize

groupdict = {}
gtmpquery = Gidgroup.query.all()
for tmp in gtmpquery:
    groupdict[tmp.name] = tmp.scheme

@home.route('/')
@home.route('/index')
@home.route('/home')
def index():
    #print(colorize('basedir  ', ansi = 9), current_app.config['BASEDIR'])
    #print(colorize('rootpath  ', ansi = 130), current_app.root_path)
    #print(colorize('instancepath  ', ansi = 160), current_app.instance_path)
    title     = 'GIDOUT of the HOUSE!'
    but       = current_app.config['DEFAULTBUTTON1']
    if current_user.is_authenticated:
        if current_user.role > 4:
            but[5] = 'food'
        if current_user.role > 49:
            but[5] = 'adminhome'
    else:
        but[5] = 'register'
    butb      = current_app.config['DEFAULTBUTTON2']
    page      = "anglican" #default page
    topgroup  = ["anglican", "airport", "civics", "archives", "amusement", "acting", "allergy", "bag"] # Default Images
    toptitle  = ["Your Title Here","Nothing Here","Empty Space","Fill Me Now","I want to be here","Terrific stuff","Happy Days","What a Gas"] # Default Titles
    toppost   = ["","","","","","","",""]
    pic       = ["","","","","","","",""]
    topid     = ["","","","","","","",""]
    toptime   = ["","","","","","","",""]
    topvenue  = ["","","","","","","",""]
    scheme    = ["","","","","","","",""]
    carousel  = ["","","","","","","",""]
    caroimage = ["","","","","","","",""]
    carolink  = ["","","","","","","",""]
    txtlist   = [["" for x in range(8)] for x in range(30)]
    place     = 5
    try:
        #get some random posts to put on the front page make sure they are in the future
        #they do not need to be popular this is the random 8 posts
        pge = Post.query.filter(Post.start >= datetime.now()).order_by(func.rand()).limit(8)
        it = 0
        for pg in pge:
            #print "PG.START  ", pg.start
            if pg.image == "":
                # show the group image
                pic[it] = "/static/images/post/" + pg.postgroup.lower() + ".png"

            else:
                # Take the image from the image column
                pic[it]   = '/static' + pg.image
                #print(colorize('pic  ', ansi = 9), pic[it])
            place = 10

            if pg.bodystripped != None and pg.bodystripped != "None":
                place = 12
                if len(pg.bodystripped) > 250:
                    info = (pg.bodystripped[:250] + ' ...')
                else:
                    place = 13
                    info = pg.bodystripped
            elif pg.body != None and pg.body != "None":
                place = 14
                if len(pg.body) > 250:
                    info = (pg.body[:250] + ' ...')
                else:
                    info = pg.body
            else:
                info = "Empty.."
            place = 15
            toppost[it]   = info
            topgroup[it]  = pg.postgroup.lower()
            toptitle[it]  = pg.posttitle
            topvenue[it]  = pg.venuename
            topid[it]     = pg.id
            toptime[it]   = datetime.strftime(pg.start,'%a %b %d %-I:%M %p')
            scheme[it]    = groupdict[pg.postgroup]
            it += 1
        #search database for what are the next 30 due to time in the database
        #place them onto the text section

        place = 20
        pge = Post.query.filter(Post.start, Post.start >= datetime.now()).order_by(Post.start).limit(30)
        it = 0
        for pg in pge:
            txtlist[it][0] = pg.postgroup.lower()
            if pg.bodystripped != None and pg.bodystripped != "None":
                if len(pg.bodystripped) > 200:
                    info = (pg.bodystripped[:200] + ' ...')
                else:
                    info = pg.bodystripped
            elif pg.body != None and pg.body != "None":
                if len(pg.body) > 200:
                    info = (pg.body[:200] + ' ...')
                else:
                    info = pg.body
            else:
                info = "Empty.."
            txtlist[it][1] = info
            txtlist[it][2] = pg.posttitle
            txtlist[it][3] = pg.id
            txtlist[it][4] = datetime.strftime(pg.start,'%a %b %d %-I:%M %p')
            txtlist[it][5] = pg.karma_good
            if pg.image == "":
                # show the group image
                txtlist[it][6] = "/static/images/post/" + pg.postgroup.lower() + ".png"
            else:
                # Take the image from the image column
                txtlist[it][6] = '/static' + pg.image

            txtlist[it][7] = groupdict[pg.postgroup]

            it += 1

        # Change the buttons depending on whether authenticated

        place = 30

        #print "carosel *****************************************************************************************************************************"
        #here I fill the CAROUSEL There are seven of them
        #print (colorize("caro almost", ansi = 11))
        if None == Carosel.query.filter( Carosel.id == 1 ).first():
            place = 34
            #print (colorize("carosel", ansi = 11))
            for x in range (8):
                #print "inside"
                caroimage[x] = "/static/images/post/food.png"
                carolink[x] = "1"
                carousel[x] = "1"
            place = 36
        else:

            place = 35
            caro = Carosel.query.filter( Carosel.id == 1 ).first()

            post1 = Post.query.filter( Post.id == caro.one ).first()
            if post1.image:
                caroimage[0] = '/static' + post1.image
            else:
                caroimage[0] = "/static/images/post/" + post1.postgroup.lower() + ".png"
            carolink[0]   = post1.id
            carousel[0]   = post1.postgroup.lower()

            post1 = Post.query.filter( Post.id == caro.two ).first()
            if post1.image:
                caroimage[1] = '/static' + post1.image
            else:
                caroimage[1] = "/static/images/post/" + post1.postgroup.lower() + ".png"
            carolink[1]   = post1.id
            carousel[1]   = post1.postgroup.lower()

            post1 = Post.query.filter( Post.id == caro.three ).first()
            if post1.image:
                caroimage[2] = '/static' + post1.image
            else:
                caroimage[2] = "/static/images/post/" + post1.postgroup.lower() + ".png"
            carolink[2]   = post1.id
            carousel[2]   = post1.postgroup.lower()

            post1 = Post.query.filter( Post.id == caro.four ).first()
            if post1.image:
                caroimage[3] = '/static' + post1.image
            else:
                caroimage[3] = "/static/images/post/" + post1.postgroup.lower() + ".png"
            carolink[3]   = post1.id
            carousel[3]   = post1.postgroup.lower()

            post1 = Post.query.filter( Post.id == caro.five ).first()
            if post1.image:
                caroimage[4] = '/static' + post1.image
            else:
                caroimage[4] = "/static/images/post/" + post1.postgroup.lower() + ".png"
            carolink[4]   = post1.id
            carousel[4]   = post1.postgroup.lower()

            post1 = Post.query.filter( Post.id == caro.six ).first()
            if post1.image:
                caroimage[5] = '/static' + post1.image
            else:
                caroimage[5] = "/static/images/post/" + post1.postgroup.lower() + ".png"
            carolink[5]   = post1.id
            carousel[5]   = post1.postgroup.lower()

            post1 = Post.query.filter( Post.id == caro.seven ).first()
            if post1.image:
                caroimage[6] = '/static' + post1.image
            else:
                caroimage[6] = "/static/images/post/" + post1.postgroup.lower() + ".png"
            carolink[6]   = post1.id
            carousel[6]   = post1.postgroup.lower()
            place = 80

    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in HOME ",
            errstring = "Error String:  " + repr(e),
            user      = "admin",
            place     = place)
        subject = "Exception Error! In Home"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        abort(404)

    return render_template('home.html',
        title       = title,
        but         = but,
        butb        = butb,
        carousel    = carousel,
        caroimage   = caroimage,
        carolink    = carolink,
        pic         = pic,
        topgroup    = topgroup,
        topid       = topid,
        topvenue    = topvenue,
        toptitle    = toptitle,
        toppost     = toppost,
        tstlist     = txtlist,
        toptime     = toptime,
        schema      = scheme
        )


