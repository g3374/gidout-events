#remove_html_markup /post
from . import post
from flask import Flask, render_template, request, flash, abort, jsonify, redirect, url_for, session, current_app
from calendar import Calendar, HTMLCalendar
import datetime, time, uuid
from xtermcolor import colorize
from gidout import db, images, pdf
from gidout.models import Gidgroup, Edge, Post, User
from gidout.forms import PostForm
import os, math, bleach, re
from flask_login import login_required, UserMixin, current_user
from gidout.admin.routes import load_user
from werkzeug.utils import secure_filename
from flask_uploads import UploadSet, IMAGES, configure_uploads, patch_request_class
import googlemaps
from MySQLdb import escape_string as thwart
from flask_ckeditor import CKEditor, CKEditorField
from flask_mail import Mail
from gidout.entry.routes import send_email
from PIL import Image
from PIL import ImageFile

ImageFile.LOAD_TRUNCATED_IMAGES = True

def remove_html_markup(s):
    tag = False
    quote = False
    out = ""
    for c in s:
            if c == '<' and not quote:
                tag = True
            elif c == '>' and not quote:
                tag = False
            elif (c == '"' or c == "'") and tag:
                quote = not quote
            elif not tag:
                out = out + c
    return out

@post.route('/post', methods=['GET', 'POST'])
@login_required
def post():
    #print(colorize('IN POST ', ansi = 9))
    form        = PostForm()
    tuser       = current_user.nickname
    spot        = 0
    but         = current_app.config['DEFAULTBUTTON1']
    if current_user.is_authenticated:
        if current_user.role > 4:
            but[5] = 'food'
        if current_user.role > 49:
            but[5] = 'adminhome'
    else:
        but[5] = 'register'
    spot = 100
    try:
        city         = current_app.config['CITY'] # get configuration value
        state        = current_app.config['STATE']
        #badword      = current_app.config['RESTRICTED_WORDS'] # to be implemented
        gmaps        = googlemaps.Client(key = current_app.config['GOOGLE_MAP_KEY'])
        now          = datetime.datetime.now()
        butb         = ""
        tuser        = current_user.nickname
        userid       = current_user.id
        role         = current_user.role
        defaultgroup = "civics"
        filename     = ""
        place        = ""
        filepdf      = ""
        starttime    = ""
        endtime      = ""
        imagefile    = ""
        pdffile      = ""
        groups       = db.session.query(Gidgroup.name)
        tgroups      = []
        # print colorize('GROUPS  ', ansi = 3), groups
        for t in groups:
            s = str(t)
            sm = s[3:-3]
            tgroups.append(sm)
        if request.method == 'POST': # check here to see if from calendar
            spot = 200
            if form.is_submitted(): # if not submitted fall through
                if request.values.get('posting'): # got here from calendar
                    #print(colorize('got here from calendar', ansi = 155))
                    #print(colorize(request.values.get('posting'), ansi = 155))
                    #print(colorize(request.values.get('year'), ansi = 155))
                    #print(colorize(request.values.get('cmonth'), ansi = 155))
                    form.cname.data    = tuser
                    form.startyr.data  = int(request.values.get('year'))
                    form.endyr.data    = int(request.values.get('year'))
                    form.startmth.data = int(request.values.get('cmonth'))
                    form.endmth.data   = int(request.values.get('cmonth'))
                    form.startday.data = int(request.values.get('posting'))
                    form.endday.data   = int(request.values.get('posting'))
                    form.starthr.data  = now.hour
                    form.endhr.data    = now.hour + 1
                    form.startmin.data = 1
                    form.endmin.data   = 1
                    form.price.data    = "Call"
                    spot = 1900
                    return render_template('post.html',
                        title     = 'Post',
                        form      = form,
                        but       = but,
                        butb      = butb,
                        role      = role,
                        groups    = tgroups,
                        imagefile = filename,
                        pdffile   = filepdf,
                        gbut      = current_app.config.get('DEFAULTBUTTON3')
                        )
                if form.validate(): # if not validated fall through
                    # the form validates load up a post object with the data and commit to database
                    # find the user id from the logon info
                    # here I get the results from the posting of the webform to the variables to be validated to the database.
                    # here I convert the separate time pieces to unix time
                    # here I convert time pieces again
                    startyr          = form.startyr.data
                    startmth         = form.startmth.data
                    startday         = form.startday.data
                    starthr          = form.starthr.data
                    startmin         = form.startmin.data
                    endyr            = form.endyr.data
                    endmth           = form.endmth.data
                    endday           = form.endday.data
                    endhr            = form.endhr.data
                    endmin           = form.endmin.data
                    startqtr         = (startmin - 1) * 15
                    endqtr           = (endmin - 1) * 15
                    starttime        = datetime.datetime(startyr, startmth, startday, starthr, startqtr)
                    endtime          = datetime.datetime(endyr, endmth, endday, endhr, endqtr)
                    tmptime          = endtime + datetime.timedelta(minutes=46)
                    if tmptime       <= starttime:
                        endtime      = starttime + datetime.timedelta(hours=1)
                    form.endyr.data  = endtime.year
                    form.endmth.data = endtime.month
                    form.endday.data = endtime.day
                    form.endhr.data  = endtime.hour
                    form.endmin.data = (endtime.minute / 15) + 1
                    newfolder        = datetime.datetime.today().strftime("%Y-%m-%d")
                    image_dest       = current_app.root_path + '/static/uploads/images/'
                    pdfdest          = current_app.root_path + '/static/uploads/pdf/'
                    spot = 250
                    # print('request.form  ', request.form)
                    #print('request.files', colorize(request.files, ansi=9))
                    #print("request.files['image']", colorize(request.files['image'], ansi=11))
                    if request.form['imagefile'] == '': # It is empty so look for regular image  NOT SUBMIT and REPEAT!!
                        #print('there is no imagefile ', request.form['imagefile'], 'huh')
                        #this is the case of submit and repeat so that the image transfers
                        spot = 300
                        filepath = request.files['image']
                        if filepath.filename == '':
                            spot = 400
                            filename = ""
                            imagefile = filename
                        else:
                            #find name of new file to upload IMAGE
                            #change the destination filename to a uuid.hex one and add the proper extension
                            #first check to see if todays directory exists make a new dir every day
                            spot = 420
                            ext = os.path.splitext(request.files['image'].filename)
                            #print("Filename  ", ext)
                            spot = 440
                            tmp = str(ext[1])
                            spot = 460
                            tmp = tmp.lower()
                            spot = 480
                            if not os.path.isdir(image_dest + '/' + newfolder): # check to see if todays folder exists, make todays folders
                                print('making new dir', image_dest + '/' + newfolder)
                                spot = 500
                                os.mkdir(image_dest + newfolder)
                                os.mkdir(image_dest + newfolder + '/' + 'thumbs')
                                os.mkdir(image_dest + newfolder + '/' + 'mini')
                                spot = 550
                                #inside the newfolder make thumbs and mini
                            spot = 600
                            tmpname = str(uuid.uuid4().hex)  # new filename
                            filename = images.save(request.files['image'], folder = image_dest + newfolder + '/', name = tmpname + ".")
                            #print(colorize('got this far ', ansi = 9), filename)
                            spot = 650
                            # take the new file and make thumbnails
                            im = Image.open(filename)
                            spot = 660
                            im.thumbnail((60, 60), Image.ANTIALIAS)
                            im = im.convert('RGB')
                            spot = 675
                            im.save(image_dest  + newfolder + '/thumbs/' + tmpname + tmp)
                            spot = 700
                            im.thumbnail((20, 20), Image.ANTIALIAS)
                            im.save(image_dest + newfolder + '/mini/' + tmpname + tmp )
                            im.close()
                            spot = 750
                            #print (colorize(filename, ansi = 11))
                            #print(image_dest)
                            #print(newfolder)
                            #print (tmpname)
                            #print(tmp)
                            filename = '/uploads/images/' + newfolder + '/' + tmpname + tmp
                            #print (colorize(filename, ansi = 9))
                            form.imagefile.data = filename
                            spot = 800

                    else: # this is a SUBMIT and REPEAT filename is old image file chosen
                        filename = request.form['imagefile']
                        #print request.files.get('imagefile').filename
                    spot = 900
                    # This is a continution of the pdf just connect the uploaded pdf into the row

                    if request.form['pdffile'] == '':
                        try:
                            if request.files.get('pdfbrochure').filename != '':
                                #find name of new file, to upload PDFBROCHURE
                                #first check to see if todays directory exists
                                ext = os.path.splitext(request.files.get('pdfbrochure').filename)
                                tmp = str(ext[1])
                                tmp = tmp.lower()
                                if not os.path.isdir(pdfdest + '/' + newfolder):
                                    os.mkdir(pdfdest + '/' + newfolder)
                                tmpname = str(uuid.uuid4().hex)
                                #print 'request files pdf  ', request.files['pdfbrochure']
                                filepdf = pdf.save(request.files['pdfbrochure'], folder = pdfdest + '/' + newfolder + '/', name = tmpname + ".")
                                filepdf = pdfdest + '/' + newfolder + '/' + tmpname + tmp
                                form.pdffile.data = filepdf
                                #print filepdf
                            else:
                                filepdf = ""
                        except:
                            filepdf = ""
                        pdffile = filepdf
                    else:
                        filepdf = request.form['pdffile']
                    spot = 1000
                    # check and do the googlemap thang.
                    try:
                        spot = 1020
                        tmp = form.vaddress.data
                        #print("tmp  ", tmp)
                        if tmp != '':
                            if form.vcity.data != '':
                                tmp = tmp + ', ' + form.vcity.data + ', ' + state
                            else:
                                tmp = tmp + ', ' + city + ', ' + state
                        else:
                            tmp = city + ', ' + state
                        geocode_result = gmaps.geocode(tmp)
                        #lat = geocode_result[0]["geometry"]["location"]["lat"]
                        place = str(geocode_result[0]['place_id'])
                    except:
                        spot = 1100
                        place = current_app.config['GOOGLE_CITY_ID']
                    # default to whatever city place_id
                    #get all the info on the user to tst so I can use the id
                    #print colorize('gotten here from tst  ', ansi=10), tst.id
                    # validate the data from the web, flash an error for weird stuff
                    # organize in the proper way
                    # instantate a post object , fill the data and send to the database

                    if role < 50:  # figure out advanced cleaning here!!                         Needs to be extended here for badwords
                        spot = 1200
                        #standard buttons
                        bleachbody = bleach.clean(str(form.postbody.data), strip = True, tags=['b',
                            'strong', 'em', 'blockquote', 'a', 'blockquote', 'p', 'i', 'li', 'ul', 'br', 'ol' 'strike', 'u', 'sub', 'sup', 'img',
                            'table', 'tr', 'td', 'thead', 'th', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'button', 'center', 'col', 'colgroup', 'font',
                            'hr', 'link'])
                        spot = 1210
                        instring  = re.compile(re.escape('fuck'), re.IGNORECASE)  # badwords here
                        instringa = instring.sub('....', bleachbody)
                        instring  = re.compile(re.escape('shit'), re.IGNORECASE)
                        spot = 1220
                        instringb = instring.sub('....', instringa)
                        instring  = re.compile(re.escape('cunt'), re.IGNORECASE)
                        instringc = instring.sub('....', instringb)
                        bleachbody = instringc
                        spot = 1230
                    else:
                        spot = 1250
                        #extra for admins buttons
                        bleachbody = bleach.clean(str(form.postbody.data), strip = True, tags=['b',
                            'strong', 'em', 'blockquote', 'p', 'i', 'li', 'ul', 'br', 'ol', 'strike', 'u', 'sub', 'sup'])
                        # also remove &nbsp;
                        spot = 1260
                        instring  = re.compile(re.escape('fuck'), re.IGNORECASE)    #badwords here
                        instringa = instring.sub('....', bleachbody)
                        instring  = re.compile(re.escape('shit'), re.IGNORECASE)
                        instringb = instring.sub('....', instringa)
                        instring  = re.compile(re.escape('cunt'), re.IGNORECASE)
                        instringc = instring.sub('....', instringb)
                        bleachbody = instringc
                    spot = 1275
                    bbodystripped1  = remove_html_markup(bleachbody)
                    bbodystripped2  = bbodystripped1.replace('&nbsp;', ' ')
                    bbodystripped3  = bbodystripped2.replace('&rdquo;', "'")
                    bbodystripped4  = bbodystripped3.replace('&ldquo;', '`')
                    bbodystripped5  = bbodystripped4.replace('&rsquo;', '"')
                    bbodystripped6  = bbodystripped5.replace('&ndash;', '-')
                    bbodystripped7  = bbodystripped6.replace('&quot;', '"')
                    bbodystripped8  = bbodystripped7.replace('&amp;', '&')
                    bbodystripped9  = bbodystripped8.replace('&hellip;', '...')
                    bbodystripped10 = bbodystripped9.replace('&bull;', '-')
                    bbodystripped11 = bbodystripped10.replace('&mdash;', '-')
                    bbodystripped12 = bbodystripped11.replace('&lt;', '<')
                    bbodystripped13 = bbodystripped12.replace('&gt;', '>')
                    bbodystripped14 = bbodystripped13.replace('&middot;', '.')
                    bbodystripped15 = bbodystripped14.replace('&eacute;', 'e')
                    bbodystripped16 = bbodystripped15.replace('&oacute;', 'o')
                    bbodystripped   = bbodystripped16.replace('&#39;', ' ')

                    spot = 1300
                    #Clean the title of nasty stuff
                    titlebodya = bleach.clean(str(form.posttitle.data), strip = True)
                    instring  = re.compile(re.escape('fuck'), re.IGNORECASE)   # badwords here
                    instringa = instring.sub('....', titlebodya)
                    instring  = re.compile(re.escape('shit'), re.IGNORECASE)
                    instringb = instring.sub('....', instringa)
                    instring  = re.compile(re.escape('cunt'), re.IGNORECASE)
                    instringc = instring.sub('....', instringb)
                    titlebodyb = remove_html_markup(instringc)
                    titlebodyc = titlebodyb.replace('&nbsp;', ' ')
                    titlebodyd = titlebodyc.replace('&rdquo;', "'")
                    titlebodye = titlebodyd.replace('&ldquo;', '`')
                    titlebodyf = titlebodye.replace('&rsquo;', '"')
                    titlebodyg = titlebodyf.replace('&ndash;', '-')
                    titlebodyh = titlebodyg.replace('&quot;', '"')
                    titlebodyi = titlebodyh.replace('&amp;', '&')
                    titlebodyj = titlebodyi.replace('&hellip;', '...')
                    titlebodyk = titlebodyj.replace('&bull;', '-')
                    titlebodyl = titlebodyk.replace('&mdash;', '-')
                    titlebodym = titlebodyl.replace('&middot;', '.')
                    titlebodyn = titlebodym.replace('&eacute;', 'e')
                    titlebodyo = titlebodyn.replace('&oacute;', 'o')
                    titlebody  = titlebodyo.replace('&#39;', ' ')

                    #check to see if group exists, try with lower case fail back to post
                    if Gidgroup.query.filter(Gidgroup.name == form.postgroup.data.lower()).first():
                        pgroup = form.postgroup.data.lower()
                    else:
                        #group not found
                        flash("Group Not Found")
                        return render_template('post.html',
                            title     = 'Post',
                            form      = form,
                            but       = but,
                            butb      = butb,
                            role      = role,
                            groups    = tgroups,
                            imagefile = filename,
                            pdffile   = filepdf,
                            gbut      = current_app.config.get('DEFAULTBUTTON3')
                            )

                    spot = 1400
                    post = Post(
                        user_id          = userid,
                        contact          = thwart(str(form.cname.data)),
                        contactemail     = thwart(str(form.vemail.data)),
                        contactphone     = thwart(str(form.vphone.data)),
                        #contactavatar    = form.avatar.data,
                        #contacthideemail = form.hideemail.data,
                        city             = thwart(str(form.vcity.data)),
                        posttitle        = titlebody,
                        image            = thwart(filename),
                        #email            = form.email.data,
                        googlemapa       = place,
                        #googlemapb       = place2,
                        mapchoice        = 1,
                        metatag          = 'data',
                        fax              = 'data',
                        #weblink          = 'data',
                        weblink_title    = thwart(str(form.externalwebsite.data)),
                        title            = thwart(str(form.posttitle.data)),
                        price            = bleach.clean(str(form.price.data), tags=[], strip = True),
                        body             = bleachbody,
                        bodystripped     = bbodystripped,
                        postgroup        = thwart(str(pgroup)),
                        venuename        = thwart(str(form.vname.data)),
                        relay            = 1,
                        venueemail       = thwart(str(form.vemail.data)),
                        venuephone       = thwart(str(form.vphone.data)),
                        venueaddress     = thwart(str(form.vaddress.data)),
                        agegroup         = thwart(form.agegroup.data),
                        pdfbrochure      = thwart(filepdf),
                        timecreated      = now,
                        timestamp        = now,
                        start            = starttime,
                        end              = endtime,
                        repeats          = '0',
                        language         = '1',
                        approved         = '1',
                        ) # this is the post data collection in the model includes all the cells in the data model
                    #print postgroup
                    db.session.add(post)
                    db.session.commit()
                    spot = 1500
                    flash('SUCCESS! Post %s has been submitted!' % (titlebody))
                # if submit then clear the fields otherwise keep the fields
                #print "about to test"
                #print request.form['submit']
                spot = 1600
                if request.form['submit'] == 'Submit': #clear fields we are finished the post ready for a new one
                    form.cemail.data          = ""
                    form.vemail.data          = ""
                    form.cphone.data          = ""
                    form.vphone.data          = ""
                    form.cname.data           = tuser
                    form.vname.data           = ""
                    form.startyr.data         = now.year
                    form.endyr.data           = now.year
                    form.startmth.data        = now.month
                    form.endmth.data          = now.month
                    form.startday.data        = now.day
                    form.endday.data          = now.day
                    form.starthr.data         = now.hour
                    form.endhr.data           = now.hour + 1
                    form.startmin.data        = 1
                    form.endmin.data          = 1
                    form.vaddress.data        = ""
                    form.vcity.data           = ""
                    form.posttitle.data       = ""
                    form.image.data           = ""
                    form.pdfbrochure.data     = ""
                    form.externalwebsite.data = ""
                    form.websitetitle.data    = ""
                    form.postbody.data        = ""
                    form.relay.data           = ""
                    form.venuename.data       = ""
                    form.street.data          = ""
                    form.city.data            = ""
                    form.other.data           = ""
                    form.price.data           = "Call"
                    form.postgroup.data       = ""
                    form.googlemap.data       = ""
                    form.mapchoice.data       = ""
                    form.name.data            = ""
                    form.imagefile.data       = ""
                    form.pdffile.data         = ""
                    filename                  = ""
                    filepdf                   = ""
                    spot = 1700
                #else: # data was submit and repeat
                #    form.postbody.data = request.form.get('ckeditor')
                #return redirect( url_for ('home.index'))
        else: # This is where GET was chosen new post setup fill in the blanks                             GET
              # get the info from the user and populate the form
            spot = 1800
            form.cname.data    = tuser
            form.startyr.data  = now.year
            form.endyr.data    = now.year
            form.startmth.data = now.month
            form.endmth.data   = now.month
            form.startday.data = now.day
            form.endday.data   = now.day
            form.starthr.data  = now.hour
            form.endhr.data    = now.hour + 1
            form.startmin.data = 1
            form.endmin.data   = 1
            form.price.data    = "Call"
            spot = 2900
        return render_template('post.html',
                title     = 'Post',
                form      = form,
                but       = but,
                butb      = butb,
                role      = role,
                groups    = tgroups,
                imagefile = filename,
                pdffile   = filepdf,
                gbut      = current_app.config.get('DEFAULTBUTTON3')
                )
    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in Post",
            errstring = str(e),
            user      = tuser,
            place     = spot)
        subject = "Exception Error!"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))












