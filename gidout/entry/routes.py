#generate_confirmation_token confirm_token send_email /confirm/<token> /reset/<token> /confirmation
#/register /contact /dmca

from . import entry
from flask import render_template, Flask, request, session, flash, url_for, redirect, current_app
from flask_wtf import Form
from flask_security import login_required
from flask_mail import Message, Mail
from xtermcolor import colorize
from gidout import db
from datetime import datetime
from gidout.models import Gidgroup, Edge, User, Contact, Scheme
from gidout.forms import ContactForm, RegistrationForm, LoginForm
import unicodedata, calendar, logging
from logging.handlers import RotatingFileHandler
from itsdangerous import URLSafeTimedSerializer
from werkzeug.security import generate_password_hash, check_password_hash
import safe
from flask_login import login_required, current_user

mail = Mail()

def generate_confirmation_token(email):
    serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
    return serializer.dumps(email, salt=current_app.config['SECURITY_PASSWORD_SALT'])

def confirm_token(token, expiration=30000):
    serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
    try:
        email = serializer.loads(
            token,
            salt=current_app.config['SECURITY_PASSWORD_SALT'],
            max_age=expiration
        )
    except:
        return False
    return email

def send_email(to, subject, template):
    msg = Message(
        subject,
        recipients=[to],
        html=template,
        sender=current_app.config['MAIL_DEFAULT_SENDER']
    )
    mail.send(msg)

@entry.route('/confirm/<token>')
def confirm_email(token):
    but = current_app.config['DEFAULTBUTTON1']
    spot = 10
    try:
        email = confirm_token(token)
        #print('email  ', email)
        if email == False:
            flash('The confirmation link is invalid or has expired.', 'danger')
            return redirect(url_for('entry.register'))

        user = User.query.filter_by(email=email).first_or_404()
        if user.confirm_email == 1:
            flash('Account already confirmed. Please login.', 'success')
        else:
            user.status = 1
            user.confirm_email = 1
            user.first_date = datetime.now()
            db.session.add(user)
            db.session.commit()
            flash('You have confirmed your account. Thanks!', 'success')
            #confirmed now logon
        return redirect(url_for('admin.login'))
    except Exception as e:
        html = render_template('adminerrortemplate.html',
            exception = "Failed in confirm email ",
            errstring = "Error String:  " + repr(e),
            user      = "tuser",
            place     = spot)
        subject = "Exception Error! In entry"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@entry.route('/reset/<token>')
def reset_email(token):
    spot = 10
    but  = current_app.config['DEFAULTBUTTON1']
    try:
        email = confirm_token(token)
        #print 'email  ', email
        resetpass = current_app.config['RESET_PASS']

        if email == False:
            flash('The reset link is invalid or has expired.', 'danger')
            return redirect(url_for('admin.login'))
        user = User.query.filter_by(email=email).first_or_404()
        session['mail'] = user.email
        user.status = resetpass # set status so that the user's security my be protected
        db.session.commit()
        #put username into a register form and send to the new password page

        return redirect(url_for('admin.resetpass'))
    except Exception as e:
        html = render_template('adminerrortemplate.html',
            exception = "Failed in reset_email ",
            errstring = "Error String:  " + repr(e),
            user      = "tuser",
            place     = spot)
        subject = "Exception Error! In entry"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@entry.route('/confirmation', methods = ['GET', 'POST'])
def confirmation():
    spot = 10
    try:
        but   = current_app.config['DEFAULTBUTTON1']
        if current_user.is_authenticated:
            if current_user.role > 4:
                but[5] = 'food'
            if current_user.role > 49:
                but[5] = 'adminhome'
                #print 'admin on front page'
        else:
            but[5] = 'register'
        butb = current_app.config['DEFAULTBUTTON2']
        form = "tree"
        now = datetime.now()
        mycal = calendar.HTMLCalendar()
        table = mycal.formatmonth(2015, 9)

        return render_template("confirmation.html",
            form = form,
            title = 'Confirmation',
            but = but,
            butb = butb)
    except Exception as e:
        html = render_template('adminerrortemplate.html',
            exception = "Failed in confirmation ",
            errstring = "Error String:  " + repr(e),
            user      = "tuser",
            place     = spot)
        subject = "Exception Error! In entry"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@entry.route('/register', methods = ['GET', 'POST'])
def register():
    but   = current_app.config['DEFAULTBUTTON1']
    if current_user.is_authenticated:
        if current_user.role > 4:
            but[5] = 'food'
        if current_user.role > 49:
            but[5] = 'adminhome'
    else:
        but[5] = 'register'
    try:
        butb = current_app.config['DEFAULTBUTTON2']
        form = RegistrationForm()
        spot = 0
        #print('Registration entry')
        if form.validate_on_submit():
            spot = 10
            # after validation check for presence of the email address flash address used forgot password
            # hash the password before saving
            # check username
            # print('Validated')
            temail = form.email.data
            tnickname = form.username.data
            #hash the password
            tpass = generate_password_hash(form.password.data)
            #print (tpass, '  ', form.password.data)
            auser = User(
                nickname        = form.username.data,
                email           = form.email.data,
                passwd          = tpass,
                first_name      = form.first_name.data,
                last_name       = form.last_name.data,
                street          = form.address.data,
                city            = form.city.data,
                state_prov      = form.state_prov.data,
                country         = form.country.data,
                postalcode      = form.postalcode.data,
                phone1          = form.phone.data,
                total_paid      = 0,
                account         = 0,
                payment_due     = 0,
                confirm_email   = False
            )
            #This is where to put a check to refuse usernames
            try:
                spot = 20
                if auser.query.filter_by(email = temail).count() != 0:
                    #email is beimg used return a flash now check for nickname may proceed
                    #print ("email already being used")
                    flash('That email address has already been used! If this is you then please logon')
                    return redirect(url_for('entry.register'))
                if auser.query.filter_by(nickname = tnickname).count() != 0:
                    spot = 25
                    #username is already used
                    flash('That Username has already been used!')
                    return redirect(url_for('entry.register'))
                strength = safe.check(raw=form.password.data, length=8,freq=3, min_types=2,level=2)
                #print('safe password  ', strength)
                if  not strength.valid :
                    spot = 30
                    if strength.message == 'password is good enough, but not strong':
                        flash('Password is good enough, but not strong')
                    if strength.message == 'password is too short':
                        flash('Password is too short, make sure it is 8 characters or more')
                    if strength.message == 'password has a pattern':
                        flash('Password is too easy to guess')
                    if strength.message == 'password is too common':
                        flash('Password is too common')
                    if strength.message == 'password is too simple':
                        flash('Password is too simple')
                    spot = 40
                    return redirect(url_for('entry.register'))
            except Exception as e:
                #print ('some other error occured got an error from the above statement the error is ->', str(e))
                #flash an error
                flash('Oh Dear, that did not work please try again! ')

                return redirect(url_for('entry.register'))
            #print('generation email  ', auser.email)
            spot = 50
            token = generate_confirmation_token(auser.email)
            confirm_url = url_for('entry.confirm_email', token=token, _external=True)
            html = render_template('activate.html', confirm_url=confirm_url)
            subject = "Please confirm your email"
            send_email(auser.email, subject, html)
            db.session.add(auser)
            db.session.commit()
            flash('A confirmation email has been sent via email.', 'success')
            return redirect(url_for("entry.confirmation"))
        return render_template("register.html",
            title = 'Registration Entry',
            form = form,
            but = but,
            butb = butb)
    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in Register",
            errstring = str(e),
            user      = "None",
            place     = spot
            )
        subject = "Exception Error!"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@entry.route('/contact', methods = ['GET', 'POST'])
@login_required
def contact():
    but   = current_app.config['DEFAULTBUTTON1']
    if current_user.is_authenticated:
        if current_user.role > 4:
            but[5] = 'food'
        if current_user.role > 49:
            but[5] = 'adminhome'
            #print 'admin on front page'
    else:
        but[5] = 'register'
    butb    = current_app.config['DEFAULTBUTTON2']
    form    = ContactForm()
    userid  = current_user.id
    #print "Inside Contact"
    spot = 5
    try:
        if request.method == 'POST':
            if form.is_submitted():
                spot = 10
                if form.validate(): # form submitted validate, send the mail, and return clean form finished and flashed
                    sendername    = form.username.data + " " + str(userid)
                    senderemail   = form.email.data
                    subject = "CONTACT" + form.subject.data
                    message = form.message.data + " " + sendername + " " + senderemail
                    email = current_app.config['MAIL_ERROR_RECIPIENT']
                    send_email(email, subject, message)
                    flash('The message has been sent via email.', 'success')
                    form.subject.data = ""
                    form.message.data = ""
                    spot = 50
        else: # GET new setup fill form and render
            user = db.session.query(User).filter(User.id == userid).first()
            form.email.data    = user.email
            form.username.data = user.nickname
            #print "This is the user", user.email
        spot = 70
        return render_template("contact.html",
            form = form,
            but = but,
            title = 'Contact',
            )
    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in Contact",
            errstring = str(e),
            user      = userid,
            place     = spot
            )
        subject = "Exception Error! in Contact"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))

@entry.route('/dmca', methods = ['GET', 'POST'])
@login_required
def dmca():
    #print "Inside DMCA"
    but   = current_app.config['DEFAULTBUTTON1']
    if current_user.is_authenticated:
        if current_user.role > 4:
            but[5] = 'food'
        if current_user.role > 49:
            but[5] = 'adminhome'
            #print 'admin on front page'
    else:
        but[5] = 'register'
    butb    = current_app.config['DEFAULTBUTTON2']
    form    = ContactForm()
    userid  = current_user.id
    spot = 0
    try:
        if request.method == 'POST':
            if form.is_submitted():
                if form.validate(): # form submitted validate, send the mail, and return clean form finished and flashed
                    spot = 10
                    sendername    = form.username.data + " " + str(userid)
                    senderemail   = form.email.data
                    subject = "DMCA  " + form.subject.data
                    message = form.message.data + " " + sendername + " " + senderemail
                    email = current_app.config['MAIL_ERROR_RECIPIENT']
                    send_email(email, subject, message)
                    flash('The message has been sent via email.', 'success')
                    form.subject.data = ""
                    form.message.data = ""

        else: # GET new setup fill form and render
            spot = 20
            user = db.session.query(User).filter(User.id == userid).first()
            form.email.data    = user.email
            form.username.data = user.nickname
            #print "This is the user", user.email

        return render_template("dmca.html",
            form = form,
            but = but,
            title = 'DMCA',
            )
    except Exception as e:
        flash("Something Failed ouch")
        html = render_template('adminerrortemplate.html',
            exception = "Failed in DMCA",
            errstring = str(e),
            user      = userid,
            place     = spot)
        subject = "Exception Error!"
        send_email(current_app.config['MAIL_ERROR_RECIPIENT'], subject, html)
        return(str(e))











