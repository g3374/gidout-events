from flask import Blueprint

entry = Blueprint('entry', __name__, template_folder='templates')

from . import routes, errors
