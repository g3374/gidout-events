import os

BASEDIR                   = os.path.abspath(os.path.dirname(__file__))

CSRF_ENABLED              = True
WTF_CSRF_TIME_LIMIT       = 7200
SECRET_KEY                = 'Someverylongseedkeywith losts of characters'
SECURITY_PASSWORD_SALT    = 'my_precious_two_is_more_than_many'
BCRYPT_LOG_ROUNDS         = 13

DEFAULTBUTTON1 = ['root', 'list', 'venue', 'calendar', 'about', 'register']
DEFAULTBUTTON2 = ["entertainment", "education", "health", "sport", "church", "vacation", "work", "volunteer", "store", "hotel"]
DEFAULTBUTTON3 = ['root','church','health','sport','news','entertainment','law','food','work','education','store','politics']

#Upload settings
UPLOADED_AVATAR_DEST       = BASEDIR + '/uploads/avatar'
UPLOADED_PDF_DEST          = BASEDIR + '/uploads/pdf'
UPLOADED_IMAGES_DEST       = BASEDIR + '/uploads/images'
ALLOWED_EXTENSIONS_PDF     = set(['pdf'])
ALLOWED_EXTENSIONS_IMAGES  = set(['png', 'jpg', 'jpeg', 'gif'])
UPLOADED_VENUE_IMAGES_DEST = BASEDIR + '/uploads/venue/'

#SqlAlchemy
SQLALCHEMY_DATABASE_URI         = 'mysql://dbuser:somebigpassword!!@127.0.0.1/gidout'
SQLALCHEMY_TRACK_MODIFICATIONS  = True
SQLALCHEMY_POOL_TIMEOUT         = 60
SQLALCHEMY_POOL_RECYCLE         = 120

#Mail settings
MAIL_SERVER            = 'mail.mailserver.ca'
MAIL_PORT              = 25
MAIL_USE_TLS           = False
MAIL_USE_SSL           = False
MAIL_USERNAME          = None #os.environ.get('MAIL_USERNAME')
MAIL_PASSWORD          = None
MAIL_DEFAULT_SENDER    = 'admin@gidout.com'
MAIL_FLUSH_INTERVAL    = 100  # one hour
MAIL_ERROR_RECIPIENT   = 'username@mygidout.com' #os.environ.get('MAIL_ERROR_RECIPIENT')
MAIL_MAX_EMAILS        = None



#Google map settings
GOOGLE_MAP_KEY         = "googlekeyhere"
GOOGLE_CITY_ID         = "ChIJcWGw3Ytzj1QR7Ui7HnTz6Dg"

#Location
CITY                   = "Victoria"
STATE                  = "British Columbia"

#User status
RESET_PASS             = 20
USER_NORMAL            = 1

#PayPal
PAYPAL_MODE                     = "sandbox" #"production"

PAYPAL_PRICE                    = "10"
PAYPAL_CURRENCY                 = "CAD"

# PAYPAL Sandbox
PAYPAL_SANDBOX_API_USERNAME     = ""
PAYPAL_SANDBOX_API_BUSINESS     = ""
PAYPAL_SANDBOX_API_PASSWORD     = ""
PAYPAL_SANDBOX_API_SIGNATURE    = ""
PAYPAL_SANDBOX_CLIENT_ID        = ""
PAYPAL_SANDBOX_CLIENT_SECRET    = ""
PAYPAL_SANDBOX_NOTIFY_URL       = "http://70.66.248.123/ipn"
PAYPAL_SANDBOX_CANCEL           = "http://70.66.248.123/paypal/cancel"
PAYPAL_SANDBOX_SUCCESS          = "http://70.66.248.123/paypal_success"
PAYPAL_SANDBOX_VALIDATE_URL     = 'https://ipnpb.sandbox.paypal.com/cgi-bin/webscr?cmd=_notify-validate{arg}'

#PayPal Production
PAYPAL_PRODUCTION_API_USERNAME  = ""
PAYPAL_PRODUCTION_API_PASSWORD  = ""
PAYPAL_PRODUCTION_API_BUSINESS  = ""
PAYPAL_PRODUCTION_API_SIGNATURE = ""
PAYPAL_PRODUCTION_CLIENT_ID     = ""
PAYPAL_PRODUCTION_CLIENT_SECRET = ""
PAYPAL_PRODUCTION_NOTIFY_URL    = ""
PAYPAL_PRODUCTION_CANCEL        = ""
PAYPAL_PRODUCTION_SUCCESS       = ""
PAYPAL_PRODUCTION_VALIDATE_URL  = ""

if PAYPAL_MODE == "sandbox":
    PAYPAL_API_USERNAME         = PAYPAL_SANDBOX_API_USERNAME
    PAYPAL_API_PASSWORD         = PAYPAL_SANDBOX_API_PASSWORD
    PAYPAL_API_SIGNATURE        = PAYPAL_SANDBOX_API_SIGNATURE
    PAYPAL_CLIENT_ID            = PAYPAL_SANDBOX_CLIENT_ID
    PAYPAL_CLIENT_SECRET        = PAYPAL_SANDBOX_CLIENT_SECRET
    PAYPAL_NOTIFY_URL           = PAYPAL_SANDBOX_NOTIFY_URL
    PAYPAL_API_BUSINESS         = PAYPAL_SANDBOX_API_BUSINESS
    PAYPAL_API_CANCEL           = PAYPAL_SANDBOX_CANCEL
    PAYPAL_API_SUCCESS          = PAYPAL_SANDBOX_SUCCESS
    PAYPAL_VALIDATE_URL         = PAYPAL_SANDBOX_VALIDATE_URL
else:
    PAYPAL_API_USERNAME         = PAYPAL_PRODUCTION_API_USERNAME
    PAYPAL_API_PASSWORD         = PAYPAL_PRODUCTION_API_PASSWORD
    PAYPAL_API_SIGNATURE        = PAYPAL_PRODUCTION_API_SIGNATURE
    PAYPAL_CLIENT_ID            = PAYPAL_PRODUCTION_CLIENT_ID
    PAYPAL_CLIENT_SECRET        = PAYPAL_PRODUCTION_CLIENT_SECRET
    PAYPAL_NOTIFY_URL           = PAYPAL_PRODUCTION_NOTIFY_URL
    PAYPAL_API_BUSINESS         = PAYPAL_PRODUCTION_API_BUSINESS
    PAYPAL_API_CANCEL           = PAYPAL_PRODUCTION_CANCEL
    PAYPAL_API_SUCCESS          = PAYPAL_PRODUCTION_SUCCESS
    PAYPAL_VALIDATE_URL         = PAYPAL_PRODUCTION_VALIDATE_URL

#User Roles
KARMA                           = 0
ROLE_GUEST                      = 0
ROLE_USER                       = 5
ROLE_ADULT                      = 10
ROLE_PREM                       = 20
ROLE_MOD                        = 50
ROLE_ADMIN                      = 100



#Dropzone Config
DROPZONE_ALLOWED_FILE_TYPE      = 'image'
DROPZONE_MAX_FILE_SIZE          = 4
DROPZONE_MAX_FILES              = 30
#DROPZONE_PARALLEL_UPLOADS       = 12    # set parallel amount
#DROPZONE_UPLOAD_MULTIPLE        = True # enable upload multiple
#DROPZONE_IN_FORM                = True
DROPZONE_ENABLE_CSRF            = True
DROPZONE_UPLOAD_ACTION          = 'venue.handle_upload'
#DROPZONE_UPLOAD_ON_CLICK        = True
#DROPZONE_UPLOAD_BTN_ID          = 'submit'

#CKEditor Config
CKEDITOR_PKG_TYPE               = 'basic'
CKEDITOR_HEIGHT                 = 400

#Dict of bad words and their replacement values
RESTRICTED_WORDS = {'fuck':'****', 'shit':'s***', 'cunt':'****', 'nigger':'****', 'slut':'****', 'jew':'***', 'kike':'****'}

#List of adult groups

ADULT_GROUPS = ["casino", "xxx", "escort", "gambling", "sex", "alcohol", "abortion", "bars", "liquor", "birth_control",
            "bingo", "adult"]

#Email Addresses
EMAIL_ADDRESS_ADMIN        = "admin@mygidout.com"
EMAIL_ADDRESS_ERROR        = "web_errors@mygidout.com"
EMAIL_ADDRESS_DISPUTES     = "disputes@mygidout.com"
EMAIL_ADDRESS_INFO         = "info@mygidout.com"
EMAIL_ADDRESS_DO_NOT_REPLY = "do_not_reply@mygidout.com"

#Gcalendar

MAX_YEAR                   = 2050
MIN_YEAR                   = 2021

# Group list of groups that can be empty
PROTECTED_GROUPS = [ "academic", "academy", "accomodation", "activism", "advocacy", "alternative",
"amusement", "art", "attractions", "care", "charity", "church", "city", "club", "college", "community_centre",
"directory", "education", "emergency", "employment", "entertainment", "environment", "fitness",
"food", "fund_raising", "games", "garage_sale", "girl", "government", "health", "hobby",
"holiday", "hospital", "hotel", "jobs", "justice", "k-12", "knowledge", "law", "learning", "leisure", "library",
"love", "market", "medical", "money", "motel", "movie", "music", "news", "psychology", "radio", "religion", "resort", "restaurant",
"retail", "retirement", "safety", "sales", "school", "school_dist_61", "school_dist_62", "school_dist_63", "school_dist_64", "science",
"security", "self_defense", "seniors", "service", "shopping", "social", "society", "spiritualism", "sport", "stage",
"stocks", "teen", "theatre", "therapy", "tourist", "tours", "toys", "trade_show",
"training", "transportation", "travel", "tutoring", "unemployment", "university", "vacation", "vegan", "vegitarian", "vehicle",
"victoria", "volunteer", "wellness", "work"]







